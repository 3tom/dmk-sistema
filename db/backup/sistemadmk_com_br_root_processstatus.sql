INSERT INTO sistemadmk_com_br_root.processstatus (id, name) VALUES (1, 'Em andamento');
INSERT INTO sistemadmk_com_br_root.processstatus (id, name) VALUES (2, 'Recusado pelo correspondente');
INSERT INTO sistemadmk_com_br_root.processstatus (id, name) VALUES (3, 'Aceito pelo correspondente');
INSERT INTO sistemadmk_com_br_root.processstatus (id, name) VALUES (4, 'Aguardando cliente	');
INSERT INTO sistemadmk_com_br_root.processstatus (id, name) VALUES (5, 'Aguardando correspondente');
INSERT INTO sistemadmk_com_br_root.processstatus (id, name) VALUES (6, 'Cancelado');
INSERT INTO sistemadmk_com_br_root.processstatus (id, name) VALUES (7, 'Finalizado');