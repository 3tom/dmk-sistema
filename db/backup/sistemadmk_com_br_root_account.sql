INSERT INTO sistemadmk_com_br_root.account (id, name, companyname, cnpj, municipalregistration, stateregistration, street, number, complement, cep, city, state, maincontact, mainphone1, mainphone2, mainphone3, mainphone4, mainphone5, mainphone6, mainphone7, mainphone8, mainemail1, mainemail2, mainemail3, site, note, created) VALUES (1, 'DMK ADVOGADOS ASSOCIADOS', 'DEBORAH MEKACHESKI PEREIRA ADVOGADOS ASSOCIADOS', '19.439.096/0001-17', 'Isento', 'Isento', 'Rua Saldanha Marinho', '374', 'Cj. 805 / 806', '88010-450', 'Florianópolis', 'SC', 'Deborah Mekacheski', '48 3207 0177', '48 3207 1304', '(48) 30372814 ', '48 98352445 TIM', '(48) 91308024 VIVO', '48 99345862 TIM', '', '', 'dmk@dmkadvogados.com.br', 'aud.prot@dmkadvogados.com.br', 'financeiro@dmkadvogados.com.br', 'www.dmkadvogados.com.br', 'ADVOGADAS:DEBORAH MEKACHESKI PEREIRA, brasileira, casada, inscrita na OAB/SC Nº 33.565B, portadora do RG: 44.392.043-6 e do CPF: 308.736.638-29; FERNANDA LOPES VIEIRA FERREIRA, brasileira, divorciada, inscrito na OAB/SC nº. 24.859, portadora do RG: 127825875 e do CPF: 003.355.999-66 e CAMILA PICCOLI GUZENSKI, brasileira, solteira, inscrita na OAB/SC 37.610, portadora do RG: 50007882 e do CPF: 059.931.479-62. 

PREPOSTOS: JULIANA GIANLUPPI PEREIRA, brasileira, solteira, portadora do RG: 5224739 e do CPF: 058.946.109-58; 
GISELE DELGADO FONTES, brasileira, solteira, portadora do RG: 30589481-X e do CPF: 280.472.558-86;
REGINA DE FÁTIMA MEKACHESKI PEREIRA, brasileira, casada, portadora do RG: 13.355.903 e do CPF: 054.151.658-27;
MATHEUS SILVA SOUTO, brasileiro, solteiro, portador do RG: 5915608 e do CPF: 101.705.679-07;
ANDRESSA NAZARIO, brasileira, solteira, portadora do RG: 7267082 e do CPF: 114.137.159-66; 
JOÃO BATISTA KRETZER, brasileiro, solteiro, portador do RG: 6.129.010 e do CPF: 091.113.499-99; 
DOUGLAS MEKACHESKI PEREIRA, brasileiro, solteiro, portador do RG: 30.376-201-1 e do CPF: 272.474.178-11; 
THIAGO DE SOUZA VAZ, brasileiro, solteiro, portador do RG: 4063503074 e do CPF: 002.649.290-39; 
ANTONIO PEREIRA NETO, CPF: 512.828.608-87; 
MÁRCIA CRISTINA CARRIEL GOMES - CPF: 591.712.199-00;', '2016-08-17 16:45:28');