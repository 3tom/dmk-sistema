INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (1, 'Acre', 'AC');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (2, 'Alagoas', 'AL');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (3, 'Amazonas', 'AM');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (4, 'Amapá', 'AP');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (5, 'Bahia', 'BA');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (6, 'Ceará', 'CE');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (7, 'Distrito Federal', 'DF');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (8, 'Espírito Santo', 'ES');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (9, 'Goiás', 'GO');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (10, 'Maranhão', 'MA');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (11, 'Minas Gerais', 'MG');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (12, 'Mato Grosso do Sul', 'MS');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (13, 'Mato Grosso', 'MT');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (14, 'Pará', 'PA');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (15, 'Paraíba', 'PB');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (16, 'Pernambuco', 'PE');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (17, 'Piauí', 'PI');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (18, 'Paraná', 'PR');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (19, 'Rio de Janeiro', 'RJ');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (20, 'Rio Grande do Norte', 'RN');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (21, 'Rondônia', 'RO');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (22, 'Roraima', 'RR');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (23, 'Rio Grande do Sul', 'RS');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (24, 'Santa Catarina', 'SC');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (25, 'Sergipe', 'SE');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (26, 'São Paulo', 'SP');
INSERT INTO sistemadmk_com_br_root.country (id, name, abbreviation) VALUES (27, 'Tocantins', 'TO');