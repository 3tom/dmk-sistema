<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-close"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lista de Cidades
                            <span class="toggle-panel pull-right">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Estado</th>
                                <th width="185">Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->country_abbreviation; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="./<?php echo $this->uri->segment(1); ?>/editar/<?php echo $row->id; ?>"
                                               class="btn btn-info"><i class="fa fa-edit"></i> Editar</a>
                                            <a href="./<?php echo $this->uri->segment(1); ?>/excluir/<?php echo $row->id; ?>"
                                               class="btn btn-danger"><i class="fa fa-trash"></i> Excluir</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer" style="border-top: none;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>