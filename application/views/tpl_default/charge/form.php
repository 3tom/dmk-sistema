<form
    action="./<?=$this->uri->segment(1); ?>/salvar/<?=isset($data->id) ? $data->id : NULL; ?>"
    method="post"
    class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?=(isset($data->id) ? 'Edição' : 'Cadastro') . ' de despesas internas'; ?> </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Categoria de despesas</label>
                                <div class="col-sm-7">
                                    <select name="chargecategory_id" id="chargecategory_id" class="form-control">
                                        <?php foreach ($chargecategory_id as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($data->chargecategory_id) && $data->chargecategory_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Tipo de despesas</label>
                                <div class="col-sm-7">
                                    <select name="chargetype_id" id="chargetype_id" class="form-control">
                                        <?php foreach ($chargetype_id as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($data->chargetype_id) && $data->chargetype_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="value" class="col-sm-3">Valor:</label>
                                <div class="col-sm-9 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input type="text" class="form-control money"
                                           name="value" value="<?=isset($data->value) ? $data->value : NULL; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Data de vencimento</label>
                                <div class="col-sm-7">
                                    <input name="deadline" type="text" class="form-control date"
                                           value="<?=isset($data->deadline) ? $data->deadline : NULL; ?>"
                                           id="deadline">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="value" class="col-sm-2">Observações:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="text" id="text" cols="30" rows="10"><?=isset($data->text) ? $data->text : NULL; ?></textarea>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <?php if(isset($data->id)):?>
                    <a href="./<?=$this->uri->segment(1); ?>/excluir/<?=$data->id; ?>">
                        <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                        <span>Excluir</span>
                    </a>
                    <?php endif;?>
                    <button type="submit" class="pull-right">
                        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                        <span>Salvar</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>