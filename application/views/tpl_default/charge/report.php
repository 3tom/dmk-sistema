<form
        action="./<?=$this->uri->segment(1); ?>"
        method="get"
        class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Categoria</label>
                                <div class="col-sm-7">
                                    <select name="chargecategory_id" id="chargecategory_id" class="form-control">
                                        <option value="">Todos</option>
                                        <?php foreach ($chargecategory_id as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($filter->chargecategory_id) && $filter->chargecategory_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Tipo de despesas</label>
                                <div class="col-sm-7">
                                    <select name="chargetype_id" id="chargetype_id" class="form-control">
                                        <option value="">Todos</option>
                                        <?php foreach ($chargetype_id as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($filter->chargetype_id) && $filter->chargetype_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-2">Período</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input name="deadlinestart" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlinestart) ? $filter->deadlinestart : NULL; ?>"
                                                   id="deadlinestart">
                                        </div>
                                        <div class="col-sm-1">até</div>
                                        <div class="col-sm-5">
                                            <input name="deadlineend" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlineend) ? $filter->deadlineend : NULL; ?>"
                                                   id="deadlineend">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-search fa-2x" aria-hidden="true"></i>
                        <span>Filtrar</span>
                    </button>
                    <button type="submit" class="pull-right" value="true" name="excel">
                        <i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i>
                        <span>Excel</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-close"
                     data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Balanço Financeiro DMK</h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default panel-close"
                             data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                            <div class="panel-body">
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                    <tr>
                                        <th>Categoria de despesa interna</th>
                                        <th>Tipo de despesa</th>
                                        <?php if(count($charge_monthly) >0)foreach (current($charge_monthly)->month as $row): ?>
                                        <th><?php echo lang('cal_' . strtolower(date('M', strtotime($row))));?></th>
                                        <?php endforeach;?>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($charge_monthly as $row): ?>
                                        <tr>
                                            <td><?=$chargecategory_id[$row->chargecategory_id]->name; ?></td>
                                            <td><?=$chargetype_id[$row->chargetype_id]->name; ?></td>
                                            <?php foreach ($row->item as $item): ?>
                                                <td><?php echo $item;?></td>
                                            <?php endforeach;?>
                                        </tr>
                                    <?php endforeach; ?>
                                    <?php if(count($correspondentValue) > 1):?>
                                        <tr>
                                            <td>PAGAMENTOS DE CORRESPONDENTES</td>
                                            <td>Pagamentos consolidades de correspondentes</td>
                                            <?php foreach ($month_value as $key => $item): ?>
                                                <td><?php echo isset($correspondentValue[$key]) ? $correspondentValue[$key] : '0,00';?></td>
                                            <?php endforeach;?>
                                        </tr>
                                    <?php endif;?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <?php foreach ($month_value as $key => $item): ?>
                                            <td></td>
                                        <?php endforeach;?>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>TOTAL DESPESAS</td>
                                        <?php foreach ($totalMonth_value as $key => $item): ?>
                                            <td><?php echo isset($item) ? $item : '0,00';?></td>
                                        <?php endforeach;?>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>TOTAL RECEITA</td>
                                        <?php foreach ($month_value as $key => $item): ?>
                                            <td><?php echo isset($clientValue[$key]) ? $clientValue[$key] : '0,00';?></td>
                                        <?php endforeach;?>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>SALDO TOTAL</td>
                                        <?php foreach ($month_value as $key => $item): ?>
                                            <td><?php echo isset($balance[$key]) ? $balance[$key] : '0,00';?></td>
                                        <?php endforeach;?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php foreach ($charge_monthly as $row): ?>
    <script class="code" language="javascript" type="text/javascript">
        $(document).ready(function(){

            <?php $min = 0; ?>
            <?php $max = 0; ?>

            var line = [];
            <?php foreach ($row->month as $month): ?>

            line.push(['<?= lang('cal_' . strtolower(date('M', strtotime($month))));?>']);
            <?php endforeach; ?>
            <?php $itemCount = 0; array_pop($row->item); foreach ($row->item as $item): ?>
            <?php $current = str_replace(',', '.', str_replace('.', '', $item)); ?>
            <?php $min = $min > $current ? $current : $min; ?>
            <?php $max = $max < $current ? $current : $max; ?>
            <?php $diff = $max * 0.1;?>
            line[<?=$itemCount++?>].push('<?=$current;?>');
            <?php endforeach;?>

            var plot2 = $.jqplot('chart<?=$chargecategory_id[$row->chargecategory_id]->id; ?>', [line], {
                title:'<?=$chargecategory_id[$row->chargecategory_id]->name; ?> - <?=$chargetype_id[$row->chargetype_id]->name; ?>',
                animate: true,
                animateReplot: true,
                cursor: {
                    show: true,
                    showTooltip: true
                },
                series:[
                    {
                        pointLabels: {
                            show: true
                        },
                        showHighlight: true,
                        rendererOptions: {
                            animation: {
                                speed: 2500
                            },
                            barWidth: 15,
                            barPadding: 15,
                            barMargin: 10,
                            highlightMouseOver: true
                        }
                    },
                    {
                        rendererOptions: {
                            animation: {
                                speed: 2000
                            }
                        }
                    }
                ],
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                    tickOptions: {
                        angle: 30
                    }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer
                    },
                    yaxis: {
                        tickOptions: {
                            formatString: "R$ %'.2f"
                        },
                        rendererOptions: {
                            forceTickAt0: true
                        },
                        min: <?=$min - $diff;?>,
                        max: <?=$max + $diff;?>
                    }
                }
            });
        });

    </script>

<div id="chart<?=$chargecategory_id[$row->chargecategory_id]->id; ?>" class="plot"></div>
<?php endforeach;?>


<?php if(count($correspondentValue) > 1):?>
    <script class="code" language="javascript" type="text/javascript">
        $(document).ready(function(){

            <?php $min = 0; ?>
            <?php $max = 0; ?>

            var line = [];
            <?php reset($charge_monthly); foreach (current($charge_monthly)->month as $row): ?>
            line.push(['<?= lang('cal_' . strtolower(date('M', strtotime($row))));?>']);
            <?php endforeach; ?>
            <?php $itemCount = 0;?>
            <?php array_pop($month_value); ?>
            <?php foreach ($month_value as $key => $item): ?>
            <?php $current = str_replace(',', '.', str_replace('.', '', (isset($correspondentValue[$key]) ? $correspondentValue[$key] : '0,00'))); ?>
            <?php $min = $min > $current ? $current : $min; ?>
            <?php $max = $max < $current ? $current : $max; ?>
            <?php $diff = $max * 0.1;?>
            line[<?=$itemCount++?>].push('<?=$current;?>');
            <?php endforeach;?>

            var plot2 = $.jqplot('chartCorreespo', [line], {
                title:'Pagamentos consolidades de correspondentes',
                animate: true,
                animateReplot: true,
                cursor: {
                    show: true,
                    showTooltip: true
                },
                series:[
                    {
                        pointLabels: {
                            show: true
                        },
                        showHighlight: true,
                        rendererOptions: {
                            animation: {
                                speed: 2500
                            },
                            barWidth: 15,
                            barPadding: 15,
                            barMargin: 10,
                            highlightMouseOver: true
                        }
                    },
                    {
                        rendererOptions: {
                            animation: {
                                speed: 2000
                            }
                        }
                    }
                ],
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                    tickOptions: {
                        angle: 30
                    }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer
                    },
                    yaxis: {
                        tickOptions: {
                            formatString: "R$ %'.2f"
                        },
                        rendererOptions: {
                            forceTickAt0: true
                        },
                        min: <?=$min - $diff;?>,
                        max: <?=$max + $diff;?>
                    }
                }
            });
        });

    </script>

<div id="chartCorreespo" class="plot"></div>
<?php endif;?>