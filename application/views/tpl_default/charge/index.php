<form
        action="./<?=$this->uri->segment(1); ?>"
        method="get"
        class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-body">
                    <div class="row">
                        <!--
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Correspondente</label>
                                <div class="col-sm-7">
                                    <select name="correspondent_id" id="correspondent_id" class="form-control">
                                        <option value="">Todos</option>
                                        <?php foreach ($correspondent_id as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($filter->correspondent_id) && $filter->correspondent_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Categoria de despesa</label>
                                <div class="col-sm-7">
                                    <select name="chargecategory_id" id="chargecategory_id" class="form-control">
                                        <option value="">Todos</option>
                                        <?php foreach ($chargecategory_id as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($filter->chargecategory_id) && $filter->chargecategory_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Tipo de despesas</label>
                                <div class="col-sm-7">
                                    <select name="chargetype_id" id="chargetype_id" class="form-control">
                                        <option value="">Todos</option>
                                        <?php foreach ($chargetype_id as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($filter->chargetype_id) && $filter->chargetype_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-2">Período</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input name="deadlinestart" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlinestart) ? $filter->deadlinestart : NULL; ?>"
                                                   id="deadlinestart">
                                        </div>
                                        <div class="col-sm-1">até</div>
                                        <div class="col-sm-5">
                                            <input name="deadlineend" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlineend) ? $filter->deadlineend : NULL; ?>"
                                                   id="deadlineend">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-search fa-2x" aria-hidden="true"></i>
                        <span>Filtrar</span>
                    </button>
					<button type="submit" class="pull-right" value="true" name="excel">
                        <i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i>
                        <span>Excel</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-close"
                     data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Acompanhamento despesas</h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default panel-close"
                             data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">

                            <div class="panel-heading">
                                <h3 class="panel-title">Despesas internas</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered charge" id="dataTable">
                                    <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Categoria de despesa interna</th>
                                        <th>Tipo de despesa</th>
                                        <th>Valor</th>
                                        <th>Vencimento</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($charge as $row): ?>
                                        <tr class="<?=$row->signal; ?>" style="cursor: pointer">
                                            <td>
                                                <a data-toggle="modal" href="#charge_<?=$row->id?>">
                                                    <?=$row->chargestatus_id; ?>
                                                    -
                                                    <?=$chargestatus_id[$row->chargestatus_id]->name; ?>
                                                </a>
                                                <div class="modal fade" id="charge_<?=$row->id?>">
                                                    <form action="" method="POST" role="form">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title">Mudar status</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <input type="hidden" name="id" value="<?=$row->id?>">
                                                                            <label for="charge_status">Status</label>
                                                                            <select name="charge_status" id="charge_status" class="form-control">
                                                                                <?php foreach ($chargestatus_id as $cRow):?>
                                                                                    <option value="<?=$cRow->id;?>" <?=isset($row->chargestatus_id) && $row->chargestatus_id == $cRow->id ? ' selected' : null;?>>
                                                                                        <?=$cRow->name;?>
                                                                                    </option>
                                                                                <?php endforeach;?>
                                                                            </select>
                                                                        </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </form>
                                                </div><!-- /.modal -->
                                            </td>
                                            <td onclick="window.location = '/despesas/editar/<?=$row->id; ?>'"><?=$chargecategory_id[$row->chargecategory_id]->name; ?></td>
                                            <td onclick="window.location = '/despesas/editar/<?=$row->id; ?>'"><?=$chargetype_id[$row->chargetype_id]->name; ?></td>
                                            <td onclick="window.location = '/despesas/editar/<?=$row->id; ?>'" data-order="<?=$row->value_db; ?>">R$ <?=$row->value; ?></td>
                                            <td onclick="window.location = '/despesas/editar/<?=$row->id; ?>'" data-order="<?=$row->deadlineTimestamp; ?>"><?=$row->deadline; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel panel-default panel-close"
                             data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">

                            <div class="panel-body">
                                <table class="table table-bordered correspondent" id="dataTable">
                                    <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Correspondentes</th>
                                        <th>Processo</th>
                                        <th>Valor Honorários</th>
                                        <th>Valor Despesas​</th>
                                        <th>Total</th>
                                        <th>Vencimento</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($process as $row): ?>
                                        <tr class="<?=$row->signal; ?>">
                                            <td>
                                                <a data-toggle="modal" href="#process_<?=$row->id?>">
                                                    <?=$row->chargestatus_id; ?>
                                                    -
                                                    <?=$chargestatus_id[$row->chargestatus_id]->name; ?>
                                                </a>
                                                <div class="modal fade" id="process_<?=$row->id?>">
                                                    <form action="" method="POST" role="form">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title">Mudar status</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <input type="hidden" name="id" value="<?=$row->id?>">
                                                                        <label for="process_status">Status</label>
                                                                        <select name="process_status" id="process_status" class="form-control">
                                                                            <?php foreach ($chargestatus_id as $cRow):?>
                                                                                <option value="<?=$cRow->id;?>" <?=isset($row->chargestatus_id) && $row->chargestatus_id == $cRow->id ? ' selected' : null;?>>
                                                                                    <?=$cRow->name;?>
                                                                                </option>
                                                                            <?php endforeach;?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </form>
                                                </div><!-- /.modal -->
                                            </td>
                                            <td><?=$row->correspondent_name; ?></td>
                                            <td><?=$row->number; ?></td>
                                            <td nowrap data-order="<?=$row->correspondentfee_db; ?>">R$ <?=$row->correspondentfee; ?></td>
                                            <td nowrap data-order="<?=$row->correspondentcoststypeservice_db; ?>">R$ <?=$row->correspondentcoststypeservice; ?></td>
                                            <td nowrap data-order="<?=$row->total_db; ?>">R$ <?=$row->total; ?></td>
                                            <td nowrap data-order="<?=$row->deadlineTimestamp; ?>"><?=$row->deadline ? $row->deadline : ''; ?></td>
                                        </tr>
                                    <?php endforeach; ?>

                                    <tr>
                                        <td style="color: transparent;">zzzzzzzzzzzzz</td>
                                        <td colspan="2" class="text-right">Totalizador</td>
                                        <td nowrap data-order="<?=$totalCorrespondentValue->correspondentfee_db; ?>">R$ <?=$totalCorrespondentValue->correspondentfee; ?></td>
                                        <td nowrap data-order="<?=$totalCorrespondentValue->correspondentcoststypeservice_db; ?>">R$ <?=$totalCorrespondentValue->correspondentcoststypeservice; ?></td>
                                        <td nowrap data-order="<?=$totalCorrespondentValue->total_db; ?>">R$ <?=$totalCorrespondentValue->total; ?></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>