<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <base href="<?php echo base_url(); ?>">
    <meta name="controller" content="<?php echo $this->router->class ?>"/>
    <meta name="method" content="<?php echo $this->router->method ?>"/>
    <?php echo isset($assets) ? $assets : NULL; ?>
    <?php echo isset($css) ? $css : NULL; ?>
    <?php echo isset($js) ? $js : NULL; ?>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="now_loading" id="now_loading">
    <i class="fa-li fa fa-spinner fa-spin"> </i>
</div>
<div class="global-inf">
    <div class="alert alert-danger alert-dismissible <?php echo (!$error) ? 'hide' : null ?>" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <strong>Erro:</strong> <?php echo $error ? $error : null ?><span class="text"></span>
    </div>

    <div class="alert alert-success alert-dismissible <?php echo (!$msg) ? 'hide' : null ?>" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <strong>Msg:</strong> <?php echo $msg ? $msg : null ?><span class="text"></span>
    </div>
</div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" id="toggle-menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="./">
                <img src="./assets/images/logo.png" alt="DMK">DMK
            </a>
        </div>

        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="./meu-perfil"><i class="fa fa-fw fa-user"></i> Perfil</a>
                    </li>
                    <li>
                        <a href="./login"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
            <?php if (isset($me->user_type) && $me->user_type == 2): ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-header">Varas</li>
                        <li>
                            <a href="./varas">Lista</a>
                        </li>
                        <li>
                            <a href="./varas/nova">Nova</a>
                        </li>
                        <li class="dropdown-header">Áreas de Direito</li>
                        <li>
                            <a href="./areas-de-direito">Lista</a>
                        </li>
                        <li>
                            <a href="./areas-de-direito/novo">Novo</a>
                        </li>
                        <li class="dropdown-header">Honorários por Tipo de Serviço</li>
                        <li>
                            <a href="./honorarios-por-tipo-de-servico">Lista</a>
                        </li>
                        <li>
                            <a href="./honorarios-por-tipo-de-servico/novo">Novo</a>
                        </li>
                        <li class="dropdown-header">Despesas por Tipo de Serviço</li>
                        <li>
                            <a href="./despesas-por-tipo-de-servico">Lista</a>
                        </li>
                        <li>
                            <a href="./despesas-por-tipo-de-servico/novo">Novo</a>
                        </li>
                        <li class="dropdown-header">Cidades</li>
                        <li>
                            <a href="./cidades">Lista</a>
                        </li>
                        <li>
                            <a href="./cidades/novo">Novo</a>
                        </li>
                        <li class="dropdown-header">Grupos de Cidades</li>
                        <li>
                            <a href="./grupos-de-cidades">Lista</a>
                        </li>
                        <li>
                            <a href="./grupos-de-cidades/novo">Novo</a>
                        </li>
                        <li class="dropdown-header">Categoria de despesas</li>
                        <li>
                            <a href="./categoria-despesas">Lista</a>
                        </li>
                        <li>
                            <a href="./categoria-despesas/nova">Novo</a>
                        </li>
                        <li class="dropdown-header">Tipo de despesas</li>
                        <li>
                            <a href="./tipo-despesas">Lista</a>
                        </li>
                        <li>
                            <a href="./tipo-despesas/novo">Novo</a>
                        </li>
                    </ul>
                </li>
            <?php endif;?>
        </ul>

        <div class="navbar-collapse toggle-menu">
            <ul class="nav navbar-nav side-nav">
                <li class="logo hidden-xs">
                    <a><img src="./assets/images/logo.png" alt="DMK"> DMK</a>
                </li>
                <?php if (isset($me->correspondent_id) && $me->correspondent_id == 0): ?>
                    <li>
                        <a href="./conta"><i class="fa fa-fw fa-home"></i> DMK</a>
                    </li>
                    <li>
                        <a a href="javascript:;" data-toggle="collapse" data-target="#client">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            Clientes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="client" class="collapse">
                            <li>
                                <a href="./cliente/relatorio">Relatório para Cliente</a>
                            </li>
                            <li>
                                <a href="./agenda-contato">Agenda de Contatos</a>
                            </li>
                            <li>
                                <a href="./cliente">Lista</a>
                            </li>
                            <li <?php echo (isset($me->user_type) && $me->user_type == 2)? '' : 'class="hidden"';?>>
                                <a href="./cliente/novo">Novo</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a a href="javascript:;" data-toggle="collapse" data-target="#correspondent">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            Correspondentes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="correspondent" class="collapse">
                            <li>
                                <a href="./correspondente">Lista</a>
                            </li>
                            <li <?php echo (isset($me->user_type) && $me->user_type == 2)? '' : 'class="hidden"';?>>
                                <a href="./correspondente/novo">Novo</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a a href="javascript:;" data-toggle="collapse" data-target="#user">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            Usuários <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="user" class="collapse">
                            <li>
                                <a href="./usuarios">Lista</a>
                            </li>
                            <li <?php echo (isset($me->user_type) && $me->user_type == 2)? '' : 'class="hidden"';?>>
                                <a href="./usuarios/novo">Novo</a>
                            </li>
                        </ul>
                    </li>
                <?php endif;?>

                <li>
                    <a a href="javascript:;" data-toggle="collapse" data-target="#process">
                        <i class="fa fa-archive" aria-hidden="true"></i>
                        Processos <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="process" class="collapse">
                        <?php if (isset($me->correspondent_id) && $me->correspondent_id == 0): ?>
                            <li>
                                <a href="./processos">Lista</a>
                            </li>
                        <?php endif;?>
                        <li>
                            <a href="./processos/acompanhamento">Acompanhamento</a>
                        </li>
                        <li <?php echo (isset($me->user_type) && $me->user_type == 2)? '' : 'class="hidden"';?>>
                            <a href="./processos/novo">Novo</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#balance">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                        Financeiro <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="balance" class="collapse">
                        <li>
                            <a href="./balanco-finaceiro">Balanço financeiro DMK</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" data-toggle="collapse" data-target="#charge">
                        <i class="fa fa-database" aria-hidden="true"></i>
                        Despesas <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="charge" class="collapse">
                        <li>
                            <a href="./despesas/nova">Cadastro de despesas internas</a>
                        </li>
                        <li>
                            <a href="./despesas">Acompanhamento Despesas</a>
                        </li>
                        <li>
                            <a href="./balanco-geral">Acompanhamento de Entradas e Balanço Geral</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">