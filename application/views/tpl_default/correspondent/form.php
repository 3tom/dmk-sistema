<form
    action="./<?php echo $this->uri->segment(1); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL; ?>"
    method="post"
    class="form-horizontal" enctype="multipart/form-data">
    <div class="row">
        <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>-form">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo (isset($data->id) ? 'Edição' : 'Cadastro') . ' de Correspondente'; ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4>Informações</h4>
                                        <hr>
                                    </div>
                                    <div class="col-lg-6 border-right">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-3">Razão Social/Nome</label>
                                            <div class="col-sm-9">
                                                <input name="name" type="text" class="form-control"
                                                       value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                                       id="name"
                                                       <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="document"
                                                   class="col-sm-3 col-md-3 col-lg-3">CNPJ/CPF</label>
                                            <div class="col-sm-5 col-md-4 col-lg-4">
                                                <input name="document" type="text" class="form-control"
                                                       value="<?php echo isset($data->document) ? $data->document : NULL; ?>"
                                                       id="document"
                                                       <?php echo ($me->user_type == 2)? '' : 'disabled';?>>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone1"
                                                   class="col-sm-3 col-md-3 col-lg-3">Telefones</label>
                                            <div class="col-sm-5 col-md-4 col-lg-4">
                                                <input name="phone1" type="text" class="form-control"
                                                       placeholder="Telefone 1"
                                                       value="<?php echo isset($data->phone1) ? $data->phone1 : NULL; ?>"
                                                       id="phone1"  <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone2" class="col-sm-3 col-md-3 col-lg-3"></label>
                                            <div class="col-sm-5 col-md-4 col-lg-4">
                                                <input name="phone2" type="text" class="form-control"
                                                       placeholder="Telefone 2"
                                                       value="<?php echo isset($data->phone2) ? $data->phone2 : NULL; ?>"
                                                       id="phone2" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone3" class="col-sm-3 col-md-3 col-lg-3"></label>
                                            <div class="col-sm-5 col-md-4 col-lg-4">
                                                <input name="phone3" type="text" class="form-control"
                                                       placeholder="Telefone 3"
                                                       value="<?php echo isset($data->phone3) ? $data->phone3 : NULL; ?>"
                                                       id="phone3" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone4" class="col-sm-3 col-md-3 col-lg-3"></label>
                                            <div class="col-sm-5 col-md-4 col-lg-4">
                                                <input name="phone4" type="text" class="form-control"
                                                       placeholder="Telefone 4"
                                                       value="<?php echo isset($data->phone4) ? $data->phone4 : NULL; ?>"
                                                       id="phone4" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="template-loop ajust-label">
                                            <div class="form-group">
                                                <label class="col-sm-3 col-md-3 col-lg-3">E-mails</label>
                                            </div>
                                            <div class="list-item">
                                                <div id="item-{{id}}" class="item template <?php echo ($me->user_type == 2)? '' : 'hidden';?>">
                                                    <div class="form-group">
                                                        <label for="correspondentemail_{{id}}_email"
                                                               class="col-sm-3 col-md-3 col-lg-3"></label>
                                                        <div class="col-sm-5 col-md-6 col-lg-6">
                                                            <div class="input-group">
                                                                <input name="correspondentemail[{{id}}][email]"
                                                                       type="email"
                                                                       class="form-control"
                                                                       placeholder="E-mail {{id}}"
                                                                       id="correspondentemail_{{id}}_email"
                                                                       >
                                                                <span class="input-group-btn">
                                                                    <a href="#item-remove"
                                                                       onclick="remove_template_item(this); return false;"
                                                                       class="pull-right btn btn-default text-danger item-remove"><i
                                                                            class="fa fa-close"></i></a>
                                                                        </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if (isset($data->correspondentemail) && count($data->correspondentemail) > 0): ?>
                                                    <?php $n = 1; ?>
                                                    <?php foreach ($data->correspondentemail as $row): ?>
                                                        <?php $id = $n; ?>
                                                        <div id="item-<?php echo $id; ?>" class="item">
                                                            <div class="form-group">
                                                                <label
                                                                    for="correspondentemail_<?php echo $id; ?>_email"
                                                                    class="col-sm-3 col-md-3 col-lg-3"></label>
                                                                <div class="col-sm-5 col-md-6 col-lg-6">
                                                                    <div class="input-group">
                                                                        <input
                                                                            name="correspondentemail[<?php echo $id; ?>][email]"
                                                                            type="email"
                                                                            class="form-control"
                                                                            placeholder="E-mail <?php echo $id; ?>"
                                                                            id="correspondentemail_<?php echo $id; ?>_email"
                                                                            value="<?php echo isset($row->email) ? $row->email : ''; ?>"
                                                                             <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                                        <span class="input-group-btn">
                                                                    <a href="#item-remove"
                                                                       onclick="remove_template_item(this); return false;"
                                                                       class="pull-right btn btn-default text-danger item-remove <?php echo ($me->user_type == 2)? '' : 'hidden';?>"><i
                                                                            class="fa fa-close"></i></a>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php $n++; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div class="clearfix"></div>
                                            <a href="#plus-template" class="plus-template pull-right <?php echo ($me->user_type == 2)? '' : 'hidden';?>">+</a>
                                            <div class="clearfix"></div>
                                        </div>
                                        <p>&nbsp;</p>
                                        <h4>Endereço</h4>
                                        <hr>
                                        <div class="form-group">
                                            <label for="street" class="col-sm-3">Rua</label>
                                            <div class="col-sm-9">
                                                <input name="street" type="text" class="form-control"
                                                       value="<?php echo isset($data->street) ? $data->street : NULL; ?>"
                                                       id="street"  <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="number" class="col-sm-3 col-md-3 col-lg-3">Nº</label>
                                            <div class="col-sm-5 col-md-4 col-lg-4">
                                                <input name="number" type="text" class="form-control"
                                                       value="<?php echo isset($data->number) ? $data->number : NULL; ?>"
                                                       id="number"  <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="complement" class="col-sm-3">Compl.</label>
                                            <div class="col-sm-9">
                                                <input name="complement" type="text" class="form-control"
                                                       value="<?php echo isset($data->complement) ? $data->complement : NULL; ?>"
                                                       id="complement"  <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="cep" class="col-sm-3 col-md-3 col-lg-3">CEP</label>
                                            <div class="col-sm-5 col-md-4 col-lg-4">
                                                <input name="cep" type="text" class="form-control"
                                                       value="<?php echo isset($data->cep) ? $data->cep : NULL; ?>"
                                                       id="cep"
                                                        <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class="col-sm-3">Bairro</label>
                                            <div class="col-sm-9">
                                                <input name="district" type="text" class="form-control"
                                                       value="<?php echo isset($data->district) ? $data->district : NULL; ?>"
                                                       id="district"
                                                    <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class="col-sm-3">Cidade</label>
                                            <div class="col-sm-9">
                                                <input name="city" type="text" class="form-control"
                                                       value="<?php echo isset($data->city) ? $data->city : NULL; ?>"
                                                       id="city"
                                                        <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="state" class="col-sm-3">Estado</label>
                                            <div class="col-sm-9">
                                                <input name="state" type="text" class="form-control"
                                                       value="<?php echo isset($data->state) ? $data->state : NULL; ?>"
                                                       id="state"
                                                        <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="note" class="col-sm-3">Observação</label>
                                            <div class="col-sm-9">
                                                        <textarea
                                                            <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                                                                name="note" class="form-control"
                                                                  id="note"><?php echo isset($data->note) ? $data->note : NULL; ?></textarea>
                                            </div>
                                        </div>
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="col-lg-6 border-left">
                                        <p>&nbsp;</p>
                                        <h4>Dados Bancários:</h4>
                                        <hr>
                                        <div class="template-loop">
                                            <div class="list-item">
                                                <div id="item-{{id}}" class="item template">
                                                    <h5><i>Dados Bancários - {{id}}</i> <a href="#item-remove"
                                                                                           onclick="remove_template_item(this); return false;"
                                                                                           class="pull-right text-danger item-remove"><i
                                                                class="fa fa-close"></i></a></h5>
                                                    <div class="form-group">
                                                        <label for="correspondentbank_{{id}}_owner"
                                                               class="col-sm-3">Titular</label>
                                                        <div class="col-sm-9">
                                                            <input name="correspondentbank[{{id}}][owner]"
                                                                   type="text"
                                                                   class="form-control"
                                                                   id="correspondentbank_{{id}}_owner" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="correspondentbank_{{id}}_cpf" class="col-sm-3">CPF /CNPJ</label>
                                                        <div class="col-sm-5">
                                                            <input name="correspondentbank[{{id}}][cpf]" type="text"
                                                                   class="form-control"
                                                                   id="correspondentbank_{{id}}_cpf" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="correspondentbank_{{id}}_type" class="col-sm-3">Tipo
                                                            da
                                                            Conta</label>
                                                        <div class="col-sm-5">
                                                            <select name="correspondentbank[{{id}}][type]"
                                                                    class="form-control">
                                                                <option value="Corrente">Corrente</option>
                                                                <option value="Poupança">Poupança</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="correspondentbank_{{id}}_name" class="col-sm-3">Banco</label>
                                                        <div class="col-sm-9">
                                                            <input name="correspondentbank[{{id}}][name]"
                                                                   type="text"
                                                                   class="form-control"
                                                                   id="correspondentbank_{{id}}_name" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="correspondentbank_{{id}}_agency"
                                                               class="col-sm-3 ">Agência</label>
                                                        <div class="col-sm-5">
                                                            <input name="correspondentbank[{{id}}][agency]"
                                                                   type="text"
                                                                   class="form-control"
                                                                   id="correspondentbank_{{id}}_agency"
                                                                   >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="correspondentbank_{{id}}_number"
                                                               class="col-sm-3">Conta</label>
                                                        <div class="col-sm-5">
                                                            <input name="correspondentbank[{{id}}][number]"
                                                                   type="text"
                                                                   class="form-control"
                                                                   id="correspondentbank_{{id}}_number"
                                                                   >
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                                <?php if (isset($data->correspondentbank) && count($data->correspondentbank)):
                                                    $n = 1;
                                                    foreach ($data->correspondentbank as $row):
                                                        $id = $n;
                                                        ?>
                                                        <div id="item-<?php echo $id; ?>" class="item">
                                                            <h5><i>Dados Bancários - <?php echo $id; ?></i> <a
                                                                    href="#item-remove"
                                                                    onclick="remove_template_item(this); return false;"
                                                                    class="pull-right text-danger item-remove <?php echo ($me->user_type == 2)? '' : 'hidden';?>"><i
                                                                        class="fa fa-close"></i></a></h5>
                                                            <div class="form-group">
                                                                <label
                                                                    for="correspondentbank_<?php echo $id; ?>_owner"
                                                                    class="col-sm-3">Titular</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="correspondentbank[<?php echo $id; ?>][owner]"
                                                                        type="text"
                                                                        class="form-control"
                                                                        id="correspondentbank_<?php echo $id; ?>_owner"
                                                                        value="<?php echo $row->owner; ?>"  <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="correspondentbank_<?php echo $id; ?>_cpf"
                                                                    class="col-sm-3">CPF</label>
                                                                <div class="col-sm-5">
                                                                    <input
                                                                        name="correspondentbank[<?php echo $id; ?>][cpf]"
                                                                        type="text"
                                                                        class="form-control"
                                                                        id="correspondentbank_<?php echo $id; ?>_cpf"
                                                                        value="<?php echo $row->cpf; ?>"  <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="correspondentbank_<?php echo $id; ?>_type"
                                                                    class="col-sm-3">Tipo da Conta</label>
                                                                <div class="col-sm-5">
                                                                    <select <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                                                                        name="correspondentbank[<?php echo $id; ?>][type]"
                                                                        class="form-control">
                                                                        <option
                                                                            value="Corrente" <?php echo ($row->type == "Corrente") ? "selected" : ""; ?>>
                                                                            Corrente
                                                                        </option>
                                                                        <option
                                                                            value="Poupança" <?php echo ($row->type == "Poupança") ? "selected" : ""; ?>>
                                                                            Poupança
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="correspondentbank_<?php echo $id; ?>_name"
                                                                    class="col-sm-3">Banco</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="correspondentbank[<?php echo $id; ?>][name]"
                                                                        type="text"
                                                                        class="form-control"
                                                                        id="correspondentbank_<?php echo $id; ?>_name"
                                                                        value="<?php echo $row->name; ?>"  <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="correspondentbank_<?php echo $id; ?>_agency"
                                                                    class="col-sm-3">Agência</label>
                                                                <div class="col-sm-4">
                                                                    <input
                                                                        name="correspondentbank[<?php echo $id; ?>][agency]"
                                                                        type="text"
                                                                        class="form-control"
                                                                        id="correspondentbank_<?php echo $id; ?>_agency"
                                                                        value="<?php echo $row->agency; ?>"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="correspondentbank_<?php echo $id; ?>_number"
                                                                    class="col-sm-3">Conta</label>
                                                                <div class="col-sm-4">
                                                                    <input
                                                                        name="correspondentbank[<?php echo $id; ?>][number]"
                                                                        type="text"
                                                                        class="form-control"
                                                                        id="correspondentbank_<?php echo $id; ?>_number"
                                                                        value="<?php echo $row->number; ?>"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                        </div>
                                                        <?php
                                                        $n++;
                                                    endforeach;

                                                endif; ?>
                                            </div>
                                            <div class="clearfix"></div>
                                            <a href="#plus-template" class="plus-template pull-right <?php echo ($me->user_type == 2)? '' : 'hidden';?>">+</a>
                                            <div class="clearfix"></div>
                                        </div>
                                        <p>&nbsp;</p>
                                        <?php if (isset($data->id) && $data->id): ?>
                                            <h4><a href="./usuarios-correspondente/<?php echo $data->id; ?>"
                                                   target="_blank">Substabelecimento  <i
                                                        class="fa fa-external-link" aria-hidden="true"></i></a>
                                            </h4>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <h3>Cadastro de preços</h3>
                                <p>&nbsp;</p>
                                <h4>Áreas de Direito</h4>
                                <hr>
                                <div class="form-group">
                                    <div class="lawareas-item-list">
                                        <?php foreach ($data->lawareas as $key => $row): ?>
                                            <label>
                                                <input type="checkbox" name="lawareas_id[]"
                                                       value="<?php echo $row->id; ?>" <?php echo (in_array($row->id, $data->correspondentlawareas)) ? 'checked' : '' ?> <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                <i class="fa icon-check"></i>
                                                <?php echo $row->name; ?>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <p>&nbsp;</p>
                                <h4>Despesas por Tipo de Serviço</h4>
                                <hr>
                                <div class="form-group">
                                    <div class="lawareas-item-list">
                                        <?php foreach ($data->coststypeservice as $key => $row): ?>
                                            <label>
                                                <input type="checkbox" name="coststypeservice_id[]"
                                                       value="<?php echo $row->id; ?>" <?php echo (in_array($row->id, $data->correspondentcoststypeservice)) ? 'checked' : '' ?> <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                <i class="fa icon-check"></i>
                                                <?php echo $row->name; ?>
                                            </label>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <?php if (isset($data->honorarytypeservice) && count($data->honorarytypeservice) > 0): ?>
                                    <p>&nbsp;</p>
                                    <h4>Honorários por Tipo de Serviço</h4>
                                    <p>&nbsp;</p>
                                    <div class="form-group">
                                        <div class="col-xs-6 col-sm-4 <?php echo ($me->user_type == 2)? '' : 'hidden';?>" style="padding-left: 15px;">
                                            <label for="honorarytypeservice-city">Grupos de cidades</label>
                                            <div class="">
                                                <select id="groupcity" class="form-control" onchange="group_city()" data-id="<?php echo isset($data->id)? $data->id : 0; ?>" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                    <option value="0">Todos</option>
                                                    <?php
                                                    $citygroup = array();

                                                    foreach ($data->citygroup as $row):
                                                        ?>
                                                        <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4 <?php echo ($me->user_type == 2)? '' : 'hidden';?>" style="padding-left: 15px;">
                                            <label for="honorarytypeservice-city">Cidades</label>
                                            <div class="input-group">
                                                <select id="honorarytypeservice-city" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                    <option value="all">Todas</option>
                                                    <?php
                                                    $cities = array();

                                                    foreach ($data->cities as $row):
                                                        $cities[$row->id] = $row->name . " - " . $row->country_abbreviation;
                                                        ?>
                                                        <option
                                                                value="<?php echo $row->id; ?>" <?php echo in_array($row->id, $data->cities_selected) ? 'disabled' : ''; ?>><?php echo $row->name . " - " . $row->country_abbreviation; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                        <button type="button" class="btn btn-default" id="add-city" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>Adicionar</button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <table class="table table-responsive" id="fee-table">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <?php foreach ($data->cities_selected as $city_id): ?>
                                                        <th class="col-city-<?php echo $city_id; ?>"
                                                            data-class=".col-city-<?php echo $city_id; ?>">
                                                            <?php echo (isset($cities[$city_id])) ? $cities[$city_id] : "ID Cidade:" . $city_id; ?>
                                                            <button type="button" class="remove-item-table <?php echo ($me->user_type == 2)? '' : 'hidden';?>"
                                                                    onclick="remove_item_table(<?php echo $city_id; ?>);">
                                                                <i
                                                                    class="fa fa-close"></i></button>
                                                        </th>
                                                    <?php endforeach; ?>
                                                    <?php if (isset($data->correspondentfee) && count($data->correspondentfee) > 0): ?>
                                                        <th class="col-note">Observações</th>
                                                    <?php endif; ?>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($data->honorarytypeservice as $key => $row): ?>
                                                    <tr id="honorarytypeservice-<?php echo $row->id; ?>"
                                                        data-id="<?php echo $row->id; ?>">
                                                        <td><?php echo $row->name; ?></td>
                                                        <?php foreach ($data->cities_selected as $city_id): ?>
                                                            <td class="col-city-<?php echo $city_id; ?>"
                                                                data-class=".col-city-<?php echo $city_id; ?>">
                                                                <div class="input-group">
                                                                <span class="input-group-addon pointer">
                                                                    R$
                                                                    <div class="action-menu">
                                                                        <ul>
                                                                            <li>
                                                                                <button type="button" class="btn btn-default screen-reader-text" onclick="automatic_value(this, 'all');">Action</button>
                                                                                <a href="javascript:;" data-toggle="modal" data-target="#automatic-value-confirm-modal">
                                                                                    Replicar este valor para toda a linha.
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <button type="button" class="btn btn-default screen-reader-text" onclick="automatic_value(this, 'empty');">Action</button>
                                                                                <a href="javascript:;" data-toggle="modal" data-target="#automatic-value-confirm-modal" >
                                                                                    Replicar o valor para os campos em branco .
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <button type="button" class="btn btn-default screen-reader-text" onclick="automatic_value(this, 'old');">Action</button>
                                                                                <a href="javascript:;" data-toggle="modal" data-target="#automatic-value-confirm-modal" >
                                                                                    Replicar o novo conteúdo para o mesmo os campos de mesmo valor.
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </span>
                                                                <input data-id="<?php echo $row->id; ?>"
                                                                       type="text" class="form-control money"
                                                                       data-old_value = "<?php echo isset($data->clientfee[$row->id]['city'][$city_id]['value']) ? $data->clientfee[$row->id]['city'][$city_id]['value'] : NULL; ?>"
                                                                       name="correspondentfee[<?php echo $row->id; ?>][city][<?php echo $city_id; ?>][value]"
                                                                       value="<?php echo isset($data->correspondentfee[$row->id]['city'][$city_id]['value']) ? $data->correspondentfee[$row->id]['city'][$city_id]['value'] : NULL; ?>"
                                                                    <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                                                                >
                                                                </div>
                                                            </td>
                                                        <?php endforeach; ?>
                                                        <?php if (isset($data->correspondentfee) && count($data->correspondentfee) > 0): ?>
                                                            <td class="col-note">
                                                                    <textarea class="form-control"
                                                                              name="correspondentfee[<?php echo $row->id; ?>][note]"
                                                                        <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                                                                    ><?php echo isset($data->correspondentfee[$row->id]['note']) ? $data->correspondentfee[$row->id]['note'] : ''; ?></textarea>
                                                            </td>
                                                        <?php endif; ?>
                                                    </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="pull-right <?php echo ($me->user_type == 2)? '' : 'hidden';?>" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                            <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                            <span><?php echo isset($data->id) ? 'salvar' : 'cadastrar'; ?></span>
                        </button>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</form>