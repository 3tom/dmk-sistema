<form
    action="./<?php echo $this->uri->segment(1); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL; ?>"
    method="post"
    class="form-horizontal" enctype="multipart/form-data">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default"
                         data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>-form">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo (isset($data->id) ? 'Edição' : 'Cadastro') . ' de Cliente'; ?>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4>Informações</h4>
                                            <hr>
                                        </div>
                                        <div class="col-lg-6 border-right">
                                            <div class="form-group">
                                                <label for="code" class="col-sm-3">Código do Cliente:</label>
                                                <div class="col-sm-4">
                                                    <input name="code" type="text" class="form-control"
                                                           value="<?php echo isset($data->code) ? $data->code : NULL; ?>"
                                                           id="code" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>

                                            <div class="form-group <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>">
                                                <label for="img" class="col-sm-3">Selecione uma imagem </label>
                                                <div class="col-sm-9">
                                                    <input name="img" type="file" class="form-control" id="img">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="name" class="col-sm-3">Cliente (Nome Fantasia)</label>
                                                <div class="col-sm-9">
                                                    <input name="name" type="text" class="form-control"
                                                           value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                                           id="name"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name" class="col-sm-3">Razão Social</label>
                                                <div class="col-sm-9">
                                                    <input name="companyname" type="text" class="form-control"
                                                           value="<?php echo isset($data->companyname) ? $data->companyname : NULL; ?>"
                                                           id="companyname"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cnpj" class="col-sm-3 col-md-3 col-lg-3">CNPJ</label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="cnpj" type="text" class="form-control"
                                                           value="<?php echo isset($data->cnpj) ? $data->cnpj : NULL; ?>"
                                                           id="cnpj"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="municipalregistration" class="col-sm-3 col-md-3 col-lg-3">Inscrição
                                                    Municipal</label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="municipalregistration" type="text" class="form-control"
                                                           value="<?php echo isset($data->municipalregistration) ? $data->municipalregistration : NULL; ?>"
                                                           id="municipalregistration"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="stateregistration" class="col-sm-3 col-md-3 col-lg-3">Inscrição
                                                    Estadual</label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="stateregistration" type="text" class="form-control"
                                                           value="<?php echo isset($data->stateregistration) ? $data->stateregistration : NULL; ?>"
                                                           id="stateregistration" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <p>&nbsp;</p>
                                            <h4>Endereço Matriz</h4>
                                            <hr>
                                            <div class="form-group">
                                                <label for="street" class="col-sm-3">Rua</label>
                                                <div class="col-sm-9">
                                                    <input name="street" type="text" class="form-control"
                                                           value="<?php echo isset($data->street) ? $data->street : NULL; ?>"
                                                           id="street" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="number" class="col-sm-3 col-md-3 col-lg-3">Nº</label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="number" type="text" class="form-control"
                                                           value="<?php echo isset($data->number) ? $data->number : NULL; ?>"
                                                           id="number"  <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="complement" class="col-sm-3">Compl.</label>
                                                <div class="col-sm-9">
                                                    <input name="complement" type="text" class="form-control"
                                                           value="<?php echo isset($data->complement) ? $data->complement : NULL; ?>"
                                                           id="complement" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cep" class="col-sm-3 col-md-3 col-lg-3">CEP</label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="cep" type="text" class="form-control"
                                                           value="<?php echo isset($data->cep) ? $data->cep : NULL; ?>"
                                                           id="cep"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="city" class="col-sm-3">Bairro</label>
                                                <div class="col-sm-9">
                                                    <input name="district" type="text" class="form-control"
                                                           value="<?php echo isset($data->district) ? $data->district : NULL; ?>"
                                                           id="district"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="city" class="col-sm-3">Cidade</label>
                                                <div class="col-sm-9">
                                                    <input name="city" type="text" class="form-control"
                                                           value="<?php echo isset($data->city) ? $data->city : NULL; ?>"
                                                           id="city"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="state" class="col-sm-3">Estado</label>
                                                <div class="col-sm-9">
                                                    <input name="state" type="text" class="form-control"
                                                           value="<?php echo isset($data->state) ? $data->state : NULL; ?>"
                                                           id="state"
                                                           <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <p>&nbsp;</p>
                                            <h4>Contato Principal</h4>
                                            <hr>
                                            <div class="form-group">
                                                <label for="maincontact" class="col-sm-3">Nome</label>
                                                <div class="col-sm-9">
                                                    <input name="maincontact" type="maincontact" class="form-control"
                                                           value="<?php echo isset($data->maincontact) ? $data->maincontact : NULL; ?>"
                                                           id="maincontact" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mainphone1"
                                                       class="col-sm-3 col-md-3 col-lg-3">Telefones</label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone1" type="text" class="form-control"
                                                           placeholder="Telefone 1"
                                                           value="<?php echo isset($data->mainphone1) ? $data->mainphone1 : NULL; ?>"
                                                           id="mainphone1" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone2" type="text" class="form-control"
                                                           placeholder="Telefone 2"
                                                           value="<?php echo isset($data->mainphone2) ? $data->mainphone2 : NULL; ?>"
                                                           id="mainphone2" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mainphone3" class="col-sm-3 col-md-3 col-lg-3"></label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone3" type="text" class="form-control"
                                                           placeholder="Telefone 3"
                                                           value="<?php echo isset($data->mainphone3) ? $data->mainphone3 : NULL; ?>"
                                                           id="mainphone3" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone4" type="text" class="form-control"
                                                           placeholder="Telefone 4"
                                                           value="<?php echo isset($data->mainphone4) ? $data->mainphone4 : NULL; ?>"
                                                           id="mainphone4" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mainphone5" class="col-sm-3 col-md-3 col-lg-3"></label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone5" type="text" class="form-control"
                                                           placeholder="Telefone 5"
                                                           value="<?php echo isset($data->mainphone4) ? $data->mainphone4 : NULL; ?>"
                                                           id="mainphone5" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>

                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone6" type="text" class="form-control"
                                                           placeholder="Telefone 6"
                                                           value="<?php echo isset($data->mainphone4) ? $data->mainphone4 : NULL; ?>"
                                                           id="mainphone6" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mainphone7" class="col-sm-3 col-md-3 col-lg-3"></label>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone7" type="text" class="form-control"
                                                           placeholder="Telefone 7"
                                                           value="<?php echo isset($data->mainphone4) ? $data->mainphone4 : NULL; ?>"
                                                           id="mainphone7" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                                <div class="col-sm-5 col-md-4 col-lg-4">
                                                    <input name="mainphone8" type="text" class="form-control"
                                                           placeholder="Telefone 8"
                                                           value="<?php echo isset($data->mainphone4) ? $data->mainphone4 : NULL; ?>"
                                                           id="mainphone8" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mainemail1" class="col-sm-3">E-mail</label>
                                                <div class="col-sm-9">
                                                    <input name="mainemail1" type="email" class="form-control"
                                                           value="<?php echo isset($data->mainemail1) ? $data->mainemail1 : NULL; ?>"
                                                           placeholder="E-mail 1"
                                                           id="mainemail1" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mainemail2" class="col-sm-3"></label>
                                                <div class="col-sm-9">
                                                    <input name="mainemail2" type="email" class="form-control"
                                                           value="<?php echo isset($data->mainemail2) ? $data->mainemail2 : NULL; ?>"
                                                           placeholder="E-mail 2"
                                                           id="mainemail2" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mainemail3" class="col-sm-3"></label>
                                                <div class="col-sm-9">
                                                    <input name="mainemail3" type="email" class="form-control"
                                                           value="<?php echo isset($data->mainemail3) ? $data->mainemail3 : NULL; ?>"
                                                           placeholder="E-mail 3"
                                                           id="mainemail3" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="site" class="col-sm-3">Site</label>
                                                <div class="col-sm-9">
                                                    <input name="site" type="text" class="form-control"
                                                           value="<?php echo isset($data->site) ? $data->site : NULL; ?>"
                                                           id="site" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                </div>
                                            </div>
                                            <p>&nbsp;</p>
                                        </div>
                                        <div class="col-lg-6 border-left">
                                            <div class="form-group">
                                                <label for="site" class="col-sm-3">Forma de Pagamento:</label>
                                                <div class="col-sm-9">
                                                    <?php
                                                    $taxinvoice_1 = TRUE;
                                                    $taxinvoice_0 = FALSE;

                                                    if (isset($data->taxinvoice) && !$data->taxinvoice) {
                                                        $taxinvoice_1 = FALSE;
                                                        $taxinvoice_0 = TRUE;
                                                    }
                                                    ?>
                                                    <div class="btn-group" data-toggle="buttons">
                                                        <label
                                                            class="btn btn-primary <?php echo $taxinvoice_1 ? 'active' : ''; ?>">
                                                            <input class="<?php echo ($me->user_type == 2)? '' : 'disabled'; ?>" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>
                                                                    type="radio" name="taxinvoice"
                                                                   value="1" <?php echo $taxinvoice_1 ? 'checked' : ''; ?>>
                                                            Com nota
                                                        </label>
                                                        <label
                                                            class="btn btn-primary <?php echo ($me->user_type == 2)? '' : 'disabled'; ?> <?php echo $taxinvoice_0 ? 'active' : ''; ?>">
                                                            <input <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>
                                                                    type="radio" name="taxinvoice"
                                                                   value="0" <?php echo $taxinvoice_0 ? 'checked' : ''; ?>>
                                                            Sem nota
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <p>&nbsp;</p>
                                            <h4>Responsáveis Financeiros</h4>
                                            <hr>
                                            <div class="template-loop">
                                                <div class="list-item">
                                                    <div id="item-{{id}}" class="item template">
                                                        <h5><i>Responsável Financeiro - {{id}}</i> <a
                                                                href="#item-remove"
                                                                onclick="remove_template_item(this); return false;"
                                                                class="pull-right text-danger item-remove"><i
                                                                    class="fa fa-close"></i></a></h5>

                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_name"
                                                                   class="col-sm-3">Nome</label>
                                                            <div class="col-sm-9">
                                                                <input name="clientfinancial[{{id}}][name]" type="text"
                                                                       class="form-control"
                                                                       id="clientfinancial_{{id}}_name">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_phone" class="col-sm-3">Telefone</label>
                                                            <div class="col-sm-4">
                                                                <input name="clientfinancial[{{id}}][phone]" type="text"
                                                                       class="form-control"
                                                                       id="clientfinancial_{{id}}_phone">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_email" class="col-sm-3">Email</label>
                                                            <div class="col-sm-9">
                                                                <input name="clientfinancial[{{id}}][email]"
                                                                       type="email" class="form-control"
                                                                       id="clientfinancial_{{id}}_email" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_street" class="col-sm-3">Rua</label>
                                                            <div class="col-sm-9">
                                                                <input name="clientfinancial[{{id}}][street]"
                                                                       type="text" class="form-control"
                                                                       id="clientfinancial_{{id}}_street" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_number" class="col-sm-3">Nº</label>
                                                            <div class="col-sm-4">
                                                                <input name="clientfinancial[{{id}}][number]"
                                                                       type="text" class="form-control"
                                                                       id="clientfinancial_{{id}}_number" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_complement"
                                                                   class="col-sm-3">Compl.</label>
                                                            <div class="col-sm-9">
                                                                <input name="clientfinancial[{{id}}][complement]"
                                                                       type="text" class="form-control"
                                                                       id="clientfinancial_{{id}}_complement" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_cep"
                                                                   class="col-sm-3">CEP</label>
                                                            <div class="col-sm-4">
                                                                <input name="clientfinancial[{{id}}][cep]" type="text"
                                                                       class="form-control"
                                                                       id="clientfinancial_{{id}}_cep" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_city" class="col-sm-3">Cidade</label>
                                                            <div class="col-sm-9">
                                                                <input name="clientfinancial[{{id}}][city]" type="text"
                                                                       class="form-control"
                                                                       id="clientfinancial_{{id}}_city" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_state" class="col-sm-3">Estado</label>
                                                            <div class="col-sm-9">
                                                                <input name="clientfinancial[{{id}}][state]" type="text"
                                                                       class="form-control"
                                                                       id="clientfinancial_{{id}}_state" >
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="clientfinancial_{{id}}_note" class="col-sm-3">Oberservação</label>
                                                            <div class="col-sm-9">
                                                                <textarea name="clientfinancial[{{id}}][note]"
                                                                          class="form-control"
                                                                          id="clientfinancial_{{id}}_note"
                                                                          ></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php foreach ($data->clientfinancial as $key => $row): ?>
                                                        <?php $key++; ?>
                                                        <div id="item-<?php echo $key; ?>" class="item">
                                                            <h5><i>Responsável Financeiro - <?php echo $key; ?></i> <a
                                                                    href="#item-remove"
                                                                    onclick="remove_template_item(this); return false;"
                                                                    class="pull-right text-danger item-remove"><i
                                                                        class="fa fa-close <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>"></i></a></h5>

                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_name"
                                                                       class="col-sm-3">Nome</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][name]"
                                                                        type="text"
                                                                        class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_name"
                                                                        value="<?php echo $row->name; ?>"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_phone"
                                                                       class="col-sm-3">Telefone</label>
                                                                <div class="col-sm-4">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][phone]"
                                                                        type="text" value="<?php echo $row->phone; ?>"
                                                                        class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_phone"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_email"
                                                                       class="col-sm-3">Email</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][email]"
                                                                        value="<?php echo $row->email; ?>"
                                                                        type="email" class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_email"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_street"
                                                                       class="col-sm-3">Rua</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][street]"
                                                                        value="<?php echo $row->street; ?>"
                                                                        type="text" class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_street"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_number"
                                                                       class="col-sm-3">Nº</label>
                                                                <div class="col-sm-4">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][number]"
                                                                        value="<?php echo $row->number; ?>"
                                                                        type="text" class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_number"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label
                                                                    for="clientfinancial_<?php echo $key; ?>_complement"
                                                                    class="col-sm-3">Compl.</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][complement]"
                                                                        value="<?php echo $row->complement; ?>"
                                                                        type="text" class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_complement"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_cep"
                                                                       class="col-sm-3">CEP</label>
                                                                <div class="col-sm-4">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][cep]"
                                                                        type="text" value="<?php echo $row->cep; ?>"
                                                                        class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_cep"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_city"
                                                                       class="col-sm-3">Cidade</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][city]"
                                                                        type="text" value="<?php echo $row->city; ?>"
                                                                        class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_city"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_state"
                                                                       class="col-sm-3">Estado</label>
                                                                <div class="col-sm-9">
                                                                    <input
                                                                        name="clientfinancial[<?php echo $key; ?>][state]"
                                                                        type="text" value="<?php echo $row->state; ?>"
                                                                        class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_state"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="clientfinancial_<?php echo $key; ?>_note"
                                                                       class="col-sm-3">Oberservação</label>
                                                                <div class="col-sm-9">
                                                                    <textarea
                                                                        name="clientfinancial[<?php echo $key; ?>][note]"
                                                                        class="form-control"
                                                                        id="clientfinancial_<?php echo $key; ?>_note"
                                                                         <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>><?php echo $row->note; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                                <div class="clearfix"></div>
                                                <a href="#plus-template" class="plus-template pull-right <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>+</a>
                                                <div class="clearfix"></div>
                                            </div>
                                            <p>&nbsp;</p>
                                            <?php if (isset($data->id) && $data->id): ?>
                                                <h4>
                                                    <a href="./responsavel-pelo-envio-de-processo/<?php echo $data->id; ?>"
                                                       target="_blank">Responsável pelo envio de Processos <i
                                                            class="fa fa-external-link" aria-hidden="true"></i></a>
                                                </h4>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <h3>Cadastro de preços</h3>
                                    <p>&nbsp;</p>
                                    <h4>Áreas de Direito</h4>
                                    <hr>
                                    <div class="form-group">
                                        <div class="lawareas-item-list">
                                            <?php foreach ($data->lawareas as $key => $row): ?>
                                                <label>
                                                    <input <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>
                                                            type="checkbox" name="lawareas_id[]"
                                                           value="<?php echo $row->id; ?>" <?php echo (in_array($row->id, $data->clientlawareas)) ? 'checked' : '' ?>>
                                                    <i class="fa icon-check"></i>
                                                    <?php echo $row->name; ?>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p>&nbsp;</p>
                                    <h4>Despesas por Tipo de Serviço</h4>
                                    <hr>
                                    <div class="form-group">
                                        <div class="lawareas-item-list">
                                            <?php foreach ($data->coststypeservice as $key => $row): ?>
                                                <label>
                                                    <input type="checkbox" name="coststypeservice_id[]"
                                                           value="<?php echo $row->id; ?>" <?php echo (in_array($row->id, $data->clientcoststypeservice)) ? 'checked' : '' ?> <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                    <i class="fa icon-check"></i>
                                                    <?php echo $row->name; ?>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <?php if (isset($data->honorarytypeservice) && count($data->honorarytypeservice) > 0): ?>
                                        <p>&nbsp;</p>
                                        <h4>Honorários por Tipo de Serviço</h4>
                                        <p>&nbsp;</p>
                                        <div class="form-group">
                                            <div class="col-xs-6 col-sm-4 <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>" style="padding-left: 15px;">
                                                <label for="groupcity">Grupos de cidades</label>
                                                <div class="">
                                                    <select id="groupcity" class="form-control" onchange="group_city()" data-id="<?php echo isset($data->id)? $data->id : 0; ?>" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                        <option value="0">Todos</option>
                                                        <?php
                                                        $citygroup = array();

                                                        foreach ($data->citygroup as $row):
                                                            ?>
                                                            <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-xs-6 col-sm-4 <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>" style="padding-left: 15px;">
                                                <label for="honorarytypeservice-city">Cidades</label>
                                                <div class="input-group">
                                                <select id="honorarytypeservice-city" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                                    <option value="all">Todas</option>
                                                    <?php
                                                    $cities = array();

                                                    foreach ($data->cities as $row):
                                                        $cities[$row->id] = $row->name . " - " . $row->country_abbreviation;
                                                        ?>
                                                        <option
                                                            value="<?php echo $row->id; ?>" <?php echo in_array($row->id, $data->cities_selected) ? 'disabled' : ''; ?>><?php echo $row->name . " - " . $row->country_abbreviation; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-default" id="add-city">Adicionar</button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <table class="table table-responsive" id="fee-table">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <?php foreach ($data->cities_selected as $city_id): ?>
                                                            <th class="col-city-<?php echo $city_id; ?>"
                                                                data-class=".col-city-<?php echo $city_id; ?>">
                                                                <?php echo (isset($cities[$city_id])) ? $cities[$city_id] : "ID Cidade:" . $city_id; ?>
                                                                <button type="button" class="remove-item-table <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>"
                                                                        onclick="remove_item_table(<?php echo $city_id; ?>);">
                                                                    <i
                                                                        class="fa fa-close"></i></button>
                                                            </th>
                                                        <?php endforeach; ?>
                                                        <?php if (isset($data->clientfee) && count($data->clientfee) > 0): ?>
                                                            <th class="col-note">Observações</th>
                                                        <?php endif; ?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($data->honorarytypeservice as $key => $row): ?>
                                                        <tr id="honorarytypeservice-<?php echo $row->id; ?>"
                                                            data-id="<?php echo $row->id; ?>">
                                                            <td><?php echo $row->name; ?></td>
                                                            <?php foreach ($data->cities_selected as $city_id): ?>
                                                                <td class="col-city-<?php echo $city_id; ?> input-group"
                                                                    data-class=".col-city-<?php echo $city_id; ?>">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon pointer">
                                                                            R$
                                                                            <div class="action-menu">
                                                                                <ul>
                                                                                    <li>
                                                                                        <button type="button" class="btn btn-default screen-reader-text" onclick="automatic_value(this, 'all');">Action</button>
                                                                                        <a href="javascript:;" data-toggle="modal" data-target="#automatic-value-confirm-modal">
                                                                                            Replicar este valor para toda a linha.
                                                                                        </a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <button type="button" class="btn btn-default screen-reader-text" onclick="automatic_value(this, 'empty');">Action</button>
                                                                                        <a href="javascript:;" data-toggle="modal" data-target="#automatic-value-confirm-modal" >
                                                                                            Replicar o valor para os campos em branco .
                                                                                        </a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <button type="button" class="btn btn-default screen-reader-text" onclick="automatic_value(this, 'old');">Action</button>
                                                                                        <a href="javascript:;" data-toggle="modal" data-target="#automatic-value-confirm-modal" >
                                                                                            Replicar o novo conteúdo para o mesmo os campos de mesmo valor.
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </span>
                                                                        <input data-id="<?php echo $row->id; ?>"
                                                                               data-old_value = "<?php echo isset($data->clientfee[$row->id]['city'][$city_id]['value']) ? $data->clientfee[$row->id]['city'][$city_id]['value'] : NULL; ?>"
                                                                            <?php echo ($me->user_type == 2)? '' : 'disable'; ?>
                                                                               type="text" class="form-control money"
                                                                               name="clientfee[<?php echo $row->id; ?>][city][<?php echo $city_id; ?>][value]"
                                                                               value="<?php echo isset($data->clientfee[$row->id]['city'][$city_id]['value']) ? $data->clientfee[$row->id]['city'][$city_id]['value'] : NULL; ?>"
                                                                            <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>
                                                                        >
                                                                    </div>
                                                                </td>
                                                            <?php endforeach; ?>
                                                            <?php if (isset($data->clientfee) && count($data->clientfee) > 0): ?>
                                                                <td class="col-note">
                                                                <textarea class="form-control"
                                                                          name="clientfee[<?php echo $row->id; ?>][note]" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>><?php echo isset($data->clientfee[$row->id]['note']) ? $data->clientfee[$row->id]['note'] : ''; ?></textarea>
                                                                </td>
                                                            <?php endif; ?>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="pull-right <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                                <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                                <span><?php echo isset($data->id) ? 'salvar' : 'cadastrar'; ?></span>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>