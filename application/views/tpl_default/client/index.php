<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-close"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lista de Clientes
                            <a href="./<?php echo $this->uri->segment(1) . '/novo/' . $this->uri->segment(2) ?>"
                               class="btn btn-xs btn-default">Adicionar +</a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th width="<?php echo ($me->user_type == 2)? '140' : '80' ?>">Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr>
                                    <td><?php echo $row->name; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a data-id="<?php echo $row->id ?>" href="javascript:change_status(<?php echo $row->id ?>)" class="btn btn-status <?php echo ($row->status == 0)? 'btn-danger' : 'btn-success' ; ?> <?php echo ($me->user_type == 2)? '' : 'hidden disabled' ?>"></a>
                                            <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo ($me->user_type == 2)? 'editar' : 'visualizar'; ?>/<?php echo ($this->uri->segment(1) == 'usuarios-correspondente') ? $this->uri->segment(2) . '/' : ''; ?><?php echo $row->id; ?>"
                                               class="btn btn-info"><i class="fa fa-edit"></i> <?php echo ($me->user_type == 2)? ' Editar' : ' Visualizar'; ?></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>