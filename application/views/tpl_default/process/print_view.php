<form class="form-horizontal">
    <div class="panel panel-default"
         data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>-form">
        <div class="panel-heading">
            <a href="processos/visualizar/<?php echo $data->id; ?>" class="btn btn-default pull-right hidden-print">Voltar</a>
            <h3 class="panel-title">Acompanhamento de processo</h3>
            
        </div>
        <div class="panel-body">
            <h4>Informações</h4>
            <hr>
            <table width="100%">
                <tr>
                    <td valign="top">
                        <div class="form-group">
                            <label class="col-sm-4">Cliente:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->client->name) ? $data->client->name : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Advogado Solicitante: </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->shippingprocess->name) ? $data->shippingprocess->name : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Data de Solicitação:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->date) ? $data->date : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Prazo Fatal:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->deadline) ? $data->deadline : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">VARA:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->judicialpanel->name) ? $data->judicialpanel->name : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Tipo de Serviço:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->honorarytypeservice->name) ? $data->honorarytypeservice->name : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Hora da Audência:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->hour) ? $data->hour : NULL; ?>">
                            </div>
                        </div>
                    </td>
                    <td valign="top">
                        <div class="form-group">
                            <label class="col-sm-4">Cod. ID:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->code) ? $data->code : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Número do Processo:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->number) ? $data->number : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Autor:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->author) ? $data->author : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Réu:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->defendant) ? $data->defendant : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Responsável:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->user->name) ? $data->user->name : NULL; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4">Advogado Correspondente:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" disabled="disabled"
                                       value="<?php echo isset($data->correspondent->name) ? $data->correspondent->name : NULL; ?>">
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 border-right">
                            <div class="form-group">
                                <label class="col-sm-4">Descrição:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" name="note" disabled><?php echo isset($data->note)? $data->note : null; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer text-center">
            <h4>Observações</h4>
        </div>
    </div>
</form>