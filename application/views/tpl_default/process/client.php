<form
        action="./<?=$this->uri->segment(1); ?>/<?=$this->uri->segment(2); ?>"
        method="get"
        class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3">Cliente</label>
                                <div class="col-sm-8">
                                    <select name="client_id" id="client_id" class="form-control">
                                        <option value="">Todos</option>
                                        <?php foreach ($client as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($filter->client_id) && $filter->client_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-2">Período</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input name="deadlinestart" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlinestart) ? $filter->deadlinestart : NULL; ?>"
                                                   id="deadlinestart">
                                        </div>
                                        <div class="col-sm-1">até</div>
                                        <div class="col-sm-5">
                                            <input name="deadlineend" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlineend) ? $filter->deadlineend : NULL; ?>"
                                                   id="deadlineend">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-search fa-2x" aria-hidden="true"></i>
                        <span>Filtrar</span>
                    </button>
                    <button type="submit" class="pull-right" value="true" name="excel">
                        <i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i>
                        <span>Excel</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-close"
                     data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Relatório para cliente</h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default panel-close"
                             data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                            <div class="panel-body">
                                <table class="table table-bordered" id="dataTable">
                                    <thead>
                                    <tr>
                                        <th>CLIENTE</th>
                                        <th>CODIGO/PASTA</th>
                                        <th>AUTOR</th>
                                        <th>RÉU</th>
                                        <th>NÚMERO DO PROCESSO</th>
                                        <th>COMARCA</th>
                                        <th>Honorários por Tipo de Serviço</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($list as $row): ?>
                                        <tr>
                                            <td><?= $row->client->name; ?></td>
                                            <td><?= $row->client->code; ?></td>
                                            <td><?= $row->author; ?></td>
                                            <td><?= $row->defendant; ?></td>
                                            <td><a href="<?= $row->link;?>" target="_blank"><?= $row->number; ?></a></td>
                                            <td><?= isset($row->client->city) ? $row->client->city : null; ?></td>
                                            <td><?= isset($row->honorarytypeservice->name) ? $row->honorarytypeservice->name : NULL; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>