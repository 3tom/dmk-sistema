<form
        action="./<?=$this->uri->segment(1); ?>"
        method="get"
        class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-2">Período</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <input name="deadlinestart" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlinestart) ? $filter->deadlinestart : NULL; ?>"
                                                   id="deadlinestart">
                                        </div>
                                        <div class="col-sm-1">até</div>
                                        <div class="col-sm-5">
                                            <input name="deadlineend" type="text" class="form-control date"
                                                   value="<?=isset($filter->deadlineend) ? $filter->deadlineend : NULL; ?>"
                                                   id="deadlineend">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-sm-2">Cliente</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <select name="client_id" id="client_id" class="form-control">
                                                <option value="">Todos</option>
                                                <?php foreach ($client as $row): ?>
                                                    <option value="<?php echo $row->id; ?>" <?=isset($filter->client_id) && $filter->client_id == $row->id ? 'selected' : NULL; ?>><?php echo $row->name.' - '.$row->code; ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-search fa-2x" aria-hidden="true"></i>
                        <span>Filtrar</span>
                    </button>
                    <button type="submit" class="pull-right" value="true" name="excel">
                        <i class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i>
                        <span>Excel</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Acompanhamento de entradas e balanço geral</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered dataTable">
                            <thead>
                            <tr>
                                <th>Status</th>
                                <th>Data recebimento</th>
                                <th>Cliente</th>
                                <th>Processo</th>
                                <th>Valor honorarios</th>
                                <th>Valor reembolso despesas cliente</th>
                                <th>Valor despesas correspondente</th>
                                <th>Honorário do correspondente</th>
                                <th>Nº nota fiscal</th>
                                <th>Data emissão nota fiscal</th>
                                <th>Valor imposto nota fiscal</th>
                                <th>Valor deduzido imposto</th>
                                <th>Lucratividade</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr class="<?=$row->signal; ?>">
                                    <td>
                                        <a data-toggle="modal" href="#charge_status<?=$row->id;?>">
                                            <?php echo isset($row->processstatus2->name) ? $row->processstatus2->name : null; ?>
                                        </a>
                                        <div class="modal fade" id="charge_status<?=$row->id?>">
                                            <form action="" method="POST" role="form">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title">Dados rápidos</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <input type="hidden" name="id" value="<?=$row->id?>">
                                                            <div class="form-group">
                                                                <label for="charge_status">Status</label>
                                                                <select name="charge_status" id="charge_status" class="form-control">
                                                                    <?php foreach ($processstatus2 as $cRow):?>
                                                                        <option value="<?=$cRow->id;?>" <?=isset($row->processstatus2_id) && $row->processstatus2_id == $cRow->id ? ' selected' : null;?>>
                                                                            <?=$cRow->name;?>
                                                                        </option>
                                                                    <?php endforeach;?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="receivedate">Data recebimento</label>
                                                                <input type="text" name="receivedate" class="form-control date" value="<?php echo isset($row->receivedate) ? $row->receivedate : null; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                            <button type="submit" class="btn btn-primary">Salvar</button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </form>
                                        </div><!-- /.modal -->
                                    </td>
                                    <td>
                                        <a data-toggle="modal" href="#charge_status<?=$row->id;?>">
                                            <?php echo isset($row->receivedate) ? $row->receivedate : null; ?>
                                        </a>
                                    </td>
                                    <td><?php echo isset($row->client->name) ? $row->client->name : null; ?> - <?php echo isset($row->client->code) ? $row->client->code : null; ?></td>
                                    <td>
                                        <a href="<?php echo $row->link; ?>">
                                            <?php echo $row->number; ?>
                                        </a>
                                    </td>
                                    <td><?php echo isset($row->clientfee) ? $row->clientfee: null; ?></td>
                                    <td><?php echo isset($row->clientcoststypeservice) ? $row->clientcoststypeservice: null; ?></td>
                                    <td><?php echo isset($row->correspondentcoststypeservice) ? $row->correspondentcoststypeservice: null; ?></td>
                                    <td><?php echo isset($row->correspondentfee) ? $row->correspondentfee: null; ?></td>
                                    <td></td>
                                    <td></td>
                                    <td><?php echo isset($row->taxinvoice) ? $row->taxinvoice: null; ?></td>
                                    <td><?php echo isset($row->clientliquidvalue) ? $row->clientliquidvalue: null; ?></td>
                                    <td><?php echo isset($row->profitability) ? $row->profitability: null; ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <td style="color: transparent;">zzzzzzzzzzzzzzzz</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><?php echo isset($total->clientfee) ? $total->clientfee: null; ?></td>
                                <td><?php echo isset($total->clientcoststypeservice) ? $total->clientcoststypeservice: null; ?></td>
                                <td><?php echo isset($total->correspondentcoststypeservice) ? $total->correspondentcoststypeservice: null; ?></td>
                                <td><?php echo isset($total->correspondentfee) ? $total->correspondentfee: null; ?></td>
                                <td></td>
                                <td></td>
                                <td><?php echo isset($total->taxinvoice) ? $total->taxinvoice: null; ?></td>
                                <td><?php echo isset($total->clientliquidvalue) ? $total->clientliquidvalue: null; ?></td>
                                <td><?php echo isset($total->profitability) ? $total->profitability: null; ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer" style="border-top: none;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>