<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Barra de Follow</h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-inline">
                            <legend>Filtro</legend>
                            <div class="form-group">
                                <label for="processcategory_id">Tipo de Processo</label>
                                <select name="processcategory_id" id="processcategory_id" class="form-control">
                                    <option value="">Todos</option>
                                    <?php foreach ($processcategory as $row):?>
                                        <option value="<?php echo $row->id; ?>" <?php echo isset($processcategory_id) && $processcategory_id == $row->id ? ' selected' : null; ?>><?php echo $row->name; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default">Filtrar</button>
                        </form>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered dataTable">
                            <thead>
                            <tr>
                                <th>Prazo Fatal</th>
                                <th>Status</th>
                                <th>Código do Cliente</th>
                                <th>Tipo de Serviço</th>
                                <th>Cidade</th>
                                <th>Correspondente</th>
                                <th>Autor</th>
                                <th>Réu</th>
                                <th>Responsavel pelo cadastro</th>
                                <th>Ver</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr class="<?php echo $row->signal; ?>">
                                    <td data-sort="<?php echo $row->timestamp; ?>"><?php echo $row->deadline; ?></td>
                                    <td><?php echo isset($row->processstatus->name) ? $row->processstatus->name : null; ?></td>
                                    <td><?php echo isset($row->client->name) ? $row->client->name : null; ?> - <?php echo isset($row->client->code) ? $row->client->code : null; ?></td>
                                    <td><?php echo isset($row->honorarytypeservice->name) ? $row->honorarytypeservice->name : null; ?></td>
                                    <td><?php echo isset($row->city->name) ? $row->city->name : null; ?> - <?php echo isset($row->city->country_abbreviation) ? $row->city->country_abbreviation : null; ?></td>
                                    <td><?php echo isset($row->correspondent->name) ? $row->correspondent->name : null; ?></td>
                                    <td><?php echo $row->author; ?></td>
                                    <td><?php echo $row->defendant; ?></td>
                                    <td><?php echo isset($row->user->name) ? $row->user->name : null; ?></td>
                                    <td>
                                        <a href="<?php echo $row->link; ?>">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer" style="border-top: none;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>