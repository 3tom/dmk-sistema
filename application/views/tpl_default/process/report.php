<form
        method="post"
        class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default"
                 data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title">Relatório</h3>
                    <h4>Cliente: <?php echo (isset($data->client_name))? $data->client_name: '' ?></h4>
                    <h4>Autor: <?php echo isset($data->author) ? $data->author : NULL; ?></h4>
                    <h4>Réu: <?php echo (isset($data->defendant))? $data->defendant: '' ?></h4>
                    <h4>Correspondente: <?php echo (isset($data->correspondent_name))? $data->correspondent_name: '' ?></h4>
                    <h4>Cidade: <?php echo (isset($data->city_name))? $data->city_name: '' ?></h4>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel-body">
                            <h3>Cliente</h3>
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Honorários: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->clientfee) ? $report->clientfee : NULL; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Despesas: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->clientcoststypeservice) ? $report->clientcoststypeservice : NULL; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Nota fiscal: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->taxinvoice) ? $report->taxinvoice : NULL; ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel-body">
                            <h3>Correspondente</h3>
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Honorários: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->correspondentfee) ? $report->correspondentfee : NULL; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Despesas: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->correspondentcoststypeservice) ? $report->correspondentcoststypeservice : NULL; ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <h3>Totalizador</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Entrada: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->in) ? $report->in : NULL; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Saida: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->out) ? $report->out : NULL; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="client_id" class="col-sm-3 col-md-3 col-lg-3">Receita: </label>
                                <div class="col-sm-7 col-md-5 col-lg-6 input-group">
                                    <span class="input-group-addon">R$</span>
                                    <input name="code" id="code" type="text" class="form-control money" value="<?php echo isset($report->result) ? $report->result : NULL; ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</form>