<form
    action="./<?php echo $this->uri->segment(1); ?>/salvar/<?php echo isset($data->id)? $data->id :null ;?>"
    method="post"
    class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default"
                 data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo (isset($data->id) ? 'Edição' : 'Cadastro') . ' de processos'; ?>
                    </h3>
                </div>


                <div class="panel-body">
                    <div class="form-group">
                        <label for="client_id" class="col-sm-3 col-md-3 col-lg-3"> Nome e Codigo do Cliente: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <select name="client_id" id="client_id" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?>onchange="add_shipping()" required>
                                <option ></option>
                                <?php foreach ($client as $row): ?>
                                    <option value="<?php echo $row->id; ?>" <?php if(isset($data)){echo ($row->id == $data->client_id)? 'selected': ''; }?>><?php echo $row->name.' - '.$row->code; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="shippingprocess_id" class="col-sm-3 col-md-3 col-lg-3">Advogado Solicitante: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <select name="shippingprocess_id" id="shippingprocess_id" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?> required>
                                <option></option>
                                <?php if(isset($data->shippingprocess)){
                                    foreach ($data->shippingprocess as $row):
                                ?>
                                        <option value="<?php echo $row->id ?>" <?php if(isset($data)){echo ($row->id == $data->shippingprocess_id)? 'selected': ''; }?>><?php echo $row->name; ?></option>
                                <?php endforeach; }?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label for="code" class="col-sm-3 col-md-3 col-lg-3"> Código/ID: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <input name="code" id="code" type="text" class="form-control"
                                   value="<?php echo isset($data->code) ? $data->code : NULL; ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="number" class="col-sm-3 col-md-3 col-lg-3"> Processo Número: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <input name="number" type="text" class="form-control"
                                   value="<?php echo isset($data->number) ? $data->number : NULL; ?>"
                                   id="number"
                                   required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="processcategory_id" class="col-sm-3 col-md-3 col-lg-3"> Tipo de processo: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <select name="processcategory_id" id="processcategory_id" class="form-control">
                                <?php foreach ($processcategory as $row):?>
                                    <option value="<?php echo $row->id; ?>" <?php echo (isset($data->processcategory_id) && $data->processcategory_id == $row->id)? 'selected' : ''; ?>><?php echo $row->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="author" class="col-sm-3 col-md-3 col-lg-3"> Autor: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <input name="author" type="text" class="form-control"
                                   value="<?php echo isset($data->author) ? $data->author : NULL; ?>"
                                   id="author"
                                    <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="city_id" class="col-sm-3 col-md-3 col-lg-3">Cidade: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <select name="city_id" id="city_id" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?> onchange="add_correspondent()">
                                <option></option>
                                <?php foreach ($city as $row): ?>
                                    <option value="<?php echo $row->id ?>" <?php if(isset($data)){echo ($row->id == $data->city)? 'selected': ''; }?>><?php echo $row->name.' - '.$row->country_abbreviation; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="correspondent_id" class="col-sm-3 col-md-3 col-lg-3">Correspondente: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <select name="correspondent_id" id="correspondent_id" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?> onchange="load_correspondent_lawareas()">
                                <option></option>
                                <?php if(isset($data->correspondents)){
                                    foreach ($data->correspondents as $row):
                                        ?>
                                        <option value="<?php echo $row->id ?>" <?php if(isset($data)){echo ($row->id == $data->correspondent_id)? 'selected': ''; }?>><?php echo $row->name; ?></option>
                                    <?php endforeach; }?>
                                <?php ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="date" class="col-sm-3 col-md-3 col-lg-3"> Data da solicitação: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <input name="date" type="text" class="form-control date"
                                   value="<?php echo (isset($data->date))? $data->date: '' ?>"
                                   id="date"
                                <?php echo (isset($data->id) && $data->id) ? NULL : ''; ?>
                                <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                            >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="hour" class="col-sm-3 col-md-3 col-lg-3">Horário da Audiência: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <input name="hour" type="text" class="form-control time"
                                   value="<?php echo (isset($data->hour))? $data->hour: '' ?>"
                                   id="hour" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="defendant" class="col-sm-3 col-md-3 col-lg-3"> RÉU </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <input name="defendant" type="text" class="form-control"
                                   value="<?php echo (isset($data->defendant))? $data->defendant: '' ?>"
                                   id="defendant"
                                    <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="judicialpanel_id" class="col-sm-3 col-md-3 col-lg-3"> Vara: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <select name="judicialpanel_id" id="judicialpanel_id" class="form-control">
                                <option ></option>
                                <?php foreach ($judicialpanel as $row): ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo isset($data->judicialpanel_id) && $row->id == $data->judicialpanel_id ? 'selected' : ''; ?>><?php echo $row->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="deadline" class="col-sm-3 col-md-3 col-lg-3"> Data Prazo Fatal: </label>
                        <div class="col-sm-6 col-md-5 col-lg-2">
                            <input name="deadline" type="text" class="form-control date"
                                   value="<?php echo (isset($data->deadline))? $data->deadline: '' ?>"
                                   id="deadline"
                                    <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="audience-with" class="col-sm-3 col-md-3 col-lg-3">Audiência com: </label>
                        <div class="col-sm-4 col-md-4 col-lg-1">
                            <label class="audience-with">Advogado:</label>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-2">
                            <input name="lawyer" type="text" class="form-control"
                                   value="<?php echo (isset($data->lawyer))? $data->lawyer: '' ?>"
                                   id="audience-with-name"
                                    <?php echo ($me->user_type == 2)? '' : 'disabled';?> placeholder="Nome">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3 col-md-3 col-lg-3"></div>
                        <div class="col-sm-4 col-md-4 col-lg-1">
                            <label class="audience-with">Preposto:</label>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-2">
                            <input name="preposed" type="text" class="form-control"
                                   value="<?php echo (isset($data->preposed))? $data->preposed: '' ?>"
                                   id=""
                                <?php echo ($me->user_type == 2)? '' : 'disabled';?> placeholder="Nome">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="note" class="col-sm-3 col-md-3 col-lg-3"> Descrição: </label>
                        <div class="col-sm-7 col-md-5 col-lg-6">
                            <textarea class="form-control" name="note"><?php echo isset($data->note)? $data->note : null; ?></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12" id="client_values">
                        <h3>Valores Cliente</h3>
                        <p>&nbsp;</p>
                        <div class="form-group">
                            <label for="client_lawarea_id" class="col-sm-3 col-md-3 col-lg-3">Área de Direito: </label>
                            <div class="col-sm-7 col-md-5 col-lg-6">
                                <select name="client_lawarea_id" id="client_lawarea_id" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?> >
                                    <option></option>
                                    <?php if(isset($data->client_lawarea)){
                                        foreach ($data->client_lawarea as $row):
                                            ?>
                                            <option value="<?php echo $row->id ?>" <?php if(isset($data)){echo ($row->id == $data->client_lawarea_id)? 'selected': ''; }?>><?php echo $row->name; ?></option>
                                        <?php endforeach; }?>
                                    <?php ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <h4>Despesas por Tipo de Serviço</h4>
                        <p></p>
                        <br>
                        <div class="form-group">
                            <?php foreach ($coststypeservice as $key => $row): ?>
                                <div class="col-md-12 coststypeservice">
                                    <div class="lawareas-item-list">
                                        <label>
                                            <?php echo isset($row->name)? $row->name : ''?>
                                        </label>
                                    </div>
                                    <div class="ol-sm-3 col-md-3 col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input data-id=""
                                                   type="text" class="form-control money"
                                                   name="client_coststypeservice[<?php echo isset($row->id)? $row->id : ''?>]"
                                                   value="<?php echo isset($data->clientcoststypeservice[$row->id])? $data->clientcoststypeservice[$row->id]->value: '';?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <span>COM REEMBOLSO</span>
                                        <label class="switch">
                                            <input type="checkbox" name="client_coststypeservice_refound[<?php echo isset($row->id)? $row->id : ''?>]" value="1" <?php if(isset($data->clientcoststypeservice[$row->id])){
                                                echo ($data->clientcoststypeservice[$row->id]->refound == 0)? '' : 'checked';
                                            } ?>>
                                            <div class="slider round"></div>
                                        </label>
                                        <span>SEM REEMBOLSO</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <p>&nbsp;</p>
                        <h4>Honorários por Tipo de Serviço</h4>
                        <p>&nbsp;</p>
                        <div class="form-group">

                            <table class="table table-responsive" id="clientfee-table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Automatico</th>
                                    <th width="16%">Avulso</th>
                                    <th class="col-note">Observações</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($honorarytypeservice as $key => $row): ?>
                                    <tr id="client_honorarytypeservice-<?php echo $row->id; ?>" data-id="<?php echo $row->id; ?>">
                                        <td><?php echo $row->name; ?></td>
                                        <td>
                                            <label class="switch">
                                                <input class="checkbox" type="checkbox" name="automatic_clientfee[<?php echo $row->id; ?>]" onchange="fee_automatic(this)" value="1" <?php echo (isset($data->clientfee[$row->id]->automatic) && $data->clientfee[$row->id]->automatic == 1)? 'checked' : '' ?>>
                                                <div class="slider round"></div>
                                            </label>
                                        </td>
                                        <td class="input-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">R$</span>
                                                <input data-id="<?php echo $row->id; ?>"
                                                       type="text" class="form-control money"
                                                       name="clientfee[<?php echo $row->id; ?>]"
                                                       value="<?php echo (isset($data->clientfee[$row->id]->value)) && $data->clientfee[$row->id]->automatic != 1 ? $data->clientfee[$row->id]->value: ''; ?>"
                                                    <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>
                                                    <?php echo (isset($data->clientfee[$row->id]->automatic) && $data->clientfee[$row->id]->automatic == 1 ? 'disabled': '')?>
                                                >
                                            </div>
                                        </td>
                                        <td class="col-note">
                                            <textarea class="form-control" name="clientfee-note[<?php echo $row->id; ?>]" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>><?php echo(isset($data->clientfeenote[$row->id]))? $data->clientfeenote[$row->id]->note : ''; ?></textarea>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-12" id="correspondent_values">
                        <h3>Valores Correspondente</h3>
                        <p>&nbsp;</p>
                        <div class="form-group">
                            <label for="correspondent_lawarea_id" class="col-sm-3 col-md-3 col-lg-3">Área de Direito: </label>
                            <div class="col-sm-7 col-md-5 col-lg-6">
                                <select name="correspondent_lawarea_id" id="correspondent_lawarea_id" class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                    <option></option>
                                    <?php if(isset($data->correspondent_lawarea)){
                                        foreach ($data->correspondent_lawarea as $row):
                                            ?>
                                            <option value="<?php echo $row->id ?>" <?php if(isset($data)){echo ($row->id == $data->correspondent_lawarea_id)? 'selected': ''; }?>><?php echo $row->name; ?></option>
                                        <?php endforeach; }?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <h4>Despesas por Tipo de Serviço</h4>
                        <p></p>
                        <div class="form-group">
                            <?php foreach ($coststypeservice as $key => $row): ?>
                                <div class="col-md-12 coststypeservice">
                                    <div class="lawareas-item-list">
                                        <label>
                                            <?php echo isset($row->name)? $row->name : ''?>
                                        </label>
                                    </div>
                                    <div class="ol-sm-3 col-md-3 col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input data-id=""
                                                   type="text" class="form-control money"
                                                   name="correspondent_coststypeservice[<?php echo isset($row->id)? $row->id : ''?>]"
                                                   value="<?php echo isset($data->correspondentcoststypeservice[$row->id])? $data->correspondentcoststypeservice[$row->id]->value: '';?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <span>COM REEMBOLSO</span>
                                        <label class="switch">
                                            <input type="checkbox" name="correspondent_coststypeservice_refound[<?php echo isset($row->id)? $row->id : ''?>]" value="1" <?php if(isset($data->correspondentcoststypeservice[$row->id])){
                                                echo ($data->correspondentcoststypeservice[$row->id]->refound == 0)? '' : 'checked';
                                            } ?>>
                                            <div class="slider round"></div>
                                        </label>
                                        <span>SEM REEMBOLSO</span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <hr>
                        <h4>Honorários por Tipo de Serviço</h4>
                        <p>&nbsp;</p>
                        <div class="form-group">
                        <table class="table table-responsive" id="correspondentfee-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Automatico</th>
                                <th width="16%">Avulso</th>
                                <th class="col-note">Observações</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($honorarytypeservice as $key => $row): ?>
                                <tr id="correspondent_honorarytypeservice-<?php echo $row->id; ?>" data-id="<?php echo $row->id; ?>">
                                    <td><?php echo $row->name; ?></td>
                                    <td>
                                        <label class="switch">
                                            <input type="checkbox" name="automatic_corespondentfee[<?php echo $row->id; ?>]" onchange="fee_automatic(this)" value="1" <?php echo (isset($data->correspondentfee[$row->id]->automatic) && $data->correspondentfee[$row->id]->automatic == 1)? 'checked' : '' ?>>
                                            <div class="slider round"></div>
                                        </label>
                                    </td>
                                    <td class="input-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input data-id="<?php echo $row->id; ?>"
                                                <?php echo ($me->user_type == 2)? '' : 'disable'; ?>
                                                   type="text" class="form-control money"
                                                   name="corespondentfee[<?php echo $row->id; ?>]"
                                                   value="<?php echo (isset($data->correspondentfee[$row->id]->value)) && $data->correspondentfee[$row->id]->automatic != 1 ? $data->correspondentfee[$row->id]->value: ''; ?>"
                                                <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>
                                                <?php echo (isset($data->correspondentfee[$row->id]->automatic) && $data->correspondentfee[$row->id]->automatic == 1 ? 'disabled': '')?>
                                            >
                                        </div>
                                    </td>
                                    <td class="col-note">
                                        <textarea class="form-control" name="corespondentfee-note[<?php echo $row->id; ?>]" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>><?php echo(isset($data->correspondentfeenote[$row->id]))? $data->correspondentfeenote[$row->id]->note : ''; ?></textarea>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                    <div class="ower">
                        <hr>
                        <h4>Responsavel pelo cadastro:</h4>
                        <div class="form-group">
                            <label for="user_id" class="col-sm-2">Usuário: </label>
                            <div class="col-sm-7">
                                <select name="user_id" id="user_id" class="form-control">
                                    <option></option>
                                    <?php foreach ($user as $row): ?>
                                        <option value="<?php echo $row->id ?>" <?php echo isset($data->user_id) && $row->id == $data->user_id ? 'selected' : null; ?>><?php echo $row->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right <?php echo ($me->user_type == 2)? '' : 'hidden';?>" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                        <span><?php echo isset($data->id) ? 'salvar' : 'cadastrar'; ?></span>
                    </button>
                    <?php if(isset($data->id)): ?>
                    <a href="./<?php echo $this->uri->segment(1); ?>/notificar/<?php echo isset($data->id)? $data->id :null ;?>" class="<?php echo ($me->user_type == 2)? '' : 'hidden';?>" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                        <i class="fa fa-mail-forward" aria-hidden="true"></i>
                        <span>Notificar correspondente</span>
                    </a>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- Modal -->
<div id="duplicate-alert" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Processo já cadastrado</h4>
            </div>
            <div class="modal-body">
                <p>Processo já cadastrado! Continuar cadastro? Sim ou não</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Sim</button>
                <a class="btn btn-danger" href="<?php base_url(); ?>">Não</a>
            </div>
        </div>

    </div>
</div>
