<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Cadastro de processos
                            <a href="./<?php echo $this->uri->segment(1) . '/novo/' . $this->uri->segment(2) ?>"
                               class="btn btn-xs btn-default <?php echo ($me->user_type == 2)? '' : 'hidden';?>">Adicionar +</a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Data da solicitação</th>
                                <th>Numero</th>
                                <th>Cliente</th>
                                <th>Correspondente</th>
                                <th width="<?php echo ($me->user_type == 2)? '260' : '80';?>">Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr>
                                    <td><?php echo $row->date; ?></td>
                                    <td><?php echo $row->number; ?></td>
                                    <td><?php echo $row->client_name; ?></td>
                                    <td><?php echo $row->correspodent_name; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo ($me->user_type == 2)? 'editar' : 'visualizar';?>/<?php echo $row->id; ?>"
                                               class="btn btn-info"><i class="fa fa-edit"></i><?php echo ($me->user_type == 2)? ' Editar' : ' Visualizar';?></a>
                                            <a href="./<?php echo $this->uri->segment(1); ?>/excluir/<?php echo $row->id; ?>"
                                               class="btn btn-danger <?php echo ($me->user_type == 2)? '' : 'hidden';?>"><i class="fa fa-trash"></i> Excluir</a>
                                            <a href="./<?php echo $this->uri->segment(1); ?>/relatorio/<?php echo $row->id; ?>"
                                               class="btn btn-default <?php echo ($me->user_type == 2)? '' : 'hidden';?>"><i class="fa fa-info"></i> Relatório</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer" style="border-top: none;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>