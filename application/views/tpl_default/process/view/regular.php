<form class="form-horizontal">
    <div class="panel panel-default"
         data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>-form">
        <div class="panel-heading">
            <h3 class="panel-title">Acompanhamento de processo</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4>Informações</h4>
                            <hr>
                        </div>
                        <div class="col-lg-6 border-right">
                            <div class="form-group">
                                <label class="col-sm-4">Cliente:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->client->name) ? $data->client->name : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Advogado Solicitante: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->shippingprocess->name) ? $data->shippingprocess->name : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Data de Solicitação:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->date) ? $data->date : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Prazo Fatal:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->deadline) ? $data->deadline : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">VARA:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->judicialpanel->name) ? $data->judicialpanel->name : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Tipo de Serviço:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->honorarytypeservice->name) ? $data->honorarytypeservice->name : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Hora da Audência:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->hour) ? $data->hour : NULL; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 border-left">
                            <div class="form-group">
                                <label class="col-sm-4">Cod. ID:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->code) ? $data->code : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Número do Processo:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->number) ? $data->number : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Autor:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->author) ? $data->author : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Réu:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->defendant) ? $data->defendant : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Responsável:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->user->name) ? $data->user->name : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Advogado Correspondente:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->correspondent->name) ? $data->correspondent->name : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Valor do Serviço Correspondente:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control money" disabled="disabled"
                                           value="<?php echo isset($data->processfee_totalized[2])? $data->processfee_totalized[2] : null; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4">Despesas Adicionais Correspoente:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" disabled="disabled"
                                           value="<?php echo isset($data->correspondentcoststypeservice_value) ? $data->correspondentcoststypeservice_value : NULL; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 border-right">
                            <div class="form-group">
                                <label class="col-sm-4">Descrição:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" name="note" disabled><?php echo isset($data->note)? $data->note : null; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Mensagens e arquivos.</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <h4 id="group-chat0">Histórico de MSG Correspondente</h4>
                <hr>
                <div class="chat-list">
                    <?php foreach ($processchat0 as $row) :?>
                        <div class="item">
                            <span class="date"><?php echo $row->date; ?></span> | <span class="time"><?php echo $row->time; ?></span> <span class="name"><?php echo $row->user_name; ?></span>:
                            <span class="text"><?php echo $row->text; ?></span>
                        </div>
                    <?php endforeach;?>
                </div>
                <form action="./<?php echo $this->uri->segment(1); ?>/mensagem/externa/<?php echo isset($data->id) ? $data->id : NULL; ?>"
                      method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <textarea required name="text" id="text" cols="30" rows="5" class="form-control" placeholder="Escreva seu texto aqui"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>
            </div>
            <div class="col-md-6 col-lg-3">
                <h4 id="group-chat1">Histórico de MSG Equipe DMK</h4>
                <hr>
                <div class="chat-list">
                    <?php foreach ($processchat1 as $row) :?>
                        <div class="item">
                            <span class="date"><?php echo $row->date; ?></span> | <span class="time"><?php echo $row->time; ?></span> <span class="name"><?php echo $row->user_name; ?></span>:
                            <span class="text"><?php echo $row->text; ?></span>
                        </div>
                    <?php endforeach;?>
                </div>
                <form action="./<?php echo $this->uri->segment(1); ?>/mensagem/interna/<?php echo isset($data->id) ? $data->id : NULL; ?>"
                      method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <textarea required name="text" id="text" cols="30" rows="5" class="form-control" placeholder="Escreva seu texto aqui"></textarea>
                    </div>

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-6">
                <div class="col-xs-12" id="group-file">
                    <h4>Arquivos do Processo</h4>
                    <hr>
                    <div class="form-group text-right">
                        <a class="btn btn-primary" data-toggle="modal" href="#modal-file">Novo arquivo</a>
                    </div>
                    <div class="modal fade" id="modal-file">
                        <form action="./<?php echo $this->uri->segment(1); ?>/arquivo/<?php echo isset($data->id) ? $data->id : NULL; ?>"
                              method="post" enctype="multipart/form-data">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title">Novo arquivo</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="description">Descrição</label>
                                            <input required type="text" class="form-control" name="description" id="description">
                                        </div>
                                        <div class="form-group">
                                            <label for="file">Arquivo</label>
                                            <input required type="file" class="form-control" name="file" id="file" accept=".pdf, .jpg, .doc, .docx, .png, .gif">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Enviar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                        </form>
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <table class="dataTable table table-bordered">
                    <thead>
                    <tr>
                        <th>Descrição</th>
                        <th>Nome do arquivo</th>
                        <th width="120">Data de inclusão</th>
                        <th width="170">Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($processfile as $row): ?>
                        <tr>
                            <td><?php echo $row->description; ?></td>
                            <td><?php echo $row->file_name; ?></td>
                            <td><?php echo $row->date; ?> | <?php echo $row->time; ?></td>
                            <td>
                                <div class="btn-group">
                                    <a href="<?php echo $row->file; ?>" download="<?php echo $row->file_name; ?>"
                                       class="btn btn-info"><i class="fa fa-eye"></i> Visualizar</a>
                                    <a href="./<?php echo $this->uri->segment(1); ?>/arquivo/excluir/<?php echo $row->id; ?>/<?php echo $row->hash; ?>"
                                       class="btn btn-info"><i class="fa fa-trash"></i> Excluir</a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<form action="./<?php echo $this->uri->segment(1); ?>/status/<?php echo $data->id; ?>"
      method="get" enctype="multipart/form-data" id="group-edit">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Status do processo.</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="col-sm-4" for="processstatus_id">Status: </label>
                        <div class="col-sm-7">
                            <select name="processstatus_id" id="processstatus_id" class="form-control">
                                <?php foreach ($processstatus as $row): ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo $data->processstatus_id == $row->id ? 'selected' : null; ?>>
                                        <?php echo $row->name; ?>
                                    </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <label class="col-sm-4" for="reason">Motivo: </label>
                        <div class="col-sm-7">
                            <textarea class="form-control" name="reason" id="reason"><?php echo isset($data->reason)? $data->reason : null; ?></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="hash" value="<?php echo $data->hash; ?>">
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button type="submit" class="pull-right">
                <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                <span>Salvar</span>
            </button>
            <div class="pull-right">
                <a href="./<?php echo $this->uri->segment(1); ?>/imprimir/<?php echo $data->id; ?>">
                    <button type="button" class="pull-right">
                        <i class="fa fa-print fa-2x" aria-hidden="true"></i>
                        <span>Imprimir</span>
                    </button>
                </a>
            </div>
            <a href="./<?php echo $this->uri->segment(1); ?>/notificar/<?php echo $data->id ;?>">
                <i class="fa fa-mail-forward" aria-hidden="true"></i>
                <span>Notificar correspondente</span>
            </a>
            <div class="clearfix"></div>
        </div>
    </div>
</form>
