<form
    action="./<?php echo $this->uri->segment(1); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL; ?>"
    method="post"
    class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo (isset($data->id) ? 'Edição' : 'Cadastro') . ' de Honorários por Tipo de Serviço'; ?>
                        <span class="toggle-panel pull-right">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </span>
                    </h3>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 col-md-2">Nome </label>
                        <div class="col-sm-9 col-md-6">
                            <input name="name" type="text" class="form-control"
                                   value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                   id="name"
                                   required>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                        <span><?php echo isset($data->id) ? 'salvar' : 'cadastrar'; ?></span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>