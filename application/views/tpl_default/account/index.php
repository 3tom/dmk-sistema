<form action="./<?php echo $this->uri->segment(1); ?>/salvar/" method="post" class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-default"
                         data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo ($me->user_type ==2)? 'Editar' : 'Visualizar' ?> dados
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6 border-right">
                                    <h4>Informações:</h4>
                                    <hr>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-3">Nome Fantasia</label>
                                        <div class="col-sm-9">
                                            <input name="name" type="text" class="form-control"
                                                   value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                                   id="name"
                                                   required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="companyname" class="col-sm-3">Razão Social</label>
                                        <div class="col-sm-9">
                                            <input name="companyname" type="text" class="form-control"
                                                   value="<?php echo isset($data->companyname) ? $data->companyname : NULL; ?>"
                                                   id="companyname" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cnpj" class="col-sm-3 ">CNPJ</label>
                                        <div class="col-sm-9">
                                            <input name="cnpj" type="text" class="form-control"
                                                   value="<?php echo isset($data->cnpj) ? $data->cnpj : NULL; ?>"
                                                   id="cnpj"
                                                   required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="municipalregistration" class="col-sm-3">Insc. Municipal</label>
                                        <div class="col-sm-9">
                                            <input name="municipalregistration" type="text" class="form-control"
                                                   value="<?php echo isset($data->municipalregistration) ? $data->municipalregistration : NULL; ?>"
                                                   id="municipalregistration" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="stateregistration" class="col-sm-3">Insc. Estadual</label>
                                        <div class="col-sm-9">
                                            <input name="stateregistration" type="text" class="form-control"
                                                   value="<?php echo isset($data->stateregistration) ? $data->stateregistration : NULL; ?>"
                                                   id="stateregistration"
                                                   required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="street" class="col-sm-3">Rua</label>
                                        <div class="col-sm-9">
                                            <input name="street" type="text" class="form-control"
                                                   value="<?php echo isset($data->street) ? $data->street : NULL; ?>"
                                                   id="street" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="number" class="col-sm-3">Nº</label>
                                        <div class="col-sm-4">
                                            <input name="number" type="text" class="form-control"
                                                   value="<?php echo isset($data->number) ? $data->number : NULL; ?>"
                                                   id="number" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="complement" class="col-sm-3">Compl.</label>
                                        <div class="col-sm-9">
                                            <input name="complement" type="text" class="form-control"
                                                   value="<?php echo isset($data->complement) ? $data->complement : NULL; ?>"
                                                   id="complement" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cep" class="col-sm-3">CEP</label>
                                        <div class="col-sm-4">
                                            <input name="cep" type="text" class="form-control"
                                                   value="<?php echo isset($data->cep) ? $data->cep : NULL; ?>" id="cep"
                                                   required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="col-sm-3">Cidade</label>
                                        <div class="col-sm-9">
                                            <input name="city" type="text" class="form-control"
                                                   value="<?php echo isset($data->city) ? $data->city : NULL; ?>"
                                                   id="city"
                                                   required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="state" class="col-sm-3">Estado</label>
                                        <div class="col-sm-9">
                                            <input name="state" type="text" class="form-control"
                                                   value="<?php echo isset($data->state) ? $data->state : NULL; ?>"
                                                   id="state"
                                                   required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <p>&nbsp;</p>
                                    <h4>Contato Principal:</h4>
                                    <hr>
                                    <div class="form-group">
                                        <label for="maincontact" class="col-sm-3">Nome</label>
                                        <div class="col-sm-9">
                                            <input name="maincontact" type="text" class="form-control"
                                                   value="<?php echo isset($data->maincontact) ? $data->maincontact : NULL; ?>"
                                                   id="maincontact" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mainphone1" class="col-sm-3 ">Telefones</label>
                                        <div class="col-sm-4 ">
                                            <input name="mainphone1" type="text" class="form-control"
                                                   placeholder="Telefone 1"
                                                   value="<?php echo isset($data->mainphone1) ? $data->mainphone1 : NULL; ?>"
                                                   id="mainphone1" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <input name="mainphone2" type="text" class="form-control"
                                                   placeholder="Telefone 2"
                                                   value="<?php echo isset($data->mainphone2) ? $data->mainphone2 : NULL; ?>"
                                                   id="mainphone2" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mainphone3" class="col-sm-3"></label>
                                        <div class="col-sm-4">
                                            <input name="mainphone3" type="text" class="form-control"
                                                   placeholder="Telefone 3"
                                                   value="<?php echo isset($data->mainphone3) ? $data->mainphone3 : NULL; ?>"
                                                   id="mainphone3" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <input name="mainphone4" type="text" class="form-control"
                                                   placeholder="Telefone 4"
                                                   value="<?php echo isset($data->mainphone4) ? $data->mainphone4 : NULL; ?>"
                                                   id="mainphone4" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mainphone5" class="col-sm-3 "></label>
                                        <div class="col-sm-4 ">
                                            <input name="mainphone5" type="text" class="form-control"
                                                   placeholder="Telefone 5"
                                                   value="<?php echo isset($data->mainphone5) ? $data->mainphone5 : NULL; ?>"
                                                   id="mainphone5" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <input name="mainphone6" type="text" class="form-control"
                                                   placeholder="Telefone 6"
                                                   value="<?php echo isset($data->mainphone6) ? $data->mainphone6 : NULL; ?>"
                                                   id="mainphone6" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mainphone7" class="col-sm-3"></label>
                                        <div class="col-sm-4">
                                            <input name="mainphone7" type="text" class="form-control"
                                                   placeholder="Telefone 7"
                                                   value="<?php echo isset($data->mainphone7) ? $data->mainphone7 : NULL; ?>"
                                                   id="mainphone7" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <input name="mainphone8" type="text" class="form-control"
                                                   placeholder="Telefone 8"
                                                   value="<?php echo isset($data->mainphone8) ? $data->mainphone8 : NULL; ?>"
                                                   id="mainphone8" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mainemail1" class="col-sm-3">E-mail</label>
                                        <div class="col-sm-9">
                                            <input name="mainemail1" type="email" class="form-control"
                                                   value="<?php echo isset($data->mainemail1) ? $data->mainemail1 : NULL; ?>"
                                                   id="mainemail1" required placeholder="E-mail 1" <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mainemail2" class="col-sm-3"></label>
                                        <div class="col-sm-9">
                                            <input name="mainemail2" type="email" class="form-control"
                                                   value="<?php echo isset($data->mainemail2) ? $data->mainemail2 : NULL; ?>"
                                                   id="mainemail2" placeholder="E-mail 2" <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                                            >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mainemail3" class="col-sm-3"></label>
                                        <div class="col-sm-9">
                                            <input name="mainemail3" type="email" class="form-control"
                                                   value="<?php echo isset($data->mainemail3) ? $data->mainemail3 : NULL; ?>"
                                                   id="mainemail3" placeholder="E-mail 3" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="site" class="col-sm-3">Site</label>
                                        <div class="col-sm-9">
                                            <input name="site" type="text" class="form-control"
                                                   value="<?php echo isset($data->site) ? $data->site : NULL; ?>"
                                                   id="site" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <p>&nbsp;</p>
                                    <div class="form-group">
                                        <label for="note" class="col-sm-3">Observações:</label>
                                        <div class="col-sm-9">
                                            <textarea name="note" type="text"
                                                      class="form-control" <?php echo ($me->user_type == 2)? '' : 'disabled';?> ><?php echo isset($data->note) ? $data->note : NULL; ?></textarea>
                                        </div>
                                    </div>
                                    <p>&nbsp;</p>
                                </div>
                                <div class="col-lg-6 border-left">
                                    <h4><a href="./usuarios" target="_blank">Colaboradores <i
                                                class="fa fa-external-link" aria-hidden="true"></i></a></h4>
                                    <p>&nbsp;</p>
                                    <h4>Dados Bancários:</h4>
                                    <hr>
                                    <div class="template-loop">
                                        <div class="list-item">
                                            <div id="item-{{id}}" class="item template">
                                                <h5><i>Dados Bancários - {{id}}</i> <a href="#item-remove"
                                                                                       onclick="remove_template_item(this); return false;"
                                                                                       class="pull-right text-danger item-remove"><i
                                                            class="fa fa-close"></i></a></h5>
                                                <div class="form-group">
                                                    <label for="bankaccount_{{id}}_owner"
                                                           class="col-sm-3">Titular</label>
                                                    <div class="col-sm-9">
                                                        <input name="bankaccount[{{id}}][owner]" type="text"
                                                               class="form-control"
                                                               id="bankaccount_{{id}}_owner" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="bankaccount_{{id}}_cpf" class="col-sm-3">CPF</label>
                                                    <div class="col-sm-5">
                                                        <input name="bankaccount[{{id}}][cpf]" type="text"
                                                               class="form-control"
                                                               id="bankaccount_{{id}}_cpf" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="bankaccount_{{id}}_type" class="col-sm-3">Tipo da
                                                        Conta</label>
                                                    <div class="col-sm-5">
                                                        <select name="bankaccount[{{id}}][type]" class="form-control">
                                                            <option value="Corrente">Corrente</option>
                                                            <option value="Poupança">Poupança</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="bankaccount_{{id}}_name" class="col-sm-3">Banco</label>
                                                    <div class="col-sm-9">
                                                        <input name="bankaccount[{{id}}][name]" type="text"
                                                               class="form-control"
                                                               id="bankaccount_{{id}}_name" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="bankaccount_{{id}}_agency"
                                                           class="col-sm-3 ">Agência</label>
                                                    <div class="col-sm-5">
                                                        <input name="bankaccount[{{id}}][agency]" type="text"
                                                               class="form-control" id="bankaccount_{{id}}_agency"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="bankaccount_{{id}}_number"
                                                           class="col-sm-3">Conta</label>
                                                    <div class="col-sm-5">
                                                        <input name="bankaccount[{{id}}][number]" type="text"
                                                               class="form-control" id="bankaccount_{{id}}_number"
                                                               required>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            <?php if (isset($data->bankaccount) && count($data->bankaccount)):
                                                $n = 1;
                                                foreach ($data->bankaccount as $row):
                                                    $id = $n;
                                                    ?>
                                                    <div id="item-<?php echo $id; ?>" class="item">
                                                        <h5><i>Dados Bancários - <?php echo $id; ?></i> <a
                                                                href="#item-remove"
                                                                onclick="remove_template_item(this); return false;"
                                                                class="pull-right text-danger item-remove <?php echo ($me->user_type == 2)? '' : 'hidden';?>"><i
                                                                    class="fa fa-close"></i></a></h5>
                                                        <div class="form-group">
                                                            <label for="bankaccount_<?php echo $id; ?>_owner"
                                                                   class="col-sm-3">Titular</label>
                                                            <div class="col-sm-9">
                                                                <input name="bankaccount[<?php echo $id; ?>][owner]"
                                                                       type="text"
                                                                       class="form-control"
                                                                       id="bankaccount_<?php echo $id; ?>_owner"
                                                                       value="<?php echo $row->owner; ?>" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="bankaccount_<?php echo $id; ?>_cpf"
                                                                   class="col-sm-3">CPF</label>
                                                            <div class="col-sm-5">
                                                                <input name="bankaccount[<?php echo $id; ?>][cpf]"
                                                                       type="text"
                                                                       class="form-control"
                                                                       id="bankaccount_<?php echo $id; ?>_cpf"
                                                                       value="<?php echo $row->cpf; ?>" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="bankaccount_<?php echo $id; ?>_type"
                                                                   class="col-sm-3">Tipo da Conta</label>
                                                            <div class="col-sm-5">
                                                                <select name="bankaccount[<?php echo $id; ?>][type]"
                                                                        class="form-control"
                                                                    <?php echo ($me->user_type == 2)? '' : 'disabled';?>
                                                                >
                                                                    <option
                                                                        value="Corrente" <?php echo ($row->type == "Corrente") ? "selected" : ""; ?>>
                                                                        Corrente
                                                                    </option>
                                                                    <option
                                                                        value="Poupança" <?php echo ($row->type == "Poupança") ? "selected" : ""; ?>>
                                                                        Poupança
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="bankaccount_<?php echo $id; ?>_name"
                                                                   class="col-sm-3">Banco</label>
                                                            <div class="col-sm-9">
                                                                <input name="bankaccount[<?php echo $id; ?>][name]"
                                                                       type="text"
                                                                       class="form-control"
                                                                       id="bankaccount_<?php echo $id; ?>_name"
                                                                       value="<?php echo $row->name; ?>" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="bankaccount_<?php echo $id; ?>_agency"
                                                                   class="col-sm-3">Agência</label>
                                                            <div class="col-sm-4">
                                                                <input
                                                                    name="bankaccount[<?php echo $id; ?>][agency]"
                                                                    type="text"
                                                                    class="form-control"
                                                                    id="bankaccount_<?php echo $id; ?>_agency"
                                                                    value="<?php echo $row->agency; ?>" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="bankaccount_<?php echo $id; ?>_number"
                                                                   class="col-sm-3">Conta</label>
                                                            <div class="col-sm-4">
                                                                <input
                                                                    name="bankaccount[<?php echo $id; ?>][number]"
                                                                    type="text"
                                                                    class="form-control"
                                                                    id="bankaccount_<?php echo $id; ?>_number"
                                                                    value="<?php echo $row->number; ?>" required <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    <?php
                                                    $n++;
                                                endforeach;

                                            endif; ?>
                                        </div>
                                        <div class="clearfix"></div>
                                        <a href="#plus-template" class="plus-template pull-right <?php echo ($me->user_type == 2)? '' : 'hidden';?>">+</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="pull-right <?php echo ($me->user_type == 2)? '' : 'hidden';?>" <?php echo ($me->user_type == 2)? '' : 'disabled';?>>
                                <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                                <span>Salvar</span>
                            </button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>