<form
        action="./<?php echo $this->uri->segment(1); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL; ?>"
        method="post"
        class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo (isset($data->id) ? 'Edição' : 'Cadastro') . ' de contatos'; ?> </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="shire" class="col-sm-3">Comarca</label>
                                <div class="col-sm-9">
                                    <select name="city_id" id="city_id" class="form-control">
                                        <?php foreach ($city as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($data->city_id) && $data->city_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3">Nome</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control"
                                           value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                           id="name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="seguiment" class="col-sm-3">Seguimento</label>
                                <div class="col-sm-9">
                                    <input name="seguiment" type="text" class="form-control"
                                           value="<?php echo isset($data->seguiment) ? $data->seguiment : NULL; ?>"
                                           id="seguiment">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="address" class="col-sm-3">Endereço</label>
                                <div class="col-sm-9">
                                    <input name="address" type="text" class="form-control"
                                           value="<?php echo isset($data->address) ? $data->address : NULL; ?>"
                                           id="address">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="contact" class="col-sm-3">Contato</label>
                                <div class="col-sm-9">
                                    <input name="contact" type="text" class="form-control"
                                           value="<?php echo isset($data->contact) ? $data->contact : NULL; ?>"
                                           id="contact">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="phone" class="col-sm-3">Telefone</label>
                                <div class="col-sm-9">
                                    <input name="phone" type="text" class="form-control"
                                           value="<?php echo isset($data->phone) ? $data->phone : NULL; ?>"
                                           id="phone">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="email" class="col-sm-3">E-mail</label>
                                <div class="col-sm-9">
                                    <input name="email" type="email" class="form-control"
                                           value="<?php echo isset($data->email) ? $data->email : NULL; ?>"
                                           id="email">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                        <span>Salvar</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<form
        action="./<?=$this->uri->segment(1); ?>"
        method="get"
        class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?=$this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-5">Comarca</label>
                                <div class="col-sm-7">
                                    <select name="city_id" id="city_id" class="form-control">
                                        <option value="">Todos</option>
                                        <?php foreach ($city as $row):?>
                                            <option value="<?=$row->id;?>" <?=isset($filter->city_id) && $filter->city_id == $row->id ? ' selected' : null;?>>
                                                <?=$row->name;?>
                                            </option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-search fa-2x" aria-hidden="true"></i>
                        <span>Filtrar</span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-close"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Agenda de contatos
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>Comarca</th>
                                <th>Nome</th>
                                <th>Seguimento</th>
                                <th>Endereço</th>
                                <th>Contato</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th width="185">Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr>
                                    <td><?php echo isset($city[$row->city_id]->name) ? $city[$row->city_id]->name : null; ?></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->seguiment; ?></td>
                                    <td><?php echo $row->address; ?></td>
                                    <td><?php echo $row->contact; ?></td>
                                    <td><?php echo $row->phone; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="./<?php echo $this->uri->segment(1); ?>/editar/<?php echo ($this->uri->segment(1) == 'usuarios-correspondente') ? $this->uri->segment(2) . '/' : ''; ?><?php echo $row->id; ?>"
                                               class="btn btn-info"><i class="fa fa-edit"></i> Editar</a>
                                            <a href="./<?php echo $this->uri->segment(1); ?>/excluir/<?php echo ($this->uri->segment(1) == 'usuarios-correspondente') ? $this->uri->segment(2) . '/' : ''; ?><?php echo $row->id; ?>"
                                               class="btn btn-danger"><i class="fa fa-trash"></i> Excluir</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>