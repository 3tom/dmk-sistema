<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lista de <?=isset($correspondent_id) && $correspondent_id > 0 ? 'substabelecimento' : 'usuários';?>
                            <a href="./<?php echo $this->uri->segment(1) . '/novo/' . $this->uri->segment(2) ?>"
                               class="btn btn-xs btn-default <?php echo ($me->user_type ==2)? '' : 'hidden' ?>">Adicionar +</a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th width="<?php echo ($me->user_type == 2)? '185' : '80' ?>">Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr>
                                    <td><?php echo $row->name; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="./<?php echo $this->uri->segment(1); ?>/<?php echo ($me->user_type == 2 || $row->id == $me->id) ? 'editar' : 'visualizar' ?>/<?php echo ($this->uri->segment(1) == 'usuarios-correspondente') ? $this->uri->segment(2) . '/' : ''; ?><?php echo $row->id; ?>"
                                               class="btn btn-info"><i class="fa fa-edit"></i><?php echo ($me->user_type == 2 || $row->id == $me->id)? ' Editar' : ' Visualizar' ?></a>
                                            <a href="./<?php echo $this->uri->segment(1); ?>/excluir/<?php echo ($this->uri->segment(1) == 'usuarios-correspondente') ? $this->uri->segment(2) . '/' : ''; ?><?php echo $row->id; ?>"
                                               class="btn btn-danger <?php echo ($me->user_type == 2)? '' : 'hidden' ?>"><i class="fa fa-trash"></i> Excluir</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>