<form
    action="./<?php echo $data->action; ?>"
    method="post"
    class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default"
                 data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?=isset($correspondent_id) && $correspondent_id > 0 ? 'Substabelecimento' : 'Usuários';?>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-6 border-right">

                        <div class="form-group <?php echo $me->user_type == 2 && !(isset($correspondent_id) && $correspondent_id > 0) ? '' : 'hidden' ?>">
                            <label for="user_type" class="col-sm-3 col-md-3 col-lg-3"> Tipo de usuario </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <select name="user_type" id="user_type" class="form-control" <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                                    <option value="1">Normal</option>
                                    <option value="2" <?php echo (isset($data->user_type) && $data->user_type == 2)? 'selected' : ''; ?>>Administrador</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group <?php echo $me->user_type == 2 && !(isset($correspondent_id) && $correspondent_id > 0) ? '' : 'hidden' ?>">
                            <label for="processcategory_id" class="col-sm-3 col-md-3 col-lg-3"> Tipo de processo </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <select name="processcategory_id" id="processcategory_id" class="form-control" <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                                    <option value="0">Todos</option>
                                    <?php foreach ($processcategory as $row):?>
                                        <option value="<?php echo $row->id; ?>" <?php echo (isset($data->processcategory_id) && $data->processcategory_id == $row->id)? 'selected' : ''; ?>><?php echo $row->name; ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="form-group">
                            <label for="name" class="col-sm-3 col-md-3 col-lg-3">Nome </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="name" type="text" class="form-control"
                                       value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                       id="name"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="department" class="col-sm-3 col-md-3 col-lg-3">Departamento </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="department" type="text" class="form-control"
                                       value="<?php echo isset($data->department) ? $data->department : NULL; ?>"
                                       id="department"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role" class="col-sm-3 col-md-3 col-lg-3">Cargo </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="role" type="text" class="form-control"
                                       value="<?php echo isset($data->role) ? $data->role : NULL; ?>"
                                       id="role"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 col-md-3 col-lg-3">Email </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="email" type="email" class="form-control"
                                       value="<?php echo isset($data->email) ? $data->email : NULL; ?>"
                                       id="email"
                                       <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 col-md-3 col-lg-3">Telefone </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="phone" type="text" class="form-control"
                                       value="<?php echo isset($data->phone) ? $data->phone : NULL; ?>"
                                       id="phone"
                                       <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cpf" class="col-sm-3 col-md-3 col-lg-3">CPF </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="cpf" type="text" class="form-control"
                                       value="<?php echo isset($data->cpf) ? $data->cpf : NULL; ?>"
                                       id="cpf"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="oab" class="col-sm-3 col-md-3 col-lg-3">OAB </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="oab" type="text" class="form-control"
                                       value="<?php echo isset($data->oab) ? $data->oab : NULL; ?>"
                                       id="oab"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rg" class="col-sm-3 col-md-3 col-lg-3">RG </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="rg" type="text" class="form-control"
                                       value="<?php echo isset($data->rg) ? $data->rg : NULL; ?>"
                                       id="rg"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="maritalstatus" class="col-sm-3 col-md-3 col-lg-3">Estado Civil </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <select name="maritalstatus" class="form-control" id="maritalstatus" required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                                    <option
                                        value="solteiro" <?php echo (isset($data->maritalstatus) && $data->maritalstatus == "") ? 'selected' : 'solteiro'; ?>>
                                        Solteiro
                                    </option>
                                    <option
                                        value="casado" <?php echo (isset($data->maritalstatus) && $data->maritalstatus == "") ? 'selected' : 'casado'; ?>>
                                        Casado
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="birth" class="col-sm-3 col-md-3 col-lg-3">Data de Nascimento </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="birth" type="text" class="form-control"
                                       value="<?php echo isset($data->birth) ? $data->birth : NULL; ?>"
                                       id="birth"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="admission" class="col-sm-3 col-md-3 col-lg-3">Data de Admissão</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="admission" type="text" class="form-control"
                                       value="<?php echo isset($data->admission) ? $data->admission : NULL; ?>"
                                       id="admission"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="form-group">
                            <label for="street" class="col-sm-3 col-md-3 col-lg-3">Rua</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="street" type="text" class="form-control"
                                       value="<?php echo isset($data->street) ? $data->street : NULL; ?>"
                                       id="street" required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="number" class="col-sm-3 col-md-3 col-lg-3">Nº</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="number" type="text" class="form-control"
                                       value="<?php echo isset($data->number) ? $data->number : NULL; ?>"
                                       id="number" required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="complement" class="col-sm-3 col-md-3 col-lg-3">Compl.</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="complement" type="text" class="form-control"
                                       value="<?php echo isset($data->complement) ? $data->complement : NULL; ?>"
                                       id="complement" <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cep" class="col-sm-3 col-md-3 col-lg-3">CEP</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="cep" type="text" class="form-control"
                                       value="<?php echo isset($data->cep) ? $data->cep : NULL; ?>"
                                       id="cep"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-sm-3">Bairro</label>
                            <div class="col-sm-9">
                                <input name="district" type="text" class="form-control"
                                       value="<?php echo isset($data->district) ? $data->district : NULL; ?>"
                                       id="district"
                                    <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-sm-3 col-md-3 col-lg-3">Cidade</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="city" type="text" class="form-control"
                                       value="<?php echo isset($data->city) ? $data->city : NULL; ?>"
                                       id="city"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="state" class="col-sm-3 col-md-3 col-lg-3">Estado</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="state" type="text" class="form-control"
                                       value="<?php echo isset($data->state) ? $data->state : NULL; ?>"
                                       id="state"
                                       required <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="form-group">
                            <label for="note" class="col-sm-3 col-md-3 col-lg-3">Observação</label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <textarea <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?> name="note" id="note" cols="30" rows="10" class="form-control" id="note"><?php echo isset($data->note) ? $data->note : NULL; ?></textarea>
                            </div>
                        </div>
                        <p> </p>
                        <div class="form-group <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'hidden'?>">
                            <label for="password" class="col-sm-3 col-md-3 col-lg-3"> Senha </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="psw" type="password" class="form-control"
                                       value=""
                                       id="password"
                                    <?php echo (isset($data->id) && $data->id) ? NULL : 'required'; ?>>
                            </div>
                        </div>
                        <div class="form-group <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'hidden'?>">
                            <label for="psw_confirm" class="col-sm-3 col-md-3 col-lg-3"> Confirmar Senha </label>
                            <div class="col-sm-9 col-md-8 col-lg-8">
                                <input name="psw_confirm" type="password" class="form-control"
                                       value=""
                                       id="psw_confirm"
                                    <?php echo (isset($data->id) && $data->id) ? NULL : 'required'; ?>>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 border-left">
                        <p>&nbsp;</p>
                        <h4>Dados Bancários:</h4>
                        <hr>
                        <div class="form-group">
                            <label for="bank_owner"
                                   class="col-sm-3">Titular</label>
                            <div class="col-sm-9">
                                <input name="bank_owner"
                                       type="text"
                                       class="form-control"
                                       id="bank_owner"
                                       value="<?php echo isset($bank->owner)? $bank->owner : null?>"
                                    <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank_cpf" class="col-sm-3">CPF /CNPJ</label>
                            <div class="col-sm-5">
                                <input name="bank_cpf" type="text"
                                       class="form-control"
                                       id="bank_cpf"
                                       value="<?php echo isset($bank->cpf)? $bank->cpf : null?>"
                                    <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank_type" class="col-sm-3">Tipo
                                da
                                Conta</label>
                            <div class="col-sm-5">
                                <select name="bank_type"
                                        class="form-control"
                                    <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>>
                                    <option value="Corrente">Corrente</option>
                                    <option value="Poupança" <?php echo (isset($bank->type) && $bank->type == "Poupança")? "selected" : null ?> >Poupança</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank_name" class="col-sm-3">Banco</label>
                            <div class="col-sm-9">
                                <input name="bank_name"
                                       type="text"
                                       class="form-control"
                                       id="bank_name"
                                       value="<?php echo isset($bank->name)? $bank->name : null?>"
                                    <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank_agency"
                                   class="col-sm-3 ">Agência</label>
                            <div class="col-sm-5">
                                <input name="bank_agency"
                                       type="text"
                                       class="form-control"
                                       id="bank_agency"
                                       value="<?php echo isset($bank->agency)? $bank->agency : null?>"
                                    <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>
                                       >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank_number"
                                   class="col-sm-3">Conta</label>
                            <div class="col-sm-5">
                                <input name="bank_number"
                                       type="text"
                                       class="form-control"
                                       id="bank_number"
                                       value="<?php echo isset($bank->number)? $bank->number : null?>"
                                    <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?>
                                       >
                            </div>
                        </div>
                        <input type="hidden" value="<?php echo isset($bank->name)? 1 : 0 ?>" name="bank_exist">
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'hidden'?>" <?php echo ($me->user_type == 2 || $data->id == $me->id)? '' : 'disabled'?> >
                        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                        <span><?php echo isset($data->id) ? 'salvar' : 'cadastrar'; ?></span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>