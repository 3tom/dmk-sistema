<form
    action="./<?php echo $this->uri->segment(1) . '/salvar/' . $this->uri->segment(3) . (isset($data->id) ? '/' . $data->id : ''); ?>"
    method="post"
    class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default"
                 data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo (isset($data->id) ? 'Edição' : 'Cadastro') . ''; ?>
                    </h3>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 col-md-3 col-lg-3">Nome </label>
                        <div class="col-sm-9 col-md-5 col-lg-3">
                            <input name="name" type="text" class="form-control"
                                   value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                   id="name"
                                   required <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-3 col-md-3 col-lg-3">Email </label>
                        <div class="col-sm-9 col-md-5 col-lg-3">
                            <input name="email" type="email" class="form-control"
                                   value="<?php echo isset($data->email) ? $data->email : NULL; ?>"
                                   id="email"
                                   required <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-3 col-md-3 col-lg-3">Telefone </label>
                        <div class="col-sm-9 col-md-5 col-lg-3">
                            <input name="phone" type="text" class="form-control"
                                   value="<?php echo isset($data->phone) ? $data->phone : NULL; ?>"
                                   id="phone"
                                   required <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right <?php echo ($me->user_type == 2)? '' : 'hidden'; ?>" <?php echo ($me->user_type == 2)? '' : 'disabled'; ?>>
                        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                        <span><?php echo isset($data->id) ? 'salvar' : 'cadastrar'; ?></span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>