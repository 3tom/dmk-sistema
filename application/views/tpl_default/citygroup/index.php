<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default panel-close"
                     data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                    <div class="panel-heading">
                        <h3 class="panel-title">Lista de Grupos de cidades
                            <a href="./<?php echo $this->uri->segment(1) . '/novo/' . $this->uri->segment(2) ?>"
                               class="btn btn-xs btn-default">Adicionar +</a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th width="185">Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row): ?>
                                <tr>
                                    <td><?php echo $row->name; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="./<?php echo $this->uri->segment(1); ?>/editar/<?php echo ($this->uri->segment(1) == 'usuarios-correspondente') ? $this->uri->segment(2) . '/' : ''; ?><?php echo $row->id; ?>"
                                               class="btn btn-info"><i class="fa fa-edit"></i> Editar</a>
                                            <a href="./<?php echo $this->uri->segment(1); ?>/excluir/<?php echo ($this->uri->segment(1) == 'usuarios-correspondente') ? $this->uri->segment(2) . '/' : ''; ?><?php echo $row->id; ?>"
                                               class="btn btn-danger"><i class="fa fa-trash"></i> Excluir</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>