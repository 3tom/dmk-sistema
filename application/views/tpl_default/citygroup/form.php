<form
    action="./<?php echo $this->uri->segment(1); ?>/salvar/<?php echo isset($data->id) ? $data->id : NULL; ?>"
    method="post"
    class="form-horizontal">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-close"
                 data-local-storage-id="<?php echo $this->router->class . '-' . $this->router->method; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo (isset($data->id) ? 'Edição' : 'Cadastro') . ' de Grupo de Cidade'; ?> </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 col-md-3">Nome</label>
                        <div class="col-sm-9 col-md-6 ">
                            <input name="name" type="text" class="form-control"
                                   value="<?php echo isset($data->name) ? $data->name : NULL; ?>"
                                   id="name"
                                   required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="country" class="col-sm-3 col-md-3">Cidades</label>
                        <div class="col-sm-9 col-md-6">
                            <select name="city_id[]" class="form-control" id="all-city" multiple required>
                                <?php foreach ($city as $row): ?>
                                    <option value="<?php echo $row->id; ?>" <?php if(isset($citygroup)) {foreach ($citygroup as $select){ echo ($select->city_id == $row->id) ? 'selected' : null;}; }?>><?php echo $row->name.' - '.$row->country_abbreviation; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="pull-right">
                        <i class="fa fa-check-square-o fa-2x" aria-hidden="true"></i>
                        <span><?php echo isset($data->id) ? 'salvar' : 'cadastrar'; ?></span>
                    </button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</form>