<body id="login">
<div class="panel-login">
    <form action="./login/auth" method="post">
        <div class="form-signin">
            <div class="col-xs-12">
                <h2 class="form-signin-heading">
                    <img src="./assets/images/logo.png" alt=" " class="img-responsive"/> DMK </h2>
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"
                                                                                         aria-hidden="true"></i></span>
                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->

                <div class="col-xs-12 forgot-password">
                    <div class="input-group">
                              <span class="input-group-addon" id="basic-addon1"><i class="fa fa-certificate"
                                                                                   aria-hidden="true"></i>
                              </span>

                        <input type="password" name="password" class="form-control" placeholder="Password"
                               required>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->

            </div><!-- /.row -->
            <div class="senha" id="forgot-password">
                <a href="#./login/auth" class="forgot-password-hide hide"> Voltar </a>
                <a href="#./login/recovery_password" class="forgot-password"> Esqueci a senha </a>
            </div>
            <button class="btn btn-lg btn-primary btn-block forgot-password" type="submit">Log In</button>
            <button class="btn btn-lg btn-primary btn-block forgot-password-hide hide" type="submit">
                Recuperar
            </button>
        </div>
    </form>
</div>