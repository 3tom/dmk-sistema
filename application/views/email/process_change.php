<p>Olá <?php echo $correspondent->name; ?>. O Sistema DMK Advogados Associados, acaba de receber uma nova solicitação.</p>
<p>
    <strong>Número do Processo: <?php echo isset($number) ? $number : null; ?> <br>
        Autor: <?php echo isset($author) ? $author : null; ?> <br>
        Réu: <?php echo isset($defendant) ? $defendant : null; ?> <br>
        Vara: <?php echo isset($judicialpanel->name) ? $judicialpanel->name : null; ?> <br>
        Cidade: <?php echo isset($city->name) ? $city->name : null; ?> <br>
        Tipo de Serviço: ????? <br>
        Prazo Fatal: <?php echo isset($deadline) ? $deadline : null; ?> <br>
        Responsável DMK: <?php echo isset($user->name) ? $user->name : null; ?> <br>
    </strong>
</p>

<p>
    Para ver mais detalhes e opções da solicitação, acesse o sistema. <br>
    Se preferir utilize o link: <a href="<?php echo $link; ?>">Clique Aqui</a>
</p>

<p>
    Cordialmente,<br>
    <strong>DMK ADVOGADOS ASSOCIADOS </strong><br>
    <a href="mailto:dmk@dmkadvogados.com.br">dmk@dmkadvogados.com.br</a><br>
    <a href="www.dmkadvogados.com.br">www.dmkadvogados.com.br</a><br>
    (48) 3207 0177 / 3207 1304<br>
    (48) 999345862 / 91308024 (VIVO)<br>
    R.Saldanha Marinho, nº.374, salas 805/806<br>
    Centro -Florianópolis/SC -CEP: 88010-450.<br>
    <img src="<?php echo base_url('assets/images/email-footer.png'); ?>">
</p>
 