<?php

$lang['email_not_found'] = "Não achei esse e-mail. Tem certeza que escreveu certo?";
$lang['wrong_password'] = "%s, a sua senha está errada.";

$lang['error_processing_form'] = "Ocorreu um erro ao processar o formulário, tente novamente mais tarde.";
$lang['error_processing_delete'] = "Ocorreu um erro ao excluir o item, tente novamente mais tarde.";

$lang['successfully_form'] = "Formulário enviado com sucesso.";

$lang['successfully_data'] = "Dados gravados com sucesso com sucesso.";

$lang['successfully_data_insert'] = "Dados inseridos com sucesso com sucesso.";

$lang['successfully_data_delete'] = "Item excluído com sucesso com sucesso.";

$lang['cannot_deleted'] = "Este item esta sendo usado e não pode ser excluído.";