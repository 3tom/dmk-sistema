<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login/recovery_password'] = 'login/recovery_password';

$route['conta'] = 'account';
$route['conta/salvar'] = 'account/save';

$route['cliente'] = 'client/index';
$route['cliente/editar/(:num)'] = 'client/form/$1';
$route['cliente/visualizar/(:num)'] = 'client/form/$1';
$route['cliente/novo'] = 'client/form/';
$route['cliente/salvar/?(:num)?'] = 'client/save/$1';
$route['cliente/relatorio'] = 'process/client';

$route['agenda-contato'] = 'contact/index';
$route['agenda-contato/editar/(:num)'] = 'contact/form/$1';
$route['agenda-contato/salvar/?(:num)?'] = 'contact/save/$1';
$route['agenda-contato/excluir/(:num)'] = 'contact/delete/$1';

$route['grupos-de-cidades'] = 'citygroup/index';
$route['grupos-de-cidades/editar/(:num)'] = 'citygroup/form/$1';
$route['grupos-de-cidades/novo'] = 'citygroup/form/';
$route['grupos-de-cidades/salvar/?(:num)?'] = 'citygroup/save/$1';
$route['grupos-de-cidades/excluir/(:num)'] = 'citygroup/delete/$1';

$route['status-despesas'] = 'chargestatus/index';
$route['status-despesas/editar/(:num)'] = 'chargestatus/form/$1';
$route['status-despesas/novo'] = 'chargestatus/form/';
$route['status-despesas/salvar/?(:num)?'] = 'chargestatus/save/$1';
$route['status-despesas/excluir/(:num)'] = 'chargestatus/delete/$1';

$route['categoria-despesas'] = 'chargecategory/index';
$route['categoria-despesas/editar/(:num)'] = 'chargecategory/form/$1';
$route['categoria-despesas/nova'] = 'chargecategory/form/';
$route['categoria-despesas/salvar/?(:num)?'] = 'chargecategory/save/$1';
$route['categoria-despesas/excluir/(:num)'] = 'chargecategory/delete/$1';

$route['tipo-despesas'] = 'chargetype/index';
$route['tipo-despesas/editar/(:num)'] = 'chargetype/form/$1';
$route['tipo-despesas/novo'] = 'chargetype/form/';
$route['tipo-despesas/salvar/?(:num)?'] = 'chargetype/save/$1';
$route['tipo-despesas/excluir/(:num)'] = 'chargetype/delete/$1';

$route['correspondente'] = 'correspondent/index';
$route['correspondente/editar/(:num)'] = 'correspondent/form/$1';
$route['correspondente/visualizar/(:num)'] = 'correspondent/form/$1';
$route['correspondente/novo'] = 'correspondent/form/';
$route['correspondente/salvar/?(:num)?'] = 'correspondent/save/$1';

$route['usuarios/editar/(:num)'] = 'user/form/0/$1';
$route['usuarios/visualizar/(:num)'] = 'user/form/0/$1';
$route['usuarios/excluir/(:num)'] = 'user/delete/0/$1';
$route['usuarios/novo'] = 'user/form/0/';
$route['usuarios/salvar/?(:num)?'] = 'user/save/0/$1';
$route['usuarios'] = 'user/index/0/';

$route['areas-de-direito/editar/(:num)'] = 'lawareas/form/$1';
$route['areas-de-direito/excluir/(:num)'] = 'lawareas/delete/$1';
$route['areas-de-direito/novo'] = 'lawareas/form';
$route['areas-de-direito/salvar/?(:num)?'] = 'lawareas/save/$1';
$route['areas-de-direito'] = 'lawareas/index';

$route['varas/editar/(:num)'] = 'judicialpanel/form/$1';
$route['varas/excluir/(:num)'] = 'judicialpanel/delete/$1';
$route['varas/nova'] = 'judicialpanel/form';
$route['varas/salvar/?(:num)?'] = 'judicialpanel/save/$1';
$route['varas'] = 'judicialpanel/index';

$route['honorarios-por-tipo-de-servico/editar/(:num)'] = 'honorarytypeservice/form/$1';
$route['honorarios-por-tipo-de-servico/excluir/(:num)'] = 'honorarytypeservice/delete/$1';
$route['honorarios-por-tipo-de-servico/novo'] = 'honorarytypeservice/form';
$route['honorarios-por-tipo-de-servico/salvar/?(:num)?'] = 'honorarytypeservice/save/$1';
$route['honorarios-por-tipo-de-servico'] = 'honorarytypeservice/index';

$route['despesas-por-tipo-de-servico/editar/(:num)'] = 'coststypeservice/form/$1';
$route['despesas-por-tipo-de-servico/excluir/(:num)'] = 'coststypeservice/delete/$1';
$route['despesas-por-tipo-de-servico/novo'] = 'coststypeservice/form';
$route['despesas-por-tipo-de-servico/salvar/?(:num)?'] = 'coststypeservice/save/$1';
$route['despesas-por-tipo-de-servico'] = 'coststypeservice/index';

$route['cidades/editar/(:num)'] = 'city/form/$1';
$route['cidades/excluir/(:num)'] = 'city/delete/$1';
$route['cidades/novo'] = 'city/form';
$route['cidades/salvar/?(:num)?'] = 'city/save/$1';
$route['cidades'] = 'city/index';

$route['usuarios/editar/(:num)'] = 'user/form/0/$1';
$route['usuarios/excluir/(:num)'] = 'user/delete/0/$1';
$route['usuarios/novo'] = 'user/form/0/';
$route['usuarios/salvar/?(:num)?'] = 'user/save/0/$1';
$route['usuarios'] = 'user/index/0/';

$route['usuarios-correspondente/editar/(:num)/(:num)'] = 'user/form/$1/$2';
$route['usuarios-correspondente/visualizar/(:num)/(:num)'] = 'user/form/$1/$2';
$route['usuarios-correspondente/excluir/(:num)/(:num)'] = 'user/delete/$1/$2';
$route['usuarios-correspondente/novo/(:num)'] = 'user/form/$1';
$route['usuarios-correspondente/salvar/(:num)/?(:num)?'] = 'user/save/$1/$2';
$route['usuarios-correspondente/(:num)'] = 'user/index/$1';

$route['responsavel-pelo-envio-de-processo/editar/(:num)/(:num)'] = 'shippingprocess/form/$1/$2';
$route['responsavel-pelo-envio-de-processo/visualizar/(:num)/(:num)'] = 'shippingprocess/form/$1/$2';
$route['responsavel-pelo-envio-de-processo/excluir/(:num)/(:num)'] = 'shippingprocess/delete/$1/$2';
$route['responsavel-pelo-envio-de-processo/novo/(:num)'] = 'shippingprocess/form/$1';
$route['responsavel-pelo-envio-de-processo/salvar/(:num)/?(:num)?'] = 'shippingprocess/save/$1/$2';
$route['responsavel-pelo-envio-de-processo/(:num)'] = 'shippingprocess/index/$1';

$route['processos/editar/(:num)'] = 'process/form/$1';
$route['processos/notificar/(:num)'] = 'process/notify/$1';
$route['processos/relatorio/(:num)'] = 'process/report/$1';
$route['processos/visualizar/(:num)'] = 'process/view/$1';
$route['processos/excluir/(:num)'] = 'process/delete/$1';
$route['processos/novo'] = 'process/form';
$route['processos/salvar/?(:num)?'] = 'process/save/$1';
$route['processos'] = 'process/index';
$route['processos/acompanhamento'] = 'process/follow';
$route['processos/mensagem/externa/(:num)'] = 'process/chat/$1/0';
$route['processos/mensagem/interna/(:num)'] = 'process/chat/$1/1';
$route['processos/arquivo/(:num)'] = 'process/file_save/$1/1';
$route['processos/status/(:num)'] = 'process/save_status/$1';
$route['processos/imprimir/(:num)'] = 'process/print_view/$1';
$route['processos/arquivo/excluir/(:num)/(:any)'] = 'process/file_delete/$1/$2';
$route['balanco-geral'] = 'process/balance';

$route['despesas/editar/(:num)'] = 'charge/form/$1';
$route['despesas/excluir/(:num)'] = 'charge/delete/$1';
$route['despesas/nova'] = 'charge/form';
$route['despesas/salvar/?(:num)?'] = 'charge/save/$1';
$route['despesas'] = 'charge/index';

$route['balanco-finaceiro'] = 'charge/report';

$route['meu-perfil'] = 'user/form/0';
$route['meu-perfil/salvar'] = 'user/save/0';

$route['adm'] = 'adm/home';
$route['adm/login'] = 'adm/login';
$route['adm/login/auth'] = 'adm/login/auth';

$route['adm/post'] = 'adm/post';
$route['adm/post/novo'] = 'adm/post/edit';
$route['adm/post/editar/(:num)'] = 'adm/post/edit/$1';
$route['adm/post/excluir/(:num)'] = 'adm/post/delete/$1';
$route['adm/post/salvar/?(:num)?'] = 'adm/post/record/$1';

$route['adm/admin'] = 'adm/admin';
$route['adm/admin/novo'] = 'adm/admin/edit';
$route['adm/admin/editar/(:num)'] = 'adm/admin/edit/$1';
$route['adm/admin/excluir/(:num)'] = 'adm/admin/delete/$1';
$route['adm/admin/salvar/?(:num)?'] = 'adm/admin/save/$1';