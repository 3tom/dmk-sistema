<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('city_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index()
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $this->load_datatable();

        $this->data['list'] = $this->city_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->setPageTitle('Lista de Cidades');

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($id = NULL)
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {
            if ($id) {
                $data = $this->city_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();

                $this->data['data'] = (count($data) > 0) ? current($data) : NULL;
            }
        }

        $this->load->model('country_model');

        $this->data['country'] = $this->country_model->get()->result();

        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de Cidade - ' . $this->data['data']->name : 'Cadastro de Cidade'));

        $this->renderer();
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type ==2 ) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Nome', 'trim|required');
            $this->form_validation->set_rules('country_id', 'Estado', 'trim|required');


            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());

                $redirect = ($id > 0) ? '/editar/' . $id : '/novo';

                redirect(base_url($this->uri->segment(1) . $redirect));

            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'country_id' => $this->input->post('country_id'),
                    'account_id' => $this->data['me']->account_id
                );

                if ($id > 0) {
                    $update = $this->city_model->update(array(
                        'id' => $id
                    ), $data);

                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }

                } else {
                    $id = $this->city_model->insert($data, TRUE);

                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }

    public function delete($id = NULL)
    {
        if ($id && $this->data['me']->user_type == 2) {

            $this->load->model(array('clientfee_model', 'correspondentfee_model'));

            $cannot_deleted = $this->clientfee_model->get(array(
                'city_id' => $id
            ))->result();

            $cannot_deleted = array_merge($cannot_deleted, $this->correspondentfee_model->get(array(
                'city_id' => $id



            ))->result());

            if (count($cannot_deleted) == 0) {
                if ($this->city_model->delete(array(
                    'id' => $id
                ))
                ) {
                    $this->setMsg($this->lang->line('successfully_data_delete'));
                } else {
                    $this->setError($this->lang->line('error_processing_delete'));
                }
            } else {
                $this->setError($this->lang->line('cannot_deleted'));
            }

        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }


        redirect(base_url($this->uri->segment(1)));
    }
}