<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientcoststypeservice extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('clientcoststypeservice_model');
        $this->load->model('coststypeservice_model');
        $this->lang->load('ws');
    }

    public function get()
    {
        $id = $this->input->post('id');

        if ($id === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {
            $data = $this->clientcoststypeservice_model->get(array('client_id' => $id))->result();
            foreach ($data as $key => $row){
                $lawareas = current($this->coststypeservice_model->get(array('id' => $row->coststypeservice_id))->result());
                $row->name = $lawareas->name;
                unset($row->client_id);
                $data[$key] = $row;
            }
            $this->data['output'][$this->router->class . 'result'] = $data;
        }
        $this->output_json($this->data['output']);
    }
}