<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Citygroup extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('city_model');
        $this->load->model('citycitygroup_model');
        $this->load->model('correspondentfee_model');
        $this->lang->load('ws');
    }

    public function get()
    {
        $group = $this->input->post('group_id');
        $id    = $this->input->post('id');

        if ($group === '' || $id === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }
        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {
            if($group == 0){
                $data = $this->city_model->get(array('account_id' => $this->data['me']->account_id))->result();
                foreach ($data as $key => $row){
                    $row->city_id = $row->id;
                    $data[$key] = $row;
                }
            }else{
                $data = $this->citycitygroup_model->get(array('citygroup_id' => $group))->result();
                if($id > 0){
                    $correspondentfee_get = $this->correspondentfee_model->get(array('correspondent_id' => $id))->result();
                    foreach ($correspondentfee_get as $row) {
                        foreach($data as $key => $city){
                            if($city->city_id == $row->city_id){
                                $city->selected = true;
                            }else{
                                $city->selected = false;
                            }
                            $data[$key] = $city;
                        }
                    }
                }
                foreach($data as $key => $row){
                    $city = current($this->city_model->get(array('id' => $row->city_id))->result());
                    $row->name = $city->name;
                    $row->country_abbreviation = $city->country_abbreviation;
                    $data[$key] = $row;
                }
            }
            $this->data['output'][$this->router->class . 'result'] = $data;
        }
        $this->output_json($this->data['output']);
    }
}