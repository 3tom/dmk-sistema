<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('process_model');
        $this->lang->load('ws');
    }

    public function get()
    {
        $where = array();

        if($this->input->post('number')) {
            $where['number'] = trim($this->input->post('number'));
        }

        if (count($where) === 0) {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {

            $data = $this->process_model->get($where)->result();
            $this->data['output'][$this->router->class . '_get'] = $data;
        }

        $this->output_json($this->data['output']);
    }
}