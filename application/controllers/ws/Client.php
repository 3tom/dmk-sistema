<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('client_model');
        $this->lang->load('ws');
    }

    public function status(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        if($id !== null && $status !==null && $this->data['me']->user_type == 2){
            $update = $this->client_model->update(array('id' => $id), array('status' => $status));
            if(!$update){
                $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
                $this->output_json($this->data['output']);
            }
        }

    }
    public function get()
    {
        $this->data['output'][$this->router->class . 'sent']['q'] = '';
        $q = trim(strtolower($this->input->post('q')));

        if ($q === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {

            $this->data['output'][$this->router->class . 'sent']['q'] = $q;

            $data = $this->client_model->get(array(
                'q' => $q,
                'account_id' => $this->data['me']->account_id
            ))->result();

            $this->data['output'][$this->router->class . 'result'] = $data;
        }

        $this->output_json($this->data['output']);
    }
}