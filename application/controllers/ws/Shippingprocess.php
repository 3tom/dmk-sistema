<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shippingprocess extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('shippingprocess_model');
        $this->lang->load('ws');
    }

    public function get()
    {
        $id = $this->input->post('id');

        if ($id === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {

            $data = $this->shippingprocess_model->get(array('client_id' => $id,'account_id' => $this->data['me']->account_id))->result();
            $this->data['output'][$this->router->class . 'result'] = $data;
        }

        $this->output_json($this->data['output']);
    }
}