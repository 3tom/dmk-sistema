<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientfee extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('clientfee_model');
        $this->lang->load('ws');
    }

    public function get()
    {
        $id = $this->input->post('id');
        $city = $this->input->post('city');

        if ($id === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if ($city === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {
            $data = $this->clientfee_model->get(array('client_id' => $id, 'city_id' => $city))->result();
            $this->data['output'][$this->router->class . 'result'] = $data;
        }
        $this->output_json($this->data['output']);
    }
}