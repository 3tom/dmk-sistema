<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Correspondentfee extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('correspondentfee_model');
        $this->lang->load('ws');
    }

    public function get()
    {
        $id = $this->input->post('id');

        if ($id === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {
            $this->load->model('correspondent_model');
            $data = $this->correspondentfee_model->get(array('city_id' => $id))->result();
            $already = array();
            $final = array();
            foreach ($data as $key => $row){
                if (!in_array($row->correspondent_id, $already)) {
                    $row2 = current($this->correspondent_model->get(array('id' => $row->correspondent_id, 'status' => 1))->result());
                    unset($data[$key]);
                    array_push($already,$row->correspondent_id);
                    if(!empty($row2)){
                        $final[] = $row2;
                    }
                }
            }
            $this->data['output'][$this->router->class . 'result'] = $final;
        }
        $this->output_json($this->data['output']);
    }

    public function get_fee(){
        $id = $this->input->post('id');
        $city = $this->input->post('city');

        if ($id === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }
        if ($city === '') {
            $this->data['output']['error'][1] = $this->lang->line('no_data_sent');
        }

        if (isset($this->data['output']['error']) == false || count($this->data['output']['error']) === 0) {
            $data = $this->correspondentfee_model->get(array('city_id' => $city, 'correspondent_id' => $id))->result();
            $this->data['output'][$this->router->class . 'result'] = $data;
        }

        $this->output_json($this->data['output']);

    }
}