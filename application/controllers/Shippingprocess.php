<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shippingprocess extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('shippingprocess_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index($client_id = FALSE)
    {
        $this->load_datatable();

        $client_id = (int)$client_id;

        $where = array(
            'account_id' => $this->data['me']->account_id,
            'client_id' => $client_id
        );

        $this->data['list'] = $this->shippingprocess_model->get($where)->result();

        $this->setPageTitle('Lista');

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($client_id = FALSE, $id = FALSE)
    {
        $client_id = (int)$client_id;

        $this->data['data'] = (object)array();

        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {

            if ($id > 0) {
                $data = $this->shippingprocess_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'client_id' => $client_id,
                    'id' => $id
                ))->result();

                $this->data['data'] = (count($data) > 0) ? current($data) : NULL;
            }
        }

        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição - ' . $this->data['data']->name : 'Cadastro'));

        $this->renderer();
    }

    public function save($client_id = FALSE, $id = NULL)
    {
        $client_id = (int)$client_id;

        if ($this->input->post() && $this->data['me']->user_type == 2) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Nome', 'trim|required');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Telefone', 'trim|required');


            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());

                $redirect = ($id > 0) ? '/editar/' . $client_id . '/' . $id : '/novo/' . $client_id;

                redirect(base_url($this->uri->segment(1) . $redirect));

            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone')
                );

                if ($id > 0) {
                    $update = $this->shippingprocess_model->update(array(
                        'account_id' => $this->data['me']->account_id,
                        'id' => $id
                    ), $data);

                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }

                } else {
                    $data['account_id'] = $this->data['me']->account_id;
                    $data['client_id'] = $client_id;

                    $id = $this->shippingprocess_model->insert($data, TRUE);

                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1) . '/' . $client_id));
    }

    public function delete($client_id = FALSE, $id = NULL)
    {
        $client_id = (int)$client_id;

        if ($id && $this->data['me']->user_type == 2) {
            if ($this->shippingprocess_model->delete(array(
                'id' => $id,
                'client_id' => $client_id
            ))
            ) {
                $this->setMsg($this->lang->line('successfully_data_delete'));
            } else {
                $this->setError($this->lang->line('error_processing_delete'));
            }

        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }

        redirect(base_url($this->uri->segment(1) . '/' . $client_id));
    }
}