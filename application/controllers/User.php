<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('bankuser_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index($correspondent_id = FALSE)
    {
        $this->load_datatable();

        $this->data['correspondent_id'] = (int)$correspondent_id;

        if ($this->uri->segment(1) == 'usuarios-correspondente' && $this->data['correspondent_id'] == 0) {
            $this->data['list'] = (object)array();
        } else {

            $where = array(
                'account_id' => $this->data['me']->account_id,
                'correspondent_id' => $this->data['correspondent_id']
            );

            $this->data['list'] = $this->user_model->get($where)->result();
        }

        $this->setPageTitle('Lista de Usuários');

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($correspondent_id = FALSE, $id = FALSE)
    {
        $this->load->model('processcategory_model');

        $this->data['processcategory'] = $this->processcategory_model->get()->result();

        $this->data['correspondent_id'] = (int)$correspondent_id;
        $this->data['data'] = (object)array();

        if ($this->uri->segment(1) == 'usuarios-correspondente' && $this->data['correspondent_id'] == 0) {
            redirect(base_url($this->uri->segment(1) . '/' . $this->uri->segment(2)));
        }

        $id = ($this->uri->segment(1) == 'meu-perfil') ? $this->data['me']->id : $id;

        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {

            if ($id > 0) {
                $this->data['bank'] = current($this->bankuser_model->get(array('user_id' => $id))->result());
                $data = $this->user_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'correspondent_id' => $this->data['correspondent_id'],
                    'id' => $id
                ))->result();

                $this->data['data'] = (count($data) > 0) ? current($data) : NULL;
            }else{
                $this->data['data']->id = 0;
            }
        }

        $this->data['data']->action = $this->uri->segment(1) . '/salvar/' . ((isset($this->data['data']->id) && $this->uri->segment(1) != 'meu-perfil') ? $this->data['data']->id : NULL);


        if ($this->uri->segment(1) == 'usuarios-correspondente') {
            $this->data['data']->action = $this->uri->segment(1) . '/salvar/' . $this->data['correspondent_id'] . (isset($this->data['data']->id) ? '/' . $this->data['data']->id : '');
        }

        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de Usuário - ' . $this->data['data']->name : 'Cadastro de Usuário'));

        $this->renderer();
    }

    public function save($correspondent_id = FALSE, $id = NULL)
    {
        if($this->data['me']->user_type == 2 || $this->data['me']->id == $id){
            $correspondent_id = (int)$correspondent_id;

            if ($this->uri->segment(1) == 'usuarios-correspondente' && $correspondent_id == 0) {
                redirect(base_url($this->uri->segment(1) . '/' . $this->uri->segment(2)));
            }

            $id = ($this->uri->segment(1) == 'meu-perfil') ? $this->data['me']->id : $id;

            if ($this->input->post()) {

                $this->session->set_userdata($this->form_key, $this->parse_post());

                $this->load->library('form_validation');

                if ($id > 0) {
                    $user = $this->user_model->get(array(
                        'account_id' => $this->data['me']->account_id,
                        'correspondent_id' => $correspondent_id,
                        'id' => $id
                    ))->result();

                    $user = current($user);

                    $is_unique = ($user->email != $this->input->post('email')) ? '|is_unique[user.email]' : '';

                    $psw_required = ($this->input->post('psw') || $this->input->post('psw_confirm'));

                } else {
                    $is_unique = '|is_unique[user.email]';
                    $psw_required = TRUE;
                }

                $this->form_validation->set_rules('name', 'Nome', 'trim|required');
                $this->form_validation->set_rules('department', 'Departamento', 'trim|required');
                $this->form_validation->set_rules('role', 'Cargo', 'trim|required');
                $this->form_validation->set_rules('cpf', 'CPF', 'trim|required');
                $this->form_validation->set_rules('oab', 'OAB', 'trim|required');
                $this->form_validation->set_rules('rg', 'RG', 'trim|required');
                $this->form_validation->set_rules('maritalstatus', 'Estado Civil', 'trim|required');
                $this->form_validation->set_rules('birth', 'Data de Nascimento', 'trim|required');
                $this->form_validation->set_rules('admission', 'Data de Admissão', 'trim|required');
                $this->form_validation->set_rules('street', 'Rua', 'trim|required');
                $this->form_validation->set_rules('number', 'Nº', 'trim|required');
                $this->form_validation->set_rules('cep', 'CEP', 'trim|required');
                $this->form_validation->set_rules('city', 'Cidade', 'trim|required');
                $this->form_validation->set_rules('state', 'Estado', 'trim|required');
                $this->form_validation->set_rules('state', 'Estado', 'trim|required');

                if ($psw_required) $this->form_validation->set_rules('psw', 'Senha', 'trim|required');
                if ($psw_required) $this->form_validation->set_rules('psw_confirm', 'Confirmar Senha', 'trim|matches[psw]|required');


                if ($this->form_validation->run() === FALSE) {
                    $this->setError(validation_errors());

                    $redirect = ($id > 0) ? '/editar/' . $id : '/novo';

                    if ($this->uri->segment(1) == 'usuarios-correspondente') {
                        $redirect = ($id > 0) ? '/editar/' . $correspondent_id . '/' . $id : '/novo/' . $correspondent_id;
                    }

                    $redirect = ($this->uri->segment(1) == 'meu-perfil') ? '' : $redirect;

                    redirect(base_url($this->uri->segment(1) . $redirect));

                } else {
                    $data = array(
                        'name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'department' => $this->input->post('department'),
                        'role' => $this->input->post('role'),
                        'phone' => $this->input->post('phone'),
                        'cpf' => $this->input->post('cpf'),
                        'oab' => $this->input->post('oab'),
                        'rg' => $this->input->post('rg'),
                        'maritalstatus' => $this->input->post('maritalstatus'),
                        'birth' => $this->input->post('birth'),
                        'admission' => $this->input->post('admission'),
                        'street' => $this->input->post('street'),
                        'number' => $this->input->post('number'),
                        'complement' => $this->input->post('complement'),
                        'cep' => $this->input->post('cep'),
                        'city' => $this->input->post('city'),
                        'district' => $this->input->post('district'),
                        'state' => $this->input->post('state'),
                        'note' => $this->input->post('note'),
                    );
                    if($this->data['me']->user_type == 2){
                        $data['user_type'] = $this->input->post('user_type');
                        $data['processcategory_id'] = $this->input->post('processcategory_id');
                    }

                    $bank = array(
                        'owner' => $this->input->post('bank_owner'),
                        'cpf' => $this->input->post('bank_cpf'),
                        'type' => $this->input->post('bank_type'),
                        'name' => $this->input->post('bank_name'),
                        'agency' => $this->input->post('bank_agency'),
                        'number' => $this->input->post('bank_number'),
                    );

                    if ($psw_required) $data['password'] = md5($this->input->post('psw'));

                    if ($id > 0) {
                        $update = $this->user_model->update(array(
                            'account_id' => $this->data['me']->account_id,
                            'id' => $id
                        ), $data);
                        $bank['user_id'] = $id;
                        if($this->input->post('bank_exist') == 0){
                            $this->bankuser_model->insert($bank);
                        }else{
                            $this->bankuser_model->update(array('user_id' => $id), $bank);
                        }


                        if (!$update) {
                            $this->setError($this->lang->line('error_processing_form'));
                        } else {
                            $this->session->unset_userdata($this->form_key);
                            $this->setMsg($this->lang->line('successfully_data'));
                        }

                    } else {
                        $bank['user_id'] = $this->data['me']->id;
                        $data['account_id'] = $this->data['me']->account_id;
                        $data['correspondent_id'] = $correspondent_id;
                        $id = $this->user_model->insert($data, TRUE);
                        $this->bankuser_model->insert($bank);
                        if (!$id) {
                            $this->setError($this->lang->line('error_processing_form'));
                        } else {
                            $this->session->unset_userdata($this->form_key);
                            $this->setMsg($this->lang->line('successfully_data_insert'));
                        }
                    }
                }

            } else {
                $this->setError($this->lang->line('error_processing_form'));
            }
            redirect(base_url($this->uri->segment(1) . (($this->uri->segment(1) == 'usuarios-correspondente') ? '/' . $correspondent_id : '')));
        }
        redirec('./');
    }

    public function delete($correspondent_id = FALSE, $id = NULL)
    {
        $correspondent_id = (int)$correspondent_id;

        if ($this->uri->segment(1) == 'usuarios-correspondente' && $correspondent_id == 0) {
            redirect(base_url($this->uri->segment(1) . '/' . $this->uri->segment(2)));
        }

        if ($id && ($this->data['me']->user_type == 2 || $this->data['me']->id == $id)) {
            if ($this->user_model->delete(array(
                'id' => $id,
                'correspondent_id' => $correspondent_id
            ))
            ) {
                $this->setMsg($this->lang->line('successfully_data_delete'));
            } else {
                $this->setError($this->lang->line('error_processing_delete'));
            }

        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }

        redirect(base_url($this->uri->segment(1) . (($this->uri->segment(1) == 'usuarios-correspondente') ? '/' . $correspondent_id : '')));
    }
}