<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('account_model', 'bankaccount_model'));
        $this->lang->load('site');

        $this->form_key = $this->router->class . '_' . $this->data['me']->account_id;
    }

    public function index()
    {
        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
        } else {
            $data = $this->account_model->get(array(
                'id' => $this->data['me']->account_id
            ))->result();

            $this->data['data'] = (count($data) > 0) ? current($data) : new stdClass();

            $bankaccount = $this->bankaccount_model->get(array(
                'account_id' => $this->data['me']->account_id
            ))->result();

            $this->data['data']->bankaccount = $bankaccount && (count($bankaccount) > 0) ? $bankaccount : NULL;
        }

        $this->setPageTitle('Conta' . (isset($this->data['data']->name) ? ' - ' . $this->data['data']->name : NULL));

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        if (isset($_POST['bankaccount'])) {
            $data->bankaccount = array();
            foreach ($_POST['bankaccount'] as $key => $row) {
                $data->bankaccount[$key] = (object)$row;
            }
        }
        return count((array)$data) !== 0 ? $data : null;
    }

    public function save()
    {
        if ($this->input->post() && $this->data[me]->user_type == 2) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Nome Fantasia', 'trim|required');
            $this->form_validation->set_rules('companyname', 'Razão Social', 'trim|required');
            $this->form_validation->set_rules('cnpj', 'CNPJ', 'trim|required');
            $this->form_validation->set_rules('municipalregistration', 'Insc. Municipal', 'trim|required');
            $this->form_validation->set_rules('stateregistration', 'Insc. Estadual', 'trim|required');
            $this->form_validation->set_rules('street', 'Rua', 'trim|required');
            $this->form_validation->set_rules('number', 'Nº', 'trim|required');
            $this->form_validation->set_rules('complement', 'Compl.', 'trim|required');
            $this->form_validation->set_rules('cep', 'CEP', 'trim|required');
            $this->form_validation->set_rules('city', 'Cidade', 'trim|required');
            $this->form_validation->set_rules('state', 'Estado', 'trim|required');
            $this->form_validation->set_rules('maincontact', 'Contato Principal - Nome', 'trim|required');
            $this->form_validation->set_rules('mainphone1', 'Contato Principal - Telefone 1', 'trim|required');
            $this->form_validation->set_rules('mainemail1', 'Contato Principal - E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('site', 'Contato Principal - Site', 'trim|required');
            if ($bankaccount = $this->input->post('bankaccount')) {
                foreach ($bankaccount as $id => $row) {
                    $this->form_validation->set_rules('bankaccount[' . $id . '][owner]', 'Dados Bancários - ' . $id . ' - Titular', 'trim|required');
                    $this->form_validation->set_rules('bankaccount[' . $id . '][cpf]', 'Dados Bancários - ' . $id . ' - CPF', 'trim|required');
                    $this->form_validation->set_rules('bankaccount[' . $id . '][type]', 'Dados Bancários - ' . $id . ' - Tipo da Conta', 'trim|required');
                    $this->form_validation->set_rules('bankaccount[' . $id . '][name]', 'Dados Bancários - ' . $id . ' - Banco', 'trim|required');
                    $this->form_validation->set_rules('bankaccount[' . $id . '][agency]', 'Dados Bancários - ' . $id . ' - Agência', 'trim|required');
                    $this->form_validation->set_rules('bankaccount[' . $id . '][number]', 'Dados Bancários - ' . $id . ' - Conta Corrente', 'trim|required');
                }
            }

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'companyname' => $this->input->post('companyname'),
                    'cnpj' => $this->input->post('cnpj'),
                    'municipalregistration' => $this->input->post('municipalregistration'),
                    'stateregistration' => $this->input->post('stateregistration'),
                    'street' => $this->input->post('street'),
                    'number' => $this->input->post('number'),
                    'complement' => $this->input->post('complement'),
                    'cep' => $this->input->post('cep'),
                    'city' => $this->input->post('city'),
                    'state' => $this->input->post('state'),
                    'maincontact' => $this->input->post('maincontact'),
                    'mainphone1' => $this->input->post('mainphone1'),
                    'mainphone2' => $this->input->post('mainphone2'),
                    'mainphone3' => $this->input->post('mainphone3'),
                    'mainphone4' => $this->input->post('mainphone4'),
                    'mainphone5' => $this->input->post('mainphone5'),
                    'mainphone6' => $this->input->post('mainphone6'),
                    'mainphone7' => $this->input->post('mainphone7'),
                    'mainphone8' => $this->input->post('mainphone8'),
                    'mainemail1' => $this->input->post('mainemail1'),
                    'mainemail2' => $this->input->post('mainemail2'),
                    'mainemail3' => $this->input->post('mainemail3'),
                    'site' => $this->input->post('site'),
                    'note' => $this->input->post('note')
                );

                $update = $this->account_model->update(array(
                    'id' => $this->data['me']->account_id
                ), $data);


                if (!$update) {
                    $this->setError($this->lang->line('error_processing_form'));
                } else {

                    $this->bankaccount_model->delete(array(
                        'account_id' => $this->data['me']->account_id
                    ));

                    if (isset($bankaccount) && count($bankaccount) > 0) {

                        foreach ($bankaccount as $id => $row) {
                            $data_bank = $row;
                            $data_bank['account_id'] = $this->data['me']->account_id;

                            $this->bankaccount_model->insert($data_bank);
                        }
                    }

                    $this->session->unset_userdata($this->form_key);
                    $this->setMsg($this->lang->line('successfully_data'));
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }
}