<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->lang->load('site');
    }

    public function index()
    {

        if (isset($this->data['usuario']->id)) {

            if ($this->session->userdata('usuario')) {
                $this->session->unset_userdata('usuario');
            }

            unset($this->data['usuario']);
        }

        $this->data['email'] = $this->session->flashdata('email');


        $this->header = $this->template . '/login/header';
        $this->footer = $this->template . '/login/footer';

        $this->renderer();
    }

    public function auth()
    {
        if ($this->input->post()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('password', 'Senha', 'trim|required');

            $this->session->set_flashdata('email', $this->input->post('email'));

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
            } else {
                $user = $this->user_model->get(array(
                    'email' => $this->input->post('email')
                ))->result();

                if (count($user) === 0) {
                    $this->setError($this->lang->line('email_not_found'));
                } else {
                    $user = current($user);
                    if ($user->password != md5($this->input->post('password'))) {
                        $this->setError(sprintf($this->lang->line('wrong_password'), $user->name));
                    } else {

                        $this->session->set_userdata('me', $user);
                        redirect(base_url());
                    }
                }
            }
        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }

    public function recovery_password()
    {
        if ($this->input->post()) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('email', 'E-mail', 'trim|required|min_length[3]');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
            } else {

            }
        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }
}
