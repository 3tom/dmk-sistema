<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function index()
    {
        $this->setPageTitle((isset($this->data['me']->name) ? 'Bem vindo, ' . $this->data['me']->name . '!' : 'Bem vindo!'));
        $this->renderer();
    }
}