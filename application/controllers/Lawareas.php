<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lawareas extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('lawareas_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index()
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $this->load_datatable();

        $this->data['list'] = $this->lawareas_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->setPageTitle('Áreas de Direito');

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($id = NULL)
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = NULL;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {

            if ($id > 0) {
                $data = $this->lawareas_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();

                if (count($data) > 0) {
                    $data = current($data);
                    $this->data['data'] = $data;
                }
            }
        }

        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de Área de Direito  - ' . $this->data['data']->name : 'Cadastro de Área de Direito '));

        $this->renderer();
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Nome', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());

                $redirect = ($id > 0) ? '/editar/' . $id : '/novo';

                redirect(base_url($this->uri->segment(1) . $redirect));

            } else {
                $data = array(
                    'name' => $this->input->post('name')
                );

                if ($id > 0) {
                    $update = $this->lawareas_model->update(array(
                        'account_id' => $this->data['me']->account_id,
                        'id' => $id
                    ), $data);

                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }

                } else {
                    $data['account_id'] = $this->data['me']->account_id;

                    $id = $this->lawareas_model->insert($data, TRUE);

                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }

    public function delete($id = FALSE)
    {
        if ($id && $this->data['me']->user_type == 2) {

            $this->load->model(array('clientlawareas_model', 'correspondentlawareas_model'));

            $cannot_deleted = $this->clientlawareas_model->get(array(
                'lawareas_id' => $id
            ))->result();

            $cannot_deleted = array_merge($cannot_deleted, $this->correspondentlawareas_model->get(array(
                'lawareas_id' => $id
            ))->result());

            if (count($cannot_deleted) == 0) {
                if ($this->lawareas_model->delete(array(
                    'id' => $id
                ))
                ) {
                    $this->setMsg($this->lang->line('successfully_data_delete'));
                } else {
                    $this->setError($this->lang->line('error_processing_delete'));
                }
            } else {
                $this->setError($this->lang->line('cannot_deleted'));
            }

        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }


        redirect(base_url($this->uri->segment(1)));
    }
}