<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('process_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    private function load_client_filter()
    {

        $filter = array();
        $this->data['filter'] = new stdClass();

        if ($this->input->get('deadlinestart')) {
            $this->data['filter']->deadlinestart = $this->input->get('deadlinestart');;
            $filter['deadline >='] = convertDate($this->data['filter']->deadlinestart);
        }

        if ($this->input->get('deadlineend')) {
            $this->data['filter']->deadlineend = $this->input->get('deadlineend');
            $filter['deadline <='] = convertDate($this->data['filter']->deadlineend);
        }

        if ($this->input->get('client_id')) {
            $filter['client_id'] = $this->data['filter']->client_id = $this->input->get('client_id');;
        }
        return $filter;
    }

    public function client()
    {
        $this->load->model('client_model');

        $this->load_mask();
        $this->get_judicialpanel();

        $this->setPageTitle('Relatório para cliente');

        $this->data['client'] = $this->client_model->get()->result();

        $where = $this->load_client_filter();

        $this->data['list'] = $this->process_model->get($where)->result();

        $client = array();
        $user = array();
        $city = array();
        $process_id = array();


        if (count($this->data['list']) === 0) {
            //return false;
        }else {

            foreach ($this->data['list'] as $key => $row) {
                $row->coststypeservice = $this->get_coststypeservice($row->id);
                $client[] = $row->client_id;
                $user[] = $row->user_id;
                $city[] = $row->city;
                $process_id[] = $row->id;
                $row->hash = md5($row->id . $row->user_id . $row->client_id);
                $this->data['list'][$key] = $row;
            }
            $this->get_client();

            foreach ($this->data['list'] as $key => $row) {
                $row->client = isset($this->data['client'][$row->client_id]) ? $this->data['client'][$row->client_id] : null;
                $row = $this->totalize_value($row);
                $row = $this->create_report($row);

                $row->link = './processos/visualizar/' . $row->id;
                $row->deadline = convertDate($row->deadline);

                $row->clientfee = number_format($row->clientfee, 2, ',', '.');
                $row->clientcoststypeservice = number_format($row->clientcoststypeservice, 2, ',', '.');
                $row->correspondentfee = number_format($row->correspondentfee, 2, ',', '.');
                $row->correspondentcoststypeservice = number_format($row->correspondentcoststypeservice, 2, ',', '.');
                $row->correspondenttotalcost = number_format($row->correspondenttotalcost, 2, ',', '.');



                $row->correspondent = $this->get_correspondent($row->correspondent_id);
                $row->correspondent = current($row->correspondent);

                $row->honorarytypeservice = $this->get_honorarytypeservice(array('process_id' => $row->id), false);
                $row->honorarytypeservice = current($row->honorarytypeservice);

                $row->shippingprocess = $this->get_shippingprocess($row->client_id, $row->shippingprocess_id);
                $row->shippingprocess = current($row->shippingprocess);

                $this->data['list'][$key] = $row;
            }
        }

        if($this->input->get('excel')) {
            $this->excelClient();
            return false;
        }

        $this->renderer();
    }

    public function save_status($id)
    {
        $data = array();
        if ($this->data['me']->correspondent_id != 0 && ($this->input->get('processstatus_id') != 2 && $this->input->get('processstatus_id') != 3 )) {
            $this->setError('Status não permitido');
        }else{
            $data = $this->process_model->get(array(
                'id' => $id
            ))->result();

        }
        if (count($data) > 0) {
            $data = current($data);

            $process = array(
                'processstatus_id' => $this->input->get('processstatus_id'),
                'reason' => $this->input->get('reason')
            );

            if($this->input->get('processstatus_id') == 2) {
                $process['correspondent_id'] = 0;
            }

            if (md5($data->id . $data->user_id . $data->client_id) == $this->input->get('hash')) {
                $this->process_model->update(array(
                    'id' => $id
                ), $process);
                $this->setMsg('Status alterado com sucesso');
            }else{
                $this->setError('Erro validação, tente novamente mais tarde');
            }
        }
        redirect('processos/acompanhamento');
    }

    private function load_chat($process_id, $internal)
    {
        $this->load->model('chat_model');
        $data = $this->chat_model->get_related(array(
            'process_id' => $process_id,
            'internal' => $internal,
        ))->result();

        foreach ($data as $key => $row) {
            $row->date = convertDate($row->datetime);
            $row->time = explode(' ', $row->datetime);
            $row->time = end($row->time);
            $data[$key] = $row;
        }

        $this->data['processchat' . $internal] = $data;
    }

    private function load_file($process_id)
    {
        $this->load->model('processfile_model');
        $data = $this->processfile_model->get(array(
            'process_id' => $process_id,
        ))->result();

        foreach ($data as $key => $row) {
            $row->file_name = explode('/', $row->file);
            $row->file_name = end($row->file_name);
            $row->date = convertDate($row->created);
            $row->time = explode(' ', $row->created);
            $row->time = end($row->time);
            $row->hash = md5($row->id . $row->user_id . $row->created);
            $data[$key] = $row;
        }

        $this->data['processfile'] = $data;
    }

    public function file_delete($id, $hash)
    {
        $this->load->model('processfile_model');

        $data = $this->processfile_model->get(array(
            'id' => $id
        ))->result();

        if (count($data) > 0) {

            $data = current($data);

            @unlink($data->file);

            if (md5($data->id . $data->user_id . $data->created) == $hash) {
                $this->processfile_model->delete(array(
                    'id' => $id,
                ));
            }
            redirect('processos/visualizar/' . $data->process_id . '/#group-file');
        } else {
            redirect();
        }
    }

    public function file_save($process_id)
    {
        if ($_FILES['file']['size'] > 0) {

            $uploadPath = './assets/upload/' . date('Y') . '/' . date('m') . '/' . date('H') . '/' . date('i') . '/' . date('s') . '/';
            if (!is_dir($uploadPath)) {
                mkdir($uploadPath, DIR_WRITE_MODE, TRUE);
            }
            $this->load->library('upload', array('upload_path' => $uploadPath, 'allowed_types' => 'gif|jpg|png|doc|pdf|docx'));

            if (!$this->upload->do_upload('file')) {
                $this->setError($this->upload->display_errors());
            } else {
                $file = $this->upload->data();
                $file = $uploadPath . $file['file_name'];

                $this->load->model('processfile_model');

                $this->processfile_model->insert(array(
                    'user_id' => $this->data['me']->id,
                    'description' => $this->input->post('description'),
                    'process_id' => $process_id,
                    'file' => $file,
                ));
            }
        }

        redirect('processos/visualizar/' . $process_id . '/#group-file');
    }

    public function chat($process_id, $internal)
    {
        $this->load->model('chat_model');

        $this->chat_model->insert(array(
            'user_id' => $this->data['me']->id,
            'process_id' => $process_id,
            'internal' => $internal,
            'text' => trim($this->input->post('text')),
        ));

        redirect('processos/visualizar/' . $process_id . '/#group-chat' . $internal);
    }

    public function print_view($id)
    {
        $this->follow($id);

        if (count($this->data['list']) === 0) {
            $this->setError('Esse processo não pode ser recuperado.');
            redirect($this->uri->segment(1) . '/acompanhamento');
        }
        $data = current($this->data['list']);
        $data->shippingprocess = $this->get_shippingprocess($data->client_id, $data->shippingprocess_id);
        $data->shippingprocess = current($data->shippingprocess);
        $data->deadline = convertDate($data->deadline);

        $data->judicialpanel = $this->get_judicialpanel($data->judicialpanel_id);
        $data->judicialpanel = current($data->judicialpanel);

        $this->data['data'] = $data;
        $this->renderer();
    }

    public function view($id)
    {
        $this->load_datatable();

        $this->follow($id);

        $this->load_mask();

        if (count($this->data['list']) === 0) {
            $this->setError('Esse processo não pode ser recuperado.');
            redirect($this->uri->segment(1) . '/acompanhamento');
        }
        $data = current($this->data['list']);

        $data->shippingprocess = $this->get_shippingprocess($data->client_id, $data->shippingprocess_id);
        $data->shippingprocess = current($data->shippingprocess);
        $data->deadline = convertDate($data->deadline);

        $data->judicialpanel = $this->get_judicialpanel($data->judicialpanel_id);
        $data->judicialpanel = current($data->judicialpanel);

        $this->data['data'] = $data;

        $this->load_chat($data->id, 1);
        $this->load_chat($data->id, 0);
        $this->load_file($data->id);

        if($this->data['me']->user_type == 2) {
            $this->content = $this->template . '/process/view/admin';
        }elseif($this->data['me']->correspondent_id == 0) {
            $this->content = $this->template . '/process/view/regular';
        }else{
            $this->content = $this->template . '/process/view/correspondent';
        }

        $this->renderer();
    }

    private function get_user($id = null)
    {
        $this->load->model('user_model');

        if ($id !== null) {
            $where = array(
                'id' => $id
            );
        } else {
            $where = array(
                'account_id' => $this->data['me']->account_id,
                'correspondent_id' => 0
            );
        }

        $user = $this->user_model->get($where)->result();

        $this->data['user'] = array();

        foreach ($user as $row) {
            $this->data['user'][$row->id] = $row;
        }

        return $this->data['user'];
    }

    private function get_shippingprocess($client_id, $id = null)
    {
        $this->load->model('shippingprocess_model');

        $where = array(
            'account_id' => $this->data['me']->account_id,
            'client_id' => $client_id
        );

        if ($id !== null) {
            $where['id'] = $id;
        }

        $shippingprocess = $this->shippingprocess_model->get($where)->result();

        $this->data['shippingprocess'] = array();

        foreach ($shippingprocess as $row) {
            $this->data['shippingprocess'][$row->id] = $row;
        }

        return $this->data['shippingprocess'];
    }

    private function get_city($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }

        $this->load->model('city_model');
        $data = $this->city_model->get($where)->result();

        $this->data['city'] = array();

        foreach ($data as $key => $row) {
            $this->data['city'][$row->id] = $row;
        }
        return $this->data['city'];
    }

    private function get_correspondent($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }

        $this->load->model('correspondent_model');
        $data = $this->correspondent_model->get($where)->result();

        $this->data['correspondent'] = array();

        foreach ($data as $key => $row) {
            $this->data['correspondent'][$row->id] = $row;
        }
        return $this->data['correspondent'];
    }


    private function get_correspondentemail($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['correspondent_id'] = $id;
        }

        $this->load->model('correspondentemail_model');
        $data = $this->correspondentemail_model->get($where)->result();

        $this->data['correspondentemail'] = array();

        foreach ($data as $key => $row) {
            $this->data['correspondentemail'][$row->id] = $row;
        }
        return $this->data['correspondentemail'];
    }

    private function send_email($data)
    {
        $message = $data->message;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '<br>' . PHP_EOL;
        $message .= '___ct___';

        $this->load->library('email');

        $this->email->from('dmk@3tom.com.br', NAME_SITE);
        $this->email->to($data->correspondent->email);
        $this->email->bcc('sena@iocode.com.br');
        $this->email->subject(NAME_SITE . ' - Solicitação');
        $this->email->message($message);
        $this->email->set_alt_message(strip_tags($message));

        if (!$this->email->send()) {
            $this->setError('Ocorreu um erro ao enviar seu email');
            log_message('error', __METHOD__ . var_export($this->email->print_debugger(), TRUE));
        } else {
            $this->setMsg('Email enviado com sucesso');
        }
    }

    private function get_status($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }

        $this->load->model('processstatus_model');
        $data = $this->processstatus_model->get($where)->result();

        $this->data['processstatus'] = array();

        foreach ($data as $key => $row) {
            $this->data['processstatus'][$row->id] = $row;
        }
        return $this->data['processstatus'];
    }

    private function get_status2($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }

        $this->load->model('processstatus2_model');
        $data = $this->processstatus2_model->get($where)->result();

        $this->data['processstatus2'] = array();

        foreach ($data as $key => $row) {
            $this->data['processstatus2'][$row->id] = $row;
        }
        return $this->data['processstatus2'];
    }

    private function changeStatusBalance()
    {
        if($this->input->post('charge_status') && $this->input->post('id')){
            $this->process_model->update(array('id' => $this->input->post('id')), array(
                'processstatus2_id' => $this->input->post('charge_status'),
                'receivedate' => $this->input->post('receivedate') ? convertDate($this->input->post('receivedate')) : null,
            ));
        }

    }

    private function load_balance_filter()
    {

        $filter = array(
            'processstatus_id' => 7//Finalizado
        );
        $this->data['filter'] = new stdClass();

        if ($this->input->get('deadlinestart')) {
            $this->data['filter']->deadlinestart = $this->input->get('deadlinestart');;
            $filter['receivedate >='] = convertDate($this->data['filter']->deadlinestart);
        }

        if ($this->input->get('deadlineend')) {
            $this->data['filter']->deadlineend = $this->input->get('deadlineend');
            $filter['receivedate <='] = convertDate($this->data['filter']->deadlineend);
        }

        if($this->data['me']->correspondent_id != 0) {
            $filter['correspondent_id'] = $this->data['me']->correspondent_id;
        }

        if ($this->input->get('client_id')) {
            $filter['client_id'] = $this->data['filter']->client_id = $this->input->get('client_id');;
        }


        if($this->data['me']->user_type != 0 && $this->data['me']->processcategory_id != 0) {
            $_GET['processcategory_id'] = $this->data['me']->processcategory_id;
        }

        if($this->input->get('processcategory_id')) {
            $this->data['processcategory_id'] = $filter['processcategory_id'] = $this->input->get('processcategory_id');
        }
        return $filter;
    }

    private function checkDeadLine($row)
    {

        $diff = new DateTime();

        $row->d = $diff = $diff->diff(new DateTime($row->deadline));

        if (!$row->deadline || $diff->invert === 1) {
            $row->signal = 'red';
        } elseif ($diff->days >= 2) {
            $row->signal = 'green';
        } else {
            $row->signal = 'yellow';
        }
        return $row;
    }
    public function balance()
    {
        $this->setPageTitle('Acompanhamento de entradas e balanço geral');

        $this->load_mask();
        $this->load_datatable();

        $this->changeStatusBalance();

        $this->get_status2();
        $this->get_client();

        $where = $this->load_balance_filter();

        $this->data['list'] = $this->process_model->get($where)->result();

        $client = array();
        $user = array();
        $city = array();
        $process_id = array();
        $correspondent = array();

        $this->data['total'] = new stdClass();
        $this->data['total']->profitability = 0;
        $this->data['total']->clientliquidvalue = 0;
        $this->data['total']->taxinvoice = 0;
        $this->data['total']->clientfee = 0;
        $this->data['total']->clientcoststypeservice = 0;
        $this->data['total']->correspondenttotalcost = 0;
        $this->data['total']->correspondentcoststypeservice = 0;
        $this->data['total']->correspondentfee = 0;


        if (count($this->data['list']) === 0) {
            //return false;
        }else {

            foreach ($this->data['list'] as $key => $row) {
                $row->coststypeservice = $this->get_coststypeservice($row->id);
                $client[] = $row->client_id;
                $user[] = $row->user_id;
                $city[] = $row->city;
                $process_id[] = $row->id;
                $correspondent[] = $row->correspondent_id;
                $row->hash = md5($row->id . $row->user_id . $row->client_id);
                $this->data['list'][$key] = $this->checkDeadLine($row);
            }

            foreach ($this->data['list'] as $key => $row) {
                $row->processstatus2 = isset($this->data['processstatus2'][$row->processstatus2_id]) ? $this->data['processstatus2'][$row->processstatus2_id] : null;
                $row->client = isset($this->data['client'][$row->client_id]) ? $this->data['client'][$row->client_id] : null;
                $row->link = './processos/visualizar/' . $row->id;
                $row = $this->totalize_value($row);
                $row = $this->create_report($row);


                $this->data['total']->profitability += $row->profitability;
                $this->data['total']->clientliquidvalue += $row->clientliquidvalue;
                $this->data['total']->taxinvoice += $row->taxinvoice;
                $this->data['total']->clientfee += $row->clientfee;
                $this->data['total']->clientcoststypeservice += $row->clientcoststypeservice;
                $this->data['total']->correspondenttotalcost += $row->correspondenttotalcost;
                $this->data['total']->correspondentcoststypeservice += $row->correspondentcoststypeservice;
                $this->data['total']->correspondentfee += $row->correspondentfee;

                $row->profitability = number_format($row->profitability, 2, ',', '.');
                $row->clientliquidvalue = number_format($row->clientliquidvalue, 2, ',', '.');
                $row->result = number_format($row->result, 2, ',', '.');
                $row->taxinvoice = number_format($row->taxinvoice, 2, ',', '.');
                $row->clientfee = number_format($row->clientfee, 2, ',', '.');
                $row->clientcoststypeservice = number_format($row->clientcoststypeservice, 2, ',', '.');
                $row->correspondenttotalcost = number_format($row->correspondenttotalcost, 2, ',', '.');
                $row->correspondentcoststypeservice = number_format($row->correspondentcoststypeservice, 2, ',', '.');
                $row->correspondentfee = number_format($row->correspondentfee, 2, ',', '.');
                $row->receivedate = convertDate($row->receivedate);


                $this->data['list'][$key] = $row;
            }
        }
        $this->data['total']->profitability = number_format($this->data['total']->profitability, 2, ',', '.');
        $this->data['total']->clientliquidvalue = number_format($this->data['total']->clientliquidvalue, 2, ',', '.');
        $this->data['total']->taxinvoice = number_format($this->data['total']->taxinvoice, 2, ',', '.');
        $this->data['total']->clientfee = number_format($this->data['total']->clientfee, 2, ',', '.');
        $this->data['total']->clientcoststypeservice = number_format($this->data['total']->clientcoststypeservice, 2, ',', '.');
        $this->data['total']->correspondenttotalcost = number_format($this->data['total']->correspondenttotalcost, 2, ',', '.');
        $this->data['total']->correspondentcoststypeservice = number_format($this->data['total']->correspondentcoststypeservice, 2, ',', '.');
        $this->data['total']->correspondentfee = number_format($this->data['total']->correspondentfee, 2, ',', '.');

        if($this->input->get('excel')) {
            $this->excelBalance();
            return false;
        }

        $this->renderer();
    }

    private function create_report($_row)
    {
        $data = array(
            'client' => $this->data['client'][$_row->client_id],
            'taxinvoice' => 0,

            'clientfee' => 0,
            'clientcoststypeservice' => 0,

            'correspondentfee' => 0,
            'correspondentcoststypeservice' => 0,

            'out' => 0,
            'in' => 0,
            'result' => 0,
        );

        foreach ($_row->correspondentfee as $row) {
            $data['correspondentfee'] += $row->value;
        }


        foreach ($_row->clientfee as $row) {
            $data['clientfee'] += $row->value;
        }

        foreach ($_row->clientcoststypeservice as $row) {
            $data['clientcoststypeservice'] += $row->value;
        }

        foreach ($_row->correspondentcoststypeservice as $row) {
            $data['correspondentcoststypeservice'] += $row->value;
        }

        if ($data['client']->taxinvoice == 1) {
            $data['taxinvoice'] = round(($data['clientfee'] * 0.05), 2);
        }
        $data['taxinvoice'] = number_format($data['taxinvoice'], 2, '.', '');

        $data['in'] = $data['clientfee'] - $data['taxinvoice'] + $data['clientcoststypeservice'];
        $data['out'] = $data['correspondentfee'] + $data['correspondentcoststypeservice'];
        $data['result'] = $data['in'] - $data['out'];

        $data['result'] = number_format($data['result'], 2, '.', '');
        $data['out'] = number_format($data['out'], 2, '.', '');
        $data['in'] = number_format($data['in'], 2, '.', '');

        $data['correspondenttotalcost'] = $data['correspondentfee'] + $data['correspondentcoststypeservice'];
        $data['clientliquidvalue'] = $data['clientfee'] - $data['taxinvoice'];

        $data['profitability'] = $data['clientliquidvalue'] - $data['clientcoststypeservice'] - $data['correspondenttotalcost'];


        $data['clientfee'] = number_format($data['clientfee'], 2, '.', '');
        $data['clientcoststypeservice'] = number_format($data['clientcoststypeservice'], 2, '.', '');

        $data['correspondentfee'] = number_format($data['correspondentfee'], 2, '.', '');
        $data['correspondentcoststypeservice'] = number_format($data['correspondentcoststypeservice'], 2, '.', '');
        $data['correspondenttotalcost'] = number_format($data['correspondenttotalcost'], 2, '.', '');

        $data = (object)$data;

        $_row->correspondentfee = $data->correspondentfee;

        $_row->result = $data->result;
        $_row->taxinvoice = $data->taxinvoice;
        $_row->clientfee = $data->clientfee;
        $_row->clientliquidvalue = $data->clientliquidvalue;
        $_row->clientcoststypeservice = $data->clientcoststypeservice;
        $_row->correspondenttotalcost = $data->correspondenttotalcost;
        $_row->profitability = $data->profitability;
        $_row->correspondentcoststypeservice = $data->correspondentcoststypeservice;
        return $_row;
    }

    private function totalize_value($data)
    {
        $this->load->model('shippingprocess_model');
        $processfee = $this->get_processfee($data->id);
        $processfeenote = $this->get_processfeenote($data->id);
        $processcoststypeservice = $this->get_coststypeservice($data->id);

        $data->clientcoststypeservice = $processcoststypeservice['clientcoststypeservice'];
        $data->correspondentcoststypeservice = $processcoststypeservice['correspondentcoststypeservice'];
        $data->clientfeenote = $processfeenote['clientfeenote'];
        $data->correspondentfeenote = $processfeenote['correspondentfeenote'];
        $data->clientfee = $processfee['clientfee'];
        $data->correspondentfee = $processfee['correspondentfee'];

        return $data;
    }

    public function follow($id = null)
    {

        $this->setPageTitle('Acompanhamento de processo');

        $this->load_datatable();

        $this->get_status();

        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }
        if ($this->router->method == 'follow') {
            $where['processstatus_id !='] = 7;//Remove finished process
        }
        if($this->data['me']->correspondent_id != 0) {
            $where['correspondent_id'] = $this->data['me']->correspondent_id;
        }


        if($this->data['me']->user_type != 0 && $this->data['me']->processcategory_id != 0) {
            $_GET['processcategory_id'] = $this->data['me']->processcategory_id;
        }

        if($this->input->get('processcategory_id')) {
            $this->data['processcategory_id'] = $where['processcategory_id'] = $this->input->get('processcategory_id');
        }

        $this->data['list'] = $this->process_model->get($where)->result();

        $client = array();
        $user = array();
        $city = array();
        $process_id = array();
        $correspondent = array();


        if (count($this->data['list']) === 0) {
            //return false;
        }else {

            foreach ($this->data['list'] as $key => $row) {
                $row->coststypeservice = $this->get_coststypeservice($row->id);
                $client[] = $row->client_id;
                $user[] = $row->user_id;
                $city[] = $row->city;
                $process_id[] = $row->id;
                $correspondent[] = $row->correspondent_id;
                $row->hash = md5($row->id . $row->user_id . $row->client_id);
                $this->data['list'][$key] = $row;
            }
            $this->get_client($client);
            $this->get_correspondent($correspondent);
            $this->get_city($city);
            $this->get_user($user);
            $this->get_processfee_totalized(array('process_id' => $process_id));

            foreach ($this->data['list'] as $key => $row) {

                $clientcoststypeservice_value = 0;
                foreach ($row->coststypeservice['clientcoststypeservice'] as $clientcoststypeservice) {
                    $clientcoststypeservice_value += $clientcoststypeservice->value;
                }
                $row->clientcoststypeservice_value = number_format($clientcoststypeservice_value, 2, '.', '');

                $correspondentcoststypeservice_value = 0;
                foreach ($row->coststypeservice['correspondentcoststypeservice'] as $correspondentcoststypeservice) {
                    $correspondentcoststypeservice_value += $correspondentcoststypeservice->value;
                }
                $row->correspondentcoststypeservice_value = number_format($correspondentcoststypeservice_value, 2, '.', '');

                $row->processstatus = isset($this->data['processstatus'][$row->processstatus_id]) ? $this->data['processstatus'][$row->processstatus_id] : null;
                $row->user = isset($this->data['user'][$row->user_id]) ? $this->data['user'][$row->user_id] : null;
                $row->client = isset($this->data['client'][$row->client_id]) ? $this->data['client'][$row->client_id] : null;
                $row->correspondent = isset($this->data['correspondent'][$row->correspondent_id]) ? $this->data['correspondent'][$row->correspondent_id] : null;
                $row->city = isset($this->data['city'][$row->city]) ? $this->data['city'][$row->city] : null;
                $row->processfee_totalized = isset($this->data['processfee_totalized'][$row->id]) ? $this->data['processfee_totalized'][$row->id] : null;


                $row->deadline_db = $row->deadline ? $row->deadline : convertDate($row->deadline_);
                $row->timestamp = (int)strtotime($row->deadline_db);

                $row->deadline = convertDate($row->deadline_db);

                $diff = new DateTime();

                $row->d = $diff = $diff->diff(new DateTime($row->deadline_db));

                if (!$row->deadline || $diff->invert === 1) {
                    $row->signal = 'red';
                } elseif ($diff->days >= 2) {
                    $row->signal = 'green';
                } else {
                    $row->signal = 'yellow';
                }


                $row->link = './processos/visualizar/' . $row->id;

                $row->honorarytypeservice = $this->get_honorarytypeservice(array('process_id' => $row->id), false);
                $row->honorarytypeservice = current($row->honorarytypeservice);

                $this->data['list'][$key] = $row;
            }
        }

        if ($this->router->method == 'follow') {

            $this->get_processcategory();

            $this->renderer();
        }
    }

    private function get_processfee_totalized(array $where = array(), $return = false)
    {

        $this->load->model('processfee_model');
        $data = $this->processfee_model->totalize($where)->result();

        $processfee = array();

        foreach ($data as $key => $row) {
            $processfee[$row->process_id][$row->type] = $row->total;
        }
        if ($return) {
            return $processfee;
        } else {
            $this->data['processfee_totalized'] = $processfee;
            return count($processfee) > 0;
        }
    }

    private function get_honorarytypeservice(array $where = array(), $return = false)
    {

        $this->load->model('honorarytypeservice_model');
        $data = $this->honorarytypeservice_model->get($where)->result();

        $honorarytypeservice = array();

        foreach ($data as $key => $row) {
            $honorarytypeservice[$row->id] = $row;
        }
        if ($return) {
            $this->data['honorarytypeservice'] = $honorarytypeservice;
            return count($honorarytypeservice) > 0;
        } else {
            return $honorarytypeservice;
        }
    }

    public function notify($id)
    {
        $data = $this->process_model->get(array(
            'account_id' => $this->data['me']->account_id,
            'id' => $id
        ))->result();

        $data = current($data);

        $data->correspondent = $this->get_correspondent($data->correspondent_id);
        $data->correspondent = current($data->correspondent);

        $data->correspondent->email = $this->get_correspondentemail($data->correspondent_id);
        $data->correspondent->email = current($data->correspondent->email);
        $data->correspondent->email = $data->correspondent->email->email;

        if (!isset($data->correspondent->email)) {
            $this->setError('Nenhum email cadastrado.');
        } else {
            $data->judicialpanel = $this->get_judicialpanel($data->judicialpanel_id);
            $data->judicialpanel = current($data->judicialpanel);

            $data->user = $this->get_user($data->user_id);
            $data->user = current($data->user);

            $data->city = $this->get_city($data->city);
            $data->city = current($data->city);

            $data->link = base_url($this->uri->segment(1) . '/visualizar/' . $id);

            $data->message = $this->load->view('email/process_change', $data, true);

            $this->send_email($data);
        }

        redirect('processos/acompanhamento');
    }

    public function index()
    {
        $this->load_datatable();
        $this->load->model('client_model');
        $this->load->model('correspondent_model');
        $list = $this->process_model->get()->result();
        foreach ($list as $key => $row) {
            $client = current($this->client_model->get(array('id' => $row->client_id))->result());
            $correspondent = current($this->correspondent_model->get(array('id' => $row->correspondent_id))->result());
            if ($client) {
                $row->client_name = $client->name;
            } else {
                $row->client_name = "Nenhum Cliente informado";
            }
            if ($correspondent) {
                $row->correspodent_name = $correspondent->name;
            } else {
                $row->correspodent_name = "Nenhum Correspondente informado";
            }

            $list[$key] = $row;
        }
        $this->data['list'] = $list;
        $this->setPageTitle('Lista de Usuários');

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        return count((array)$data) !== 0 ? $data : null;
    }

    private function get_processcategory(array $where = array())
    {

        $this->load->model('processcategory_model');
        $data = $this->processcategory_model->get($where)->result();

        $this->data['processcategory'] = array();

        foreach ($data as $key => $row) {
            $this->data['processcategory'][$row->id] = $row;
        }
        return $this->data['processcategory'];
    }

    public function form($id = FALSE)
    {
        $this->get_processcategory();

        $this->get_user();

        $this->load_mask();
        $id = ($this->uri->segment(1) == 'meu-perfil') ? $this->data['me']->id : $id;
        $client = array();
        $city = array();
        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {
            $this->load->model('client_model');
            $this->load->model('city_model');
            $this->load->model('coststypeservice_model');
            $this->load->model('honorarytypeservice_model');


            $this->get_judicialpanel();

            $city = $this->city_model->get(array('account_id' => $this->data['me']->account_id))->result();
            $client = $this->client_model->get(array('account_id' => $this->data['me']->account_id, 'status' => 1))->result();

            if ($id > 0) {
                $data = $this->process_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();
                $this->data['data'] = (count($data) > 0) ? current($data) : NULL;
                if ($this->data['data'] != NULL) {

                    $this->data['data']->deadline = $this->data['data']->deadline ? convertDate($this->data['data']->deadline) : $this->data['data']->deadline_;

                    $this->load->model('shippingprocess_model');
                    $correspondents = $this->get_correspondents($this->data['data']->city);
                    $client_lawarea = $this->get_client_lawarea($this->data['data']->client_id);
                    $correspondent_lawarea = $this->get_correspondent_lawarea($this->data['data']->correspondent_id);
                    $processfee = $this->get_processfee($id);
                    $processfeenote = $this->get_processfeenote($id);
                    $processcoststypeservice = $this->get_coststypeservice($id);

                    $this->data['data']->clientcoststypeservice = $processcoststypeservice['clientcoststypeservice'];
                    $this->data['data']->correspondentcoststypeservice = $processcoststypeservice['correspondentcoststypeservice'];
                    $this->data['data']->clientfeenote = $processfeenote['clientfeenote'];
                    $this->data['data']->correspondentfeenote = $processfeenote['correspondentfeenote'];
                    $this->data['data']->clientfee = $processfee['clientfee'];
                    $this->data['data']->correspondentfee = $processfee['correspondentfee'];
                    $this->data['data']->correspondent_lawarea = $correspondent_lawarea;
                    $this->data['data']->client_lawarea = $client_lawarea;
                    $this->data['data']->correspondents = $correspondents;
                    $this->data['data']->shippingprocess = $this->shippingprocess_model->get(array('client_id' => $this->data['data']->client_id, 'account_id' => $this->data['me']->account_id))->result();
                }
            }
        }
        $this->data['coststypeservice'] = $this->coststypeservice_model->get(array('account_id' => $this->data['me']->account_id))->result();
        $this->data['honorarytypeservice'] = $this->honorarytypeservice_model->get(array('account_id' => $this->data['me']->account_id))->result();
        $this->data['city'] = $city;
        $this->data['client'] = $client;
        $this->load_chosen();
        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de Processos - ' . $this->data['data']->name : 'Cadastro de Processos'));
        $this->renderer();
    }

    public function report($id)
    {
        $this->load_mask();

        $this->get_processcategory();

        $this->load_mask();
        $id = ($this->uri->segment(1) == 'meu-perfil') ? $this->data['me']->id : $id;
        $client = array();
        $city = array();
        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {
            $this->load->model('client_model');
            $this->load->model('city_model');
            $this->load->model('coststypeservice_model');
            $this->load->model('honorarytypeservice_model');
            $city = $this->city_model->get(array('account_id' => $this->data['me']->account_id))->result();
            $client = $this->client_model->get(array('account_id' => $this->data['me']->account_id, 'status' => 1))->result();

            if ($id > 0) {
                $data = $this->process_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();
                $this->data['data'] = (count($data) > 0) ? current($data) : NULL;
                if ($this->data['data'] != NULL) {
                    $this->load->model('shippingprocess_model');
                    $correspondents = $this->get_correspondents($this->data['data']->city);
                    $client_lawarea = $this->get_client_lawarea($this->data['data']->client_id);
                    $correspondent_lawarea = $this->get_correspondent_lawarea($this->data['data']->correspondent_id);
                    $processfee = $this->get_processfee($id);
                    $processfeenote = $this->get_processfeenote($id);
                    $processcoststypeservice = $this->get_coststypeservice($id);

                    $this->data['data']->clientcoststypeservice = $processcoststypeservice['clientcoststypeservice'];
                    $this->data['data']->correspondentcoststypeservice = $processcoststypeservice['correspondentcoststypeservice'];
                    $this->data['data']->clientfeenote = $processfeenote['clientfeenote'];
                    $this->data['data']->correspondentfeenote = $processfeenote['correspondentfeenote'];
                    $this->data['data']->clientfee = $processfee['clientfee'];
                    $this->data['data']->correspondentfee = $processfee['correspondentfee'];
                    $this->data['data']->correspondent_lawarea = $correspondent_lawarea;
                    $this->data['data']->client_lawarea = $client_lawarea;
                    $this->data['data']->correspondents = $correspondents;
                    $this->data['data']->shippingprocess = $this->shippingprocess_model->get(array('client_id' => $this->data['data']->client_id, 'account_id' => $this->data['me']->account_id))->result();
                }
            }
        }
        $this->data['coststypeservice'] = $this->coststypeservice_model->get(array('account_id' => $this->data['me']->account_id))->result();
        $this->data['honorarytypeservice'] = $this->honorarytypeservice_model->get(array('account_id' => $this->data['me']->account_id))->result();
        $this->data['city'] = $city;
        $this->data['client'] = $client;
        $this->load_chosen();
        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de Processos - ' . $this->data['data']->name : 'Cadastro de Processos'));


        $this->data['report'] = array(
            'client' => current($this->get_client($this->data['data']->client_id)),

            'taxinvoice' => 0,

            'clientfee' => 0,
            'clientcoststypeservice' => 0,

            'correspondentfee' => 0,
            'correspondentcoststypeservice' => 0,

            'out' => 0,
            'in' => 0,
            'result' => 0,
        );


        foreach ($this->data['data']->correspondentfee as $row) {
            $this->data['report']['correspondentfee'] += $row->value;
        }

        foreach ($this->data['data']->clientfee as $row) {
            $this->data['report']['clientfee'] += $row->value;
        }

        foreach ($this->data['data']->clientcoststypeservice as $row) {
            $this->data['report']['clientcoststypeservice'] += $row->value;
        }

        foreach ($this->data['data']->correspondentcoststypeservice as $row) {
            $this->data['report']['correspondentcoststypeservice'] += $row->value;
        }

        if ($this->data['report']['client']->taxinvoice == 1) {
            $this->data['report']['taxinvoice'] = round(($this->data['report']['clientfee'] * 0.05), 2);
        }
        $this->data['report']['taxinvoice'] = number_format($this->data['report']['taxinvoice'], 2, '.', '');

        $this->data['report']['in'] = $this->data['report']['clientfee'] - $this->data['report']['taxinvoice'] + $this->data['report']['clientcoststypeservice'];
        $this->data['report']['out'] = $this->data['report']['correspondentfee'] + $this->data['report']['correspondentcoststypeservice'];
        $this->data['report']['result'] = $this->data['report']['in'] - $this->data['report']['out'];

        $this->data['report']['result'] = number_format($this->data['report']['result'], 2, '.', '');
        $this->data['report']['out'] = number_format($this->data['report']['out'], 2, '.', '');
        $this->data['report']['in'] = number_format($this->data['report']['in'], 2, '.', '');
        $this->data['report']['clientfee'] = number_format($this->data['report']['clientfee'], 2, '.', '');
        $this->data['report']['correspondentfee'] = number_format($this->data['report']['correspondentfee'], 2, '.', '');
        $this->data['report']['correspondentcoststypeservice'] = number_format($this->data['report']['correspondentcoststypeservice'], 2, '.', '');
        $this->data['report']['clientcoststypeservice'] = number_format($this->data['report']['clientcoststypeservice'], 2, '.', '');

        $this->data['report'] = (object)$this->data['report'];


        foreach ($this->data['city'] as $row) {
            if ($this->data['data']->city == $row->id) {
                $this->data['data']->city_name = $row->name;
            }
        }
        foreach ($this->data['client'] as $row) {
            if ($this->data['data']->client_id == $row->id) {
                $this->data['data']->client_name = $row->name;
            }
        }

        foreach ($this->data['data']->correspondents as $row) {
            if ($this->data['data']->correspondent_id == $row->id) {
                $this->data['data']->correspondent_name = $row->name;
            }
        }

        $this->renderer();
    }

    private function get_coststypeservice($id)
    {
        $this->load->model('processcoststypeservice_model');
        $data = $this->processcoststypeservice_model->get(array('process_id' => $id))->result();
        $clientcoststypeservice = array();
        $correspondentcoststypeservice = array();
        foreach ($data as $key => $row) {
            if ($row->type == 'client') {
                $clientcoststypeservice[$row->coststypeservice_id] = $row;
            } else {
                $correspondentcoststypeservice[$row->coststypeservice_id] = $row;
            }
        }
        $data = array();
        $data['clientcoststypeservice'] = $clientcoststypeservice;
        $data['correspondentcoststypeservice'] = $correspondentcoststypeservice;
        return $data;

    }

    private function get_judicialpanel($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }

        $this->load->model('judicialpanel_model');
        $data = $this->judicialpanel_model->get($where)->result();

        $this->data['judicialpanel'] = array();

        foreach ($data as $key => $row) {
            $this->data['judicialpanel'][$row->id] = $row;
        }
        return $this->data['judicialpanel'];

    }

    private function get_processfeenote($id)
    {
        $this->load->model('processfeenote_model');
        $data = $this->processfeenote_model->get(array('process_id' => $id))->result();
        $clientfeenote = array();
        $correspondentfeenote = array();
        foreach ($data as $key => $row) {
            if ($row->type == 'client') {
                $clientfeenote[$row->honorarytypeservice_id] = $row;
            } else {
                $correspondentfeenote[$row->honorarytypeservice_id] = $row;
            }
        }
        $data = array();
        $data['clientfeenote'] = $clientfeenote;
        $data['correspondentfeenote'] = $correspondentfeenote;
        return $data;
    }

    private function get_processfee($id)
    {
        $this->load->model('processfee_model');
        $data = $this->processfee_model->get(array('process_id' => $id))->result();
        $clientfee = array();
        $correspondentfee = array();
        foreach ($data as $key => $row) {
            if ($row->type == 1/*'client'*/) {
                $clientfee[$row->honorarytypeservice_id] = $row;
            } else {
                $correspondentfee[$row->honorarytypeservice_id] = $row;
            }
        }
        $data = array();
        $data['clientfee'] = $clientfee;
        $data['correspondentfee'] = $correspondentfee;
        return $data;
    }

    private function get_correspondent_lawarea($id)
    {
        $this->load->model('correspondentlawareas_model');
        $this->load->model('lawareas_model');
        $data = $this->correspondentlawareas_model->get(array('correspondent_id' => $id))->result();
        foreach ($data as $key => $row) {
            $lawareas = current($this->lawareas_model->get(array('id' => $row->lawareas_id))->result());
            $row->name = $lawareas->name;
            $row->id = $row->lawareas_id;
            unset($row->lawareas_id);
            unset($row->correspondent_id);
            $data[$key] = $row;
        }
        return $data;
    }

    private function get_client_lawarea($id)
    {
        $this->load->model('clientlawareas_model');
        $this->load->model('lawareas_model');

        $data = $this->clientlawareas_model->get(array('client_id' => $id))->result();

        foreach ($data as $key => $row) {
            $lawareas = current($this->lawareas_model->get(array('id' => $row->lawareas_id))->result());
            $row->name = $lawareas->name;
            $row->id = $row->lawareas_id;
            unset($row->lawareas_id);
            unset($row->client_id);
            $data[$key] = $row;
        }
        return $data;

    }

    private function get_client($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }

        $this->load->model('client_model');
        $data = $this->client_model->get($where)->result();

        $this->data['client'] = array();

        foreach ($data as $key => $row) {
            $this->data['client'][$row->id] = $row;
        }
        return $this->data['client'];
    }

    private function get_correspondents($id)
    {
        $this->load->model('correspondentfee_model');
        $this->load->model('correspondent_model');
        $correspondents = $this->correspondentfee_model->get(array('city_id' => $id))->result();
        $already = array();
        foreach ($correspondents as $key => $row) {
            if (!in_array($row->correspondent_id, $already)) {
                $row2 = current($this->correspondent_model->get(array('id' => $row->correspondent_id))->result());
                unset($correspondents[$key]);
                $correspondents[count($already)] = $row2;
                array_push($already, $row->correspondent_id);
            } else {
                unset($correspondents[$key]);
            }
        }

        return $correspondents;
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $this->load->library('form_validation');

            if ($id > 0) {

            }

            $this->form_validation->set_rules('code', ' Código/ID', 'required');


            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                $redirect = ($id > 0) ? '/editar/' . $id : '/novo';
                redirect(base_url($this->uri->segment(1) . $redirect));
            } else {
                $this->load->model('processfee_model');
                $this->load->model('processfeenote_model');
                $this->load->model('clientfee_model');
                $this->load->model('correspondentfee_model');
                $this->load->model('processcoststypeservice_model');
                if ($this->input->post('local') == 1) {
                    $local = 1;
                } else {
                    $local = 0;
                }
                $city = $this->input->post('city_id');
                $client_id = $this->input->post('client_id');
                $correspondent_id = $this->input->post('correspondent_id');
                $automatic_clientfee = $this->input->post('automatic_clientfee') ? $this->input->post('automatic_clientfee') : array();
                $process_clientfeenote = $this->input->post('clientfee-note');
                $process_correspondentfeenote = $this->input->post('corespondentfee-note');
                $correspondentfee = $this->correspondentfee_model->get(array('correspondent_id' => $client_id, 'city_id' => $city))->result();
                $client_coststypeservice = $this->input->post('client_coststypeservice');
                $correspondent_coststypeservice = $this->input->post('correspondent_coststypeservice');
                $client_coststypeservice_refound = $this->input->post('client_coststypeservice_refound');
                $correspondent_coststypeservice_refound = $this->input->post('correspondent_coststypeservice_refound');
                $clientfee = $this->clientfee_model->get(array('client_id' => $client_id, 'city_id' => $city))->result();

                $process_coststypeservice = array();
                foreach ($clientfee as $key => $row) {
                    $clientfee[$row->honorarytypeservice_id] = $row;
                }
                foreach ($correspondentfee as $key => $row) {
                    $correspondentfee[$row->honorarytypeservice_id] = $row;
                }
                $automatic_corespondentfee = $this->input->post('automatic_corespondentfee') ? $this->input->post('automatic_corespondentfee') : array();
                $data = array(
                    'client_id' => $client_id,
                    'note' => strtoupper($this->input->post('note')),
                    'processcategory_id' => $this->input->post('processcategory_id'),
                    'client_lawarea_id' => $this->input->post('client_lawarea_id'),
                    'user_id' => $this->input->post('user_id'),
                    'correspondent_lawarea_id' => $this->input->post('correspondent_lawarea_id'),
                    'preposed' => $this->input->post('preposed'),
                    'lawyer' => $this->input->post('lawyer'),
                    'shippingprocess_id' => $this->input->post('shippingprocess_id'),
                    'correspondent_id' => $correspondent_id,
                    'number' => $this->input->post('number'),
                    'author' => $this->input->post('author'),
                    'date' => $this->input->post('date'),
                    'hour' => $this->input->post('hour'),
                    'defendant' => $this->input->post('defendant'),
                    'judicialpanel_id' => $this->input->post('judicialpanel_id'),
                    'deadline' => convertDate($this->input->post('deadline')),
                    'local' => $local,
                    'city' => $this->input->post('city_id'),
                    'code' => $this->input->post('code'),
                );
                $process_clientfee = $this->input->post('clientfee');
                $process_correspondentfee = $this->input->post('corespondentfee');
                if ($id > 0) {
                    $update = $this->process_model->update(array(
                        'account_id' => $this->data['me']->account_id,
                        'id' => $id
                    ), $data);
                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }
                } else {
                    $data['account_id'] = $this->data['me']->account_id;
                    $id = $this->process_model->insert($data, TRUE);
                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
                $this->processcoststypeservice_model->delete(array(
                    'process_id' => $id
                ));

                $this->processfee_model->delete(array(
                    'process_id' => $id
                ));
                $this->processfeenote_model->delete(array(
                    'process_id' => $id
                ));
                foreach ($process_clientfeenote as $key => $row) {
                    $row = (object)$row;
                    $row->note = $row->scalar;
                    $row->process_id = $id;
                    $row->honorarytypeservice_id = $key;
                    $row->type = "client";
                    unset($row->scalar);
                    $process_clientfeenote[$key] = $row;
                }
                foreach ($process_correspondentfeenote as $key => $row) {
                    $row = (object)$row;
                    $row->note = $row->scalar;
                    $row->process_id = $id;
                    $row->honorarytypeservice_id = $key;
                    $row->type = "correspondent";
                    unset($row->scalar);
                    $process_corespondentfeenote[$key] = $row;
                }
                foreach ($automatic_clientfee as $key => $row) {
                    $row = (object)$row;
                    $atual = current($this->clientfee_model->get(
                        array('honorarytypeservice_id' => $key,
                            'city_id' => $this->input->post('city_id'),
                            'client_id' => $client_id
                        ))->result());
                    if ($atual) {
                        $row->value = $atual->value;
                    } else {
                        $row->value = 0;
                    }
                    $row->automatic = 1;
                    $row->type = 1;//"client";
                    $row->honorarytypeservice_id = $key;
                    $row->process_id = $id;
                    unset($row->scalar);
                    $process_clientfee[$key] = $row;
                }
                foreach ($process_clientfee as $key => $row) {
                    if (!is_object($row)) {
                        $row = (object)$row;
                        $row->scalar = str_replace(".", "", $row->scalar);
                        $row->value = str_replace(",", ".", $row->scalar);
                        $row->automatic = 0;
                        $row->type = 1;//"client";
                        $row->honorarytypeservice_id = $key;
                        $row->process_id = $id;
                        unset($row->scalar);
                    }
                    $process_clientfee[$key] = $row;
                }
                foreach ($automatic_corespondentfee as $key => $row) {
                    $row = (object)$row;
                    $atual = current($this->correspondentfee_model->get(
                        array('honorarytypeservice_id' => $key,
                            'city_id' => $this->input->post('city_id'),
                            'correspondent_id' => $correspondent_id
                        ))->result());
                    if ($atual) {
                        $row->value = $atual->value;
                    } else {
                        $row->value = 0;
                    }
                    $row->automatic = 1;
                    $row->type = 2;//"correspondent";
                    $row->honorarytypeservice_id = $key;
                    $row->process_id = $id;
                    unset($row->scalar);
                    $process_correspondentfee[$key] = $row;
                }
                foreach ($process_correspondentfee as $key => $row) {
                    if (!is_object($row)) {
                        $row = (object)$row;
                        $row->scalar = str_replace(".", "", $row->scalar);
                        $row->value = str_replace(",", ".", $row->scalar);
                        $row->automatic = 0;
                        $row->type = 2;//"correspondent";
                        $row->honorarytypeservice_id = $key;
                        $row->process_id = $id;
                        unset($row->scalar);
                    }
                    $process_correspondentfee[$key] = $row;
                }

                foreach ($client_coststypeservice as $key => $row) {
                    $row = (object)$row;
                    $row->scalar = str_replace(".", "", $row->scalar);
                    $row->value = str_replace(",", ".", $row->scalar);
                    $row->coststypeservice_id = $key;
                    $row->process_id = $id;
                    $row->type = 'client';
                    if (isset($client_coststypeservice_refound[$key])) {
                        $row->refound = 1;
                    } else {
                        $row->refound = 0;
                    }
                    unset($row->scalar);
                    $process_coststypeservice[] = $row;
                }
                foreach ($correspondent_coststypeservice as $key => $row) {
                    $row = (object)$row;
                    $row->scalar = str_replace(".", "", $row->scalar);
                    $row->value = str_replace(",", ".", $row->scalar);
                    $row->coststypeservice_id = $key;
                    $row->process_id = $id;
                    $row->type = 'correspondent';
                    if (isset($correspondent_coststypeservice_refound[$key])) {
                        $row->refound = 1;
                    } else {
                        $row->refound = 0;
                    }
                    unset($row->scalar);
                    $process_coststypeservice[] = $row;
                }
                if (count($process_coststypeservice) > 0) {
                    $this->processcoststypeservice_model->insert_batch($process_coststypeservice);
                }
                if (count($process_correspondentfee) > 0) {
                    $this->processfee_model->insert_batch($process_correspondentfee);
                }
                if (count($process_clientfee) > 0) {
                    $this->processfee_model->insert_batch($process_clientfee);
                }
                if (count($process_clientfeenote) > 0) {
                    $this->processfeenote_model->insert_batch($process_clientfeenote);
                }
                if (count($process_corespondentfeenote) > 0) {
                    $this->processfeenote_model->insert_batch($process_corespondentfeenote);
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }

    public function delete($id = FALSE)
    {
        if ($id && $this->data['me']->user_type == 2) {
            if ($this->process_model->delete(array(
                'id' => $id
            ))
            ) {
                $this->setMsg($this->lang->line('successfully_data_delete'));
            } else {
                $this->setError($this->lang->line('error_processing_delete'));
            }

        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }

        redirect(base_url($this->uri->segment(1)));
    }
    public function excelBalance()
    {
        //load our new PHPExcel library
        $this->load->library('excel');

        $this->excel->setActiveSheetIndex(0);
        $sheet = $this->excel->getActiveSheet();

        $sheet->setTitle('Entradas e balanço geral');

        $l = 1;
        $c = 'A';
        $sheet->setCellValue($c . $l, 'Status');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Data recebimento');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Cliente');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Processo');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Valor honorarios');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Valor reembolso despesas cliente');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Valor despesas correspondente');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Honorário do correspondente');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Nº nota fiscal');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Data emissão nota fiscal');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Valor imposto nota fiscal');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Valor deduzido imposto');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Lucratividade');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);

        foreach ($this->data['list'] as $row) {
            $c = 'A';
            $l++;
            $sheet->setCellValue($c . $l, (isset($row->processstatus2->name) ? $row->processstatus2->name : null));
            $c++;
            $sheet->setCellValue($c . $l, (($row->receivedate) ? $row->receivedate : null));
            $c++;
            $sheet->setCellValue($c . $l, (isset($row->client->name) ? $row->client->name : null) . ' - ' . (isset($row->client->code) ? $row->client->code : null));
            $c++;
            $sheet->setCellValue($c . $l, $row->number);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->clientfee) ? $row->clientfee: null);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->clientcoststypeservice) ? $row->clientcoststypeservice: null);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->correspondentcoststypeservice) ? $row->correspondentcoststypeservice: null);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->correspondentfee) ? $row->correspondentfee: null);
            $c++;
            $sheet->setCellValue($c . $l, null);
            $c++;
            $sheet->setCellValue($c . $l, null);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->taxinvoice) ? $row->taxinvoice: null);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->clientliquidvalue) ? $row->clientliquidvalue: null);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->profitability) ? $row->profitability: null);
            $c++;
        }

        $this->excel->setActiveSheetIndex(0);


        //change the font size
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
        //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename = NAME_SITE . 'Acompanhamento de entradas e balanço geral' . rand() . '.xlsx'; //save our workbook as this file name


        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function excelClient()
    {
        //load our new PHPExcel library
        $this->load->library('excel');

        $this->excel->setActiveSheetIndex(0);
        $sheet = $this->excel->getActiveSheet();

        $sheet->setTitle('Cliente');

        $l = 1;
        $c = 'A';
        $sheet->setCellValue($c . $l, 'N.');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'CLIENTE');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'CODIGO/PASTA');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'NÚMERO DO PROCESSO');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'PARTE CONTRÁRIA (AUTOR)');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'COMARCA');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'VARA');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'DATA DA SOLICITAÇÃO');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Prazo Fatal');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Honorários por Tipo de Serviço');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'ADVOGADO');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'PREPOSTO');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'ADVOGADO SOLICITANTE');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'NOME DO CORREPONDENTE');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'RÉU');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'VALOR DOS HONORÁRIOS CLENTES');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Custas Judiciais');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Cliente Cópias');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Correios Cliente');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'VALOR DOS HONORÁRIOS CORRESPONDENTE');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'VALOR DESPESAS CORRESPONDENTE');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'VALOR TOTAL CORRESPONDENTE');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;


        foreach ($this->data['list'] as $row) {
            $c = 'A';
            $l++;
            $sheet->setCellValue($c . $l, $row->id);
            $c++;
            $sheet->setCellValue($c . $l, $row->client->name);
            $c++;
            $sheet->setCellValue($c . $l, $row->client->code);
            $c++;
            $sheet->setCellValue($c . $l, $row->number);
            $c++;
            $sheet->setCellValue($c . $l, $row->author);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->client->city) ? $row->client->city : null);
            $c++;
            $sheet->setCellValue($c . $l, (isset($this->data['judicialpanel'][$row->judicialpanel_id]->name) ? $this->data['judicialpanel'][$row->judicialpanel_id]->name : null));
            $c++;
            $sheet->setCellValue($c . $l, $row->date);
            $c++;
            $sheet->setCellValue($c . $l, $row->deadline);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->honorarytypeservice->name) ? $row->honorarytypeservice->name : NULL);
            $c++;
            $sheet->setCellValue($c . $l, $row->lawyer);
            $c++;
            $sheet->setCellValue($c . $l, $row->preposed);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->shippingprocess->name) ? $row->shippingprocess->name : null);
            $c++;
            $sheet->setCellValue($c . $l, isset($row->correspondent->name) ? $row->correspondent->name : NULL);
            $c++;
            $sheet->setCellValue($c . $l, $row->defendant);
            $c++;
            $sheet->setCellValue($c . $l, $row->clientfee);
            $c++;
            $sheet->setCellValue($c . $l, $row->clientcoststypeservice);
            $c++;
            $sheet->setCellValue($c . $l, null);
            $c++;
            $sheet->setCellValue($c . $l, null);
            $c++;
            $sheet->setCellValue($c . $l, $row->correspondentfee);
            $c++;
            $sheet->setCellValue($c . $l, $row->correspondentcoststypeservice);
            $c++;
            $sheet->setCellValue($c . $l, $row->correspondenttotalcost);
            $c++;
        }

        $this->excel->setActiveSheetIndex(0);


        //change the font size
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
        //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename = NAME_SITE . 'Acompanhamento de entradas e balanço geral' . rand() . '.xlsx'; //save our workbook as this file name


        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
}