<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('client_model', 'clientfinancial_model'));
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index()
    {
        $this->load_datatable();
        $this->setPageTitle("Clientes");
        $this->data['list'] = $this->client_model->get()->result();
        $this->renderer();
    }

    public function form($id = NULL){
        $this->load_mask();
        $this->load_chosen();

        $this->loadJs(array(
            'name' => 'automatic_value'
        ));
        $this->loadCss(array(
            'name' => 'automatic_value'
        ));

        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        $clientfinancial = array();

        $clientlawareas = array();

        $clientcoststypeservice = array();

        $clientfee = array();

        $cities_selected = array();

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;

            $clientfinancial = $session_form->clientfinancial;

            $clientlawareas = $session_form->clientlawareas;

            $clientcoststypeservice = $session_form->clientcoststypeservice;

            $clientfee = $session_form->clientfee;

            if ($clientfee && count($clientfee) > 0) {
                foreach ($clientfee as $row) {
                    foreach ($row['city'] as $city_id => $value) {
                        $cities_selected[$city_id] = $city_id;
                    }
                }
            }

        } else {

            if ($id > 0) {
                $data = $this->client_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();

                $this->data['data'] = (count($data) > 0) ? current($data) : NULL;

                $clientfinancial = $this->clientfinancial_model->get(array(
                    'client_id' => $id
                ))->result();

                $this->load->model('clientlawareas_model');

                $clientlawareas_get = $this->clientlawareas_model->get(array(
                    'client_id' => $id
                ))->result();

                if (count($clientlawareas_get)) {
                    foreach ($clientlawareas_get as $row) {
                        $clientlawareas[] = $row->lawareas_id;
                    }
                }

                $this->load->model('clientcoststypeservice_model');

                $clientcoststypeservice_get = $this->clientcoststypeservice_model->get(array(
                    'client_id' => $id
                ))->result();

                if (count($clientcoststypeservice_get)) {
                    foreach ($clientcoststypeservice_get as $row) {
                        $clientcoststypeservice[] = $row->coststypeservice_id;
                    }
                }

                $this->load->model('clientfee_model');

                $clientfee_get = $this->clientfee_model->get(array(
                    'client_id' => $id
                ))->result();

                foreach ($clientfee_get as $row) {
                    $clientfee[$row->honorarytypeservice_id]['city'][$row->city_id]['value'] = $row->value;
                    $cities_selected[$row->city_id] = $row->city_id;
                }

                $this->load->model('clientfeenote_model');

                $clientfeenote_get = $this->clientfeenote_model->get(array(
                    'client_id' => $id
                ))->result();

                foreach ($clientfeenote_get as $row) {
                    $clientfee[$row->honorarytypeservice_id]['note'] = $row->note;
                }

            } else {
                $this->data['data'] = (object)array();
            }
        }

        $this->data['data']->clientfinancial = (object)$clientfinancial;

        $this->data['data']->clientlawareas = $clientlawareas;

        $this->data['data']->clientcoststypeservice = $clientcoststypeservice;

        $this->data['data']->clientfee = $clientfee;

        $this->data['data']->cities_selected = $cities_selected;

        $this->load->model('lawareas_model');
        $this->data['data']->lawareas = $this->lawareas_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->load->model('coststypeservice_model');
        $this->data['data']->coststypeservice = $this->coststypeservice_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->load->model('honorarytypeservice_model');
        $this->data['data']->honorarytypeservice = $this->honorarytypeservice_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->load->model('citygroup_model');
        $this->data['data']->citygroup = $this->citygroup_model->get(array('account_id' => $this->data['me']->account_id))->result();

        $this->load->model('city_model');
        $this->data['data']->cities = $this->city_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de Cliente - ' . $this->data['data']->name : 'Cadastro de Cliente'));

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        if (isset($_POST['clientfinancial'])) {
            $data->clientfinancial = array();
            foreach ($_POST['clientfinancial'] as $key => $row) {
                $data->clientfinancial[$key] = (object)$row;
            }
        }

        $data->clientlawareas = array();
        if (isset($_POST['lawareas_id'])) {
            foreach ($_POST['lawareas_id'] as $key => $row) {
                $data->clientlawareas[$key] = $row;
            }
        }

        $data->clientcoststypeservice = array();
        if (isset($_POST['coststypeservice_id'])) {
            foreach ($_POST['coststypeservice_id'] as $key => $row) {
                $data->clientcoststypeservice[$key] = $row;
            }
        }

        if (isset($_POST['clientfee'])) {
            $data->clientfee = array();
            foreach ($_POST['clientfee'] as $key => $row) {
                $data->clientfee[$key] = $row;
            }
        }

        return count((array)$data) !== 0 ? $data : null;
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $this->load->library('form_validation');

            $client = (object)array();

            if ($id > 0) {
                $client = $this->client_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();

                if (count($client) > 0) $client = current($client);
            }

            $is_unique_code = (isset($client->code) && $client->code != $this->input->post('code')) ? 'is_unique[client.code]' : '';
            $this->form_validation->set_rules('code', 'Código do Cliente', 'trim|' . $is_unique_code);

            $clientfinancial = $this->input->post('clientfinancial');
            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());

                $redirect = ($id > 0) ? '/' . $id : '';

                redirect(base_url($this->uri->segment(1) . $redirect));

            } else {

                $file_path = './assets/upload/' . $this->router->class . '/';

                $config['upload_path'] = $file_path;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '5000'; // 5mb
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);

                if (!empty($_FILES['img']['name']) && !$this->upload->do_upload('img')) {

                    $this->setError($this->upload->display_errors());

                    $redirect = ($id > 0) ? '/' . $id : '';

                    redirect(base_url($this->uri->segment(1) . $redirect));

                } else {

                    $upload = $this->upload->data();

                    $data = array(
                        'code' => $this->input->post('code'),
                        'name' => $this->input->post('name'),
                        'companyname' => $this->input->post('companyname'),
                        'municipalregistration' => $this->input->post('municipalregistration'),
                        'stateregistration' => $this->input->post('stateregistration'),
                        'cnpj' => $this->input->post('cnpj'),
                        'street' => $this->input->post('street'),
                        'number' => $this->input->post('number'),
                        'complement' => $this->input->post('complement'),
                        'cep' => $this->input->post('cep'),
                        'city' => $this->input->post('city'),
                        'district' => $this->input->post('district'),
                        'state' => $this->input->post('state'),
                        'maincontact' => $this->input->post('maincontact'),
                        'mainphone1' => $this->input->post('mainphone1'),
                        'mainphone2' => $this->input->post('mainphone2'),
                        'mainphone3' => $this->input->post('mainphone3'),
                        'mainphone4' => $this->input->post('mainphone4'),
                        'mainphone5' => $this->input->post('mainphone5'),
                        'mainphone6' => $this->input->post('mainphone6'),
                        'mainphone7' => $this->input->post('mainphone7'),
                        'mainphone8' => $this->input->post('mainphone8'),
                        'mainemail1' => $this->input->post('mainemail1'),
                        'mainemail2' => $this->input->post('mainemail2'),
                        'mainemail3' => $this->input->post('mainemail3'),
                        'site' => $this->input->post('site'),
                        'taxinvoice' => $this->input->post('taxinvoice')
                    );

                    if ($upload['file_name']) {
                        $data['img'] = $file_path . $upload['file_name'];

                        if ($client->img) {
                            unlink($client->img);
                        }
                    }

                    if ($id > 0) {
                        $update = $this->client_model->update(array(
                            'account_id' => $this->data['me']->account_id,
                            'id' => $id
                        ), $data);

                        if (!$update) {
                            $this->setError($this->lang->line('error_processing_form'));
                        } else {
                            $this->session->unset_userdata($this->form_key);
                            $this->setMsg($this->lang->line('successfully_data'));
                        }

                    } else {
                        $data['account_id'] = $this->data['me']->account_id;

                        $id = $this->client_model->insert($data, TRUE);

                        if (!$id) {
                            $this->setError($this->lang->line('error_processing_form'));
                        } else {
                            $this->session->unset_userdata($this->form_key);
                            $this->setMsg($this->lang->line('successfully_data_insert'));
                        }
                    }

                    if ($id > 0 && $this->data['error'] === NULL) {

                        $this->clientfinancial_model->delete(array(
                            'client_id' => $id
                        ));

                        if (isset($clientfinancial) && count($clientfinancial) > 0) {

                            foreach ($clientfinancial as $row) {
                                $row['client_id'] = $id;

                                $this->clientfinancial_model->insert($row);
                            }
                        }

                        $this->load->model('clientlawareas_model');

                        $this->clientlawareas_model->delete(array(
                            'client_id' => $id
                        ));

                        $lawareas = $this->input->post('lawareas_id');

                        if ($lawareas && count($lawareas) > 0) {
                            foreach ($lawareas as $row) {
                                $this->clientlawareas_model->insert(array(
                                    'client_id' => $id,
                                    'lawareas_id' => $row
                                ));
                            }
                        }

                        $this->load->model('clientcoststypeservice_model');

                        $this->clientcoststypeservice_model->delete(array(
                            'client_id' => $id
                        ));

                        $clientcoststypeservice = $this->input->post('coststypeservice_id');

                        if ($clientcoststypeservice && count($clientcoststypeservice) > 0) {
                            foreach ($clientcoststypeservice as $row) {
                                $this->clientcoststypeservice_model->insert(array(
                                    'client_id' => $id,
                                    'coststypeservice_id' => $row
                                ));
                            }
                        }

                        $clientfee_post = $this->input->post('clientfee');

                        $clientfee = array();
                        $clientfeenote = array();

                        $this->load->model(array('clientfee_model', 'clientfeenote_model'));

                        $this->clientfee_model->delete(array(
                            'client_id' => $id
                        ));

                        $this->clientfeenote_model->delete(array(
                            'client_id' => $id
                        ));


                        if ($clientfee_post && count($clientfee_post) > 0) {
                            foreach ($clientfee_post as $honorarytypeservice_id => $row) {
                                foreach ($row['city'] as $city_id => $value) {
                                    $clientfee[] = array(
                                        'client_id' => $id,
                                        'city_id' => $city_id,
                                        'honorarytypeservice_id' => $honorarytypeservice_id,
                                        'value' => $value['value'] ? $value['value'] : NULL
                                    );
                                }

                                $clientfeenote[] = array(
                                    'client_id' => $id,
                                    'honorarytypeservice_id' => $honorarytypeservice_id,
                                    'note' => $row['note'] ? $row['note'] : NULL
                                );
                            }

                            if (count($clientfee) > 0) $this->clientfee_model->insert_batch($clientfee);
                            if (count($clientfeenote) > 0) $this->clientfeenote_model->insert_batch($clientfeenote);


                        }

                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }
}