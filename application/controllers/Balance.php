<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balance extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        redirect(base_url($this->uri->segment(1)));
    }
}