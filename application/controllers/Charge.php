<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charge extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('charge_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    private function get_client($id = null)
    {
        $where = array();

        if ($id !== null) {
            $where['id'] = $id;
        }

        $this->load->model('client_model');
        $data = $this->client_model->get($where)->result();

        $this->data['client'] = array();

        foreach ($data as $key => $row) {
            $this->data['client'][$row->id] = $row;
        }
        return $this->data['client'];
    }

    private function reportCorrespondentAndClient()
    {
        $filter = array();
        if ($this->input->get('deadlinestart')) {
            $filter['deadline >='] = convertDate($this->input->get('deadlinestart'));
        }

        if ($this->input->get('deadlineend')) {
            $filter['deadline <='] = convertDate($this->input->get('deadlineend'));
        }

        $client = $this->get_client();


        $this->load->model('process_model');
        $process = $this->process_model->get($filter)->result();

        $this->data['correspondentValue'] = array();
        $this->data['clientValue'] = array();
        $this->data['balance'] = array();

        $this->data['correspondentValue']['total'] = 0;
        $this->data['clientValue']['total'] = 0;
        $this->data['balance']['total'] = 0;

        foreach ($process as $row) {
            if(!isset($this->data['correspondentValue'][date('01-m-Y', strtotime(($row->deadline)))])) {
                $this->data['correspondentValue'][date('01-m-Y', strtotime(($row->deadline)))] = 0;
            }
            if(!isset($this->data['clientValue'][date('01-m-Y', strtotime(($row->deadline)))])) {
                $this->data['clientValue'][date('01-m-Y', strtotime(($row->deadline)))] = 0;
            }
            if(!isset($this->data['balance'][date('01-m-Y', strtotime(($row->deadline)))])) {
                $this->data['balance'][date('01-m-Y', strtotime(($row->deadline)))] = 0;
            }
            $value = $this->calc_correspondent_value($row->id);

            $clientValue = $this->calc_client_value($row->id);


            if ($client[$row->client_id]->taxinvoice == 1) {
                $clientValue -= round(($clientValue * 0.05), 2);
            }

            $this->data['correspondentValue']['total'] += $value;
            $this->data['correspondentValue'][date('01-m-Y', strtotime(($row->deadline)))] += $value;

            $this->data['clientValue'][date('01-m-Y', strtotime(($row->deadline)))] += $clientValue;
            $this->data['clientValue']['total'] += $clientValue;

            $this->data['balance'][date('01-m-Y', strtotime(($row->deadline)))] += $clientValue - $value;
            $this->data['balance']['total'] += $clientValue;


            $this->data['totalMonth_value'][date('01-m-Y', strtotime(($row->deadline)))] += $value;
            $this->data['totalMonth_value']['total'] += $value;
        }


        $this->data['balance']['total'] -= $this->data['totalMonth_value']['total'];


        foreach ($this->data['correspondentValue'] as $key => $row) {
            $this->data['correspondentValue'][$key] = number_format($row, 2, ',', '.');
        }
        foreach ($this->data['clientValue'] as $key => $row) {
            $this->data['clientValue'][$key] = number_format($row, 2, ',', '.');
        }
        foreach ($this->data['balance'] as $key => $row) {
            $this->data['balance'][$key] = number_format($row, 2, ',', '.');
        }
    }

    public function report()
    {
        $this->setPageTitle('Balanço financeiro DMK');
        $this->lang->load('calendar');
        $this->load->helper('language');

        $this->loadCss(array(
            'name' => 'jquery.jqplot',
            'path' => 'assets/jquery.jqplot/'
        ));
        $this->loadJs(array(
            'name' => 'jquery.jqplot.min',
            'path' => 'assets/jquery.jqplot/'
        ));
        $this->loadJs(array(
            'name' => 'jqplot.highlighter',
            'path' => 'assets/jquery.jqplot/plugins/'
        ));
        $this->loadJs(array(
            'name' => 'jqplot.cursor',
            'path' => 'assets/jquery.jqplot/plugins/'
        ));
        $this->loadJs(array(
            'name' => 'jqplot.pointLabels',
            'path' => 'assets/jquery.jqplot/plugins/'
        ));
        $this->loadJs(array(
            'name' => 'jqplot.categoryAxisRenderer',
            'path' => 'assets/jquery.jqplot/plugins/'
        ));
        $this->fixDeadline();

        $this->load_chargecategory();
        $this->load_chargetype();
        $this->load_mask();

        $this->split_charge_month();

        $this->reportCorrespondentAndClient();

        foreach ($this->data['totalMonth_value'] as $key => $row) {
            $this->data['totalMonth_value'][$key] = number_format($row, 2, ',', '.');
        }


        if($this->input->get('excel')) {
            $this->excelReport();
            return false;
        }
        $this->renderer();
    }

    private function fixDeadline()
    {
        if(!$this->input->get('deadlinestart')) {
            $_GET['deadlinestart'] = date('d/m/Y');
        }
        if(!$this->input->get('deadlineend')) {
            $_GET['deadlineend'] = date('d/m/Y', strtotime("+1 months", strtotime(date("y-m-d"))));
        }
    }

    private function split_charge_month()
    {
        $this->load_charge();

        $this->data['charge_monthly'] = array();

        $month = $this->fill_month($this->input->get('deadlinestart'), $this->input->get('deadlineend'));

        $this->data['month_value'] = array();
        foreach ($month as $row) {
            $this->data['month_value'][$row] = 0;
        }
        $this->data['month_value']['total'] = 0;
        $this->data['totalMonth_value'] = $this->data['month_value'];

        foreach ($this->data['charge'] as $row) {
            if (!isset($this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id])) {
                $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id] = new stdClass();
                $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id] = new stdClass();
                $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id]->item = $this->data['month_value'];
                $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id]->chargecategory_id = $row->chargecategory_id;
                $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id]->chargetype_id = $row->chargetype_id;
                $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id]->month = $month;
            }
            $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id]->item['total'] += $row->value_db;
            $this->data['totalMonth_value'][date('01-m-Y', strtotime(convertDate($row->deadline)))] += $row->value_db;
            $this->data['totalMonth_value']['total'] += $row->value_db;
            $this->data['charge_monthly'][$row->chargecategory_id . $row->chargetype_id]->item[date('01-m-Y', strtotime(convertDate($row->deadline)))] += $row->value_db;
        }
        foreach ($this->data['charge_monthly'] as $key => $item) {
            foreach ($item->item as $item_key => $item_value) {
                $item_value = number_format($item_value, 2, ',', '.');
                $item->item[$item_key] = $item_value;
            }
            $this->data['charge_monthly'][$key] = $item;
        }
    }

    private function checkDeadLine($row)
    {

        $diff = new DateTime();

        $row->d = $diff = $diff->diff(new DateTime($row->deadline));

        if (!$row->deadline || $diff->invert === 1) {
            $row->signal = 'red';
        } elseif ($diff->days >= 2) {
            $row->signal = 'green';
        } else {
            $row->signal = 'yellow';
        }
        return $row;
    }

    private function fill_month($start = false, $end = false)
    {
        if ($start) {
            $start = convertDate($start);
        } else {
            $start = date('Y-m-d');
        }
        if ($end) {
            $end = convertDate($end);
        } else {
            $end = date('Y-m-d', strtotime($start . ' + 12 month'));
        }

        $end = date('m-Y', strtotime($end));

        $data = array();

        while (!isset($exit)) {
            $current = date('m-Y', strtotime($start));

            $data['01-' . $current] = '01-' . $current;

            $start = date('Y-m-d', strtotime($start . ' + 1 month'));

            if ($end == $current) {
                $exit = true;
            }
        }
        return $data;
    }

    private function changeStatusCharge()
    {
        if($this->input->post('charge_status') && $this->input->post('id')){
            $this->charge_model->update(array('id' => $this->input->post('id')), array('chargestatus_id' => $this->input->post('charge_status')));
        }

    }
    private function changeStatusProcess()
    {
        if($this->input->post('process_status') && $this->input->post('id')){
            $this->load->model('process_model');
            $this->process_model->update(array('id' => $this->input->post('id')), array('chargestatus_id' => $this->input->post('process_status')));
        }
    }
    public function index()
    {
        $this->changeStatusCharge();
        $this->changeStatusProcess();
        $this->load_datatable();
        $this->load_mask();

        $this->load_correspondent();
        $this->load_chargecategory();
        $this->load_chargetype();
        $this->load_chargestatus();

        $this->load_charge();
        $this->load_process();

        $this->load_chargecategory();

        $this->setPageTitle('Status de despesas');

        if($this->input->get('excel')) {
            $this->excel();
            return false;
        }
        $this->renderer();
    }

    private function load_charge()
    {
        $filter = $this->load_charge_filter();
        $this->data['charge'] = $this->charge_model->get($filter)->result();

        foreach ($this->data['charge'] as $key => $row) {
            $row->deadlineTimestamp = strtotime($row->deadline);
            $row->value_db = $row->value;
            $row->value = number_format($row->value, 2, ',', '.');
            $row = $this->checkDeadLine($row);
            $row->deadline = convertDate($row->deadline);
            $this->data['charge'][$key] = $row;
        }
    }

    private function load_process()
    {
        $filter = $this->load_process_filter();

        $this->load->model('process_model');

        $this->data['process'] = $this->process_model->get($filter)->result();

        $this->data['totalCorrespondentValue'] = new stdClass();
        $this->data['totalCorrespondentValue']->correspondentcoststypeservice = 0;
        $this->data['totalCorrespondentValue']->correspondentfee = 0;
        $this->data['totalCorrespondentValue']->total = 0;

        foreach ($this->data['process'] as $key => $row) {
            $row->correspondent_name = isset($this->data['correspondent_id'][$row->correspondent_id]->name) ? $this->data['correspondent_id'][$row->correspondent_id]->name : '';


            //----------

            if(!isset($row->correspondentcoststypeservice)) {
                $row->correspondentcoststypeservice = 0;
            }
            if(!isset($row->correspondentfee)) {
                $row->correspondentfee = 0;
            }
            if(!isset($row->value)) {
                $row->total = 0;
            }

            $processcoststypeservice = $this->get_coststypeservice($row->id);
            foreach ($processcoststypeservice['correspondentcoststypeservice'] as $correspondentcoststypeservice) {
                $row->correspondentcoststypeservice += $correspondentcoststypeservice->value;
                $this->data['totalCorrespondentValue']->correspondentcoststypeservice += $correspondentcoststypeservice->value;
            }

            $processfee = $this->get_processfee($row->id);
            foreach ($processfee['correspondentfee'] as $correspondentfee) {
                $row->correspondentfee += $correspondentfee->value;
                $this->data['totalCorrespondentValue']->correspondentfee += $correspondentfee->value;
            }
            $row->total = $row->correspondentfee + $row->correspondentcoststypeservice;

            //----------


            $row->deadlineTimestamp = strtotime($row->deadline);

            $row = $this->checkDeadLine($row);

            $row->deadline = convertDate($row->deadline);

            $this->data['process'][$key] = $row;
        }
        foreach ($this->data['process'] as $key => $row) {
            $row->total_db = $row->total;
            $row->correspondentcoststypeservice_db = $row->correspondentcoststypeservice;
            $row->correspondentfee_db = $row->correspondentfee;

            $row->total = number_format($row->total, 2, ',', '.');
            $row->correspondentcoststypeservice = number_format($row->correspondentcoststypeservice, 2, ',', '.');
            $row->correspondentfee = number_format($row->correspondentfee, 2, ',', '.');
        }

        $this->data['totalCorrespondentValue']->total_db = $this->data['totalCorrespondentValue']->total;
        $this->data['totalCorrespondentValue']->correspondentcoststypeservice_db = $this->data['totalCorrespondentValue']->correspondentcoststypeservice;
        $this->data['totalCorrespondentValue']->correspondentfee_db = $this->data['totalCorrespondentValue']->correspondentfee;

        $this->data['totalCorrespondentValue']->total = $this->data['totalCorrespondentValue']->correspondentcoststypeservice + $this->data['totalCorrespondentValue']->correspondentfee;

        $this->data['totalCorrespondentValue']->total = number_format($this->data['totalCorrespondentValue']->total, 2, ',', '.');
        $this->data['totalCorrespondentValue']->correspondentcoststypeservice = number_format($this->data['totalCorrespondentValue']->correspondentcoststypeservice, 2, ',', '.');
        $this->data['totalCorrespondentValue']->correspondentfee = number_format($this->data['totalCorrespondentValue']->correspondentfee, 2, ',', '.');


    }

    private function calc_client_value($id)
    {
        $value = 0;
        $processcoststypeservice = $this->get_coststypeservice($id);
        foreach ($processcoststypeservice['clientcoststypeservice'] as $row) {
            $value += $row->value;
        }

        $processfee = $this->get_processfee($id);
        foreach ($processfee['clientfee'] as $row) {
            $value += $row->value;
        }

        return $value;
    }

    private function calc_correspondent_value($id)
    {
        $value = 0;
        $processcoststypeservice = $this->get_coststypeservice($id);
        foreach ($processcoststypeservice['correspondentcoststypeservice'] as $row) {
            $value += $row->value;
        }

        $processfee = $this->get_processfee($id);
        foreach ($processfee['correspondentfee'] as $row) {
            $value += $row->value;
        }

        return $value;
    }

    private function get_coststypeservice($id)
    {
        $this->load->model('processcoststypeservice_model');
        $data = $this->processcoststypeservice_model->get(array('process_id' => $id))->result();
        $clientcoststypeservice = array();
        $correspondentcoststypeservice = array();
        foreach ($data as $key => $row) {
            if ($row->type == 'client') {
                $clientcoststypeservice[$row->coststypeservice_id] = $row;
            } else {
                $correspondentcoststypeservice[$row->coststypeservice_id] = $row;
            }
        }
        $data = array();
        $data['clientcoststypeservice'] = $clientcoststypeservice;
        $data['correspondentcoststypeservice'] = $correspondentcoststypeservice;
        return $data;

    }

    private function get_processfee($id)
    {
        $this->load->model('processfee_model');
        $data = $this->processfee_model->get(array('process_id' => $id))->result();
        $clientfee = array();
        $correspondentfee = array();
        foreach ($data as $key => $row) {
            if ($row->type == 1/*'client'*/) {
                $clientfee[$row->honorarytypeservice_id] = $row;
            } else {
                $correspondentfee[$row->honorarytypeservice_id] = $row;
            }
        }
        $data = array();
        $data['clientfee'] = $clientfee;
        $data['correspondentfee'] = $correspondentfee;
        return $data;
    }

    private function load_charge_filter()
    {
        $filter = array();
        $this->data['filter'] = new stdClass();

        if ($this->input->get('chargecategory_id')) {
            $filter['chargecategory_id'] = $this->data['filter']->chargecategory_id = $this->input->get('chargecategory_id');
        }

        if ($this->input->get('chargetype_id')) {
            $filter['chargetype_id'] = $this->data['filter']->chargetype_id = $this->input->get('chargetype_id');
        }
        if ($this->input->get('deadlinestart')) {
            $this->data['filter']->deadlinestart = $this->input->get('deadlinestart');;
            $filter['deadline >='] = convertDate($this->data['filter']->deadlinestart);
        }

        if ($this->input->get('deadlineend')) {
            $this->data['filter']->deadlineend = $this->input->get('deadlineend');
            $filter['deadline <='] = convertDate($this->data['filter']->deadlineend);
        }
        return $filter;
    }

    private function load_process_filter()
    {
        $filter = array();
        $this->data['filter'] = new stdClass();

        if ($this->input->get('correspondent_id')) {
            $filter['correspondent_id'] = $this->data['filter']->correspondent_id = $this->input->get('correspondent_id');
        }
        if ($this->input->get('deadlinestart')) {
            $this->data['filter']->deadlinestart = $this->input->get('deadlinestart');;
            $filter['deadline >='] = convertDate($this->data['filter']->deadlinestart);
        }

        if ($this->input->get('deadlineend')) {
            $this->data['filter']->deadlineend = $this->input->get('deadlineend');
            $filter['deadline <='] = convertDate($this->data['filter']->deadlineend);
        }
        return $filter;
    }

    private function load_correspondent()
    {
        $this->load->model('correspondent_model');
        $data = $this->correspondent_model->get()->result();
        foreach ($data as $row) {
            $this->data['correspondent_id'][$row->id] = $row;
        }
    }

    private function load_chargetype()
    {
        $this->load->model('chargetype_model');
        $data = $this->chargetype_model->get()->result();
        foreach ($data as $row) {
            $this->data['chargetype_id'][$row->id] = $row;
        }
    }

    private function load_chargecategory()
    {
        $this->load->model('chargecategory_model');
        $data = $this->chargecategory_model->get()->result();
        foreach ($data as $row) {
            $this->data['chargecategory_id'][$row->id] = $row;
        }
    }

    private function load_chargestatus()
    {
        $this->load->model('chargestatus_model');
        $data = $this->chargestatus_model->get()->result();
        foreach ($data as $row) {
            $this->data['chargestatus_id'][$row->id] = $row;
        }
    }

    private function parse_post()
    {
        $data = (object)$_POST;
        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($id = NULL)
    {
        $this->load_mask();

        $this->load_chargecategory();
        $this->load_chargetype();
        $this->load_chargestatus();

        $session_form = $this->session->userdata($this->form_key);
        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }
        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {
            if ($id) {
                $data = $this->charge_model->get(array('id' => $id))->result();
                $this->data['data'] = current($data);
                $this->data['data']->deadline = convertDate($this->data['data']->deadline);
            }
        }

        if(isset($this->data['data']->value)) {
            $this->data['data']->value = number_format($this->data['data']->value, 2, ',', '.');
        }
        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de status de despesas - ' . $this->data['data']->name : 'Cadastro de status de despesas'));
        $this->renderer();
    }

    private function parse_money($data)
    {
        $data = preg_replace('/\./', '', $data);
        $data = preg_replace('/\,/', '.', $data);
        $data = number_format($data, 2, '.', '');
        return $data;
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {
            $this->session->set_userdata($this->form_key, $this->parse_post());
            $this->load->library('form_validation');
            $this->form_validation->set_rules('value', 'Valor', 'trim');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                $redirect = ($id > 0) ? '/editar/' . $id : '/nova';
                redirect(base_url($this->uri->segment(1) . $redirect));
            } else {
                $data = array(
                    'value' => $this->parse_money($this->input->post('value')),
                    'deadline' => convertDate($this->input->post('deadline')),
                    'text' => $this->input->post('text'),
                    'chargecategory_id' => $this->input->post('chargecategory_id'),
                    'chargetype_id' => $this->input->post('chargetype_id'),
                );
                $this->load_chargestatus();
                $data['chargestatus_id'] = current($this->data['chargestatus_id']);
                $data['chargestatus_id'] = $data['chargestatus_id']->id;
                if ($id > 0) {
                    $update = $this->charge_model->update(array(
                        'id' => $id
                    ), $data);

                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }
                } else {
                    $id = $this->charge_model->insert($data, TRUE);

                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }
        redirect(base_url($this->uri->segment(1)));
    }

    public function delete($id = NULL)
    {
        if ($id && $this->data['me']->user_type == 2) {
            if ($this->charge_model->delete(array('id' => $id))) {
                $this->setMsg($this->lang->line('successfully_data_delete'));
            } else {
                $this->setError($this->lang->line('error_processing_delete'));
            }
        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }
        redirect(base_url($this->uri->segment(1)));
    }
    public function excel()
    {
        //load our new PHPExcel library
        $this->load->library('excel');

        $this->excel->setActiveSheetIndex(0);
        $sheet = $this->excel->getActiveSheet();

        $sheet->setTitle('Despesas internas');

        $l = 1;

        $c = 'A';
        $sheet->setCellValue($c . $l, 'Categoria de despesa interna');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Tipo de despesa');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Valor');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Vencimento');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Status');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);

        foreach ($this->data['charge'] as $row) {
            $c = 'A';
            $l++;
            $sheet->setCellValue($c . $l, $this->data['chargecategory_id'][$row->chargecategory_id]->name);
            $c++;
            $sheet->setCellValue($c . $l, $this->data['chargetype_id'][$row->chargetype_id]->name);
            $c++;
            $sheet->setCellValue($c . $l, $row->value);
            $c++;
            $sheet->setCellValue($c . $l, $row->deadline);
            $c++;
            $sheet->setCellValue($c . $l, $this->data['chargestatus_id'][$row->chargestatus_id]->name);
            $c++;
        }
		//----------------------------------------------------------------
		$this->excel->createSheet();
        $this->excel->setActiveSheetIndex(1);
        $sheet = $this->excel->getActiveSheet();

        $sheet->setTitle('Correspondentes');

        $l = 1;

        $c = 'A';
        $sheet->setCellValue($c . $l, 'Correspondentes');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Processo');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Valor');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Vencimento');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Status');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);

        foreach ($this->data['process'] as $row) {
            $c = 'A';
            $l++;
            $sheet->setCellValue($c . $l, $row->correspondent_name);
            $c++;
            $sheet->setCellValue($c . $l, $row->number);
            $c++;
            $sheet->setCellValue($c . $l, $row->value);
            $c++;
            $sheet->setCellValue($c . $l, $row->deadline ? $row->deadline : '');
            $c++;
            $sheet->setCellValue($c . $l, $this->data['chargestatus_id'][$row->chargestatus_id]->name);
            $c++;
        }
		
        $this->excel->setActiveSheetIndex(0);


        //change the font size
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
        //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename = NAME_SITE . 'Acompanhamento despesas' . rand() . '.xlsx'; //save our workbook as this file name


        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
    public function excelReport()
    {
        //load our new PHPExcel library
        $this->load->library('excel');

        $this->excel->setActiveSheetIndex(0);
        $sheet = $this->excel->getActiveSheet();

        $sheet->setTitle('Balanço financeiro');

        $l = 1;

        $c = 'A';
        $sheet->setCellValue($c . $l, 'Categoria de despesa interna');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        $sheet->setCellValue($c . $l, 'Tipo de despesa');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        if (count($this->data['charge_monthly']) > 0) foreach (current($this->data['charge_monthly'])->month as $row) {
            $sheet->setCellValue($c . $l, lang('cal_' . strtolower(date('M', strtotime($row)))));
            $sheet->getStyle($c . $l)->getFont()->setBold(true);
            $c++;
        }
        $sheet->setCellValue($c . $l, 'Total');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);


        foreach ($this->data['charge_monthly'] as $row) {
            $c = 'A';
            $l++;
            $sheet->setCellValue($c . $l, $this->data['chargecategory_id'][$row->chargecategory_id]->name);
            $c++;
            $sheet->setCellValue($c . $l, $this->data['chargetype_id'][$row->chargetype_id]->name);
            $c++;
            foreach ($row->item as $item) {
                $sheet->setCellValue($c . $l, $item);
                $c++;
            }
        }
        $c = 'A';
        $l++;
        $sheet->setCellValue($c . $l, 'PAGAMENTOS DE CORRESPONDENTES');
        $c++;
        $sheet->setCellValue($c . $l, 'Pagamentos consolidades de correspondentes');
        $c++;
        foreach ($this->data['month_value'] as $key => $item) {
            $sheet->setCellValue($c . $l, isset($this->data['correspondentValue'][$key]) ? $this->data['correspondentValue'][$key] : '0,00');
            $c++;
        }

        $l++;
        $l++;
        $c = 'A';
        $l++;
        $sheet->setCellValue($c . $l, null);
        $c++;
        $sheet->setCellValue($c . $l, 'TOTAL DESPESAS');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        foreach ($this->data['totalMonth_value'] as $key => $item) {
            $sheet->setCellValue($c . $l, isset($item) ? $item : '0,00');
            $c++;
        }

        $c = 'A';
        $l++;
        $sheet->setCellValue($c . $l, null);
        $c++;
        $sheet->setCellValue($c . $l, 'TOTAL RECEITA');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        foreach ($this->data['month_value'] as $key => $item) {
            $sheet->setCellValue($c . $l, isset($this->data['clientValue'][$key]) ? $this->data['clientValue'][$key] : '0,00');
            $c++;
        }

        $c = 'A';
        $l++;
        $sheet->setCellValue($c . $l, null);
        $c++;
        $sheet->setCellValue($c . $l, 'SALDO TOTAL');
        $sheet->getStyle($c . $l)->getFont()->setBold(true);
        $c++;
        foreach ($this->data['month_value'] as $key => $item) {
            $sheet->setCellValue($c . $l, isset($this->data['balance'][$key]) ? $this->data['balance'][$key] : '0,00');
            $c++;
        }

        $this->excel->setActiveSheetIndex(0);


        //change the font size
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:D1');
        //set aligment to center for that merged cell (A1 to D1)
        //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename = NAME_SITE . 'Balanço Financeiro' . rand() . '.xlsx'; //save our workbook as this file name


        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
}