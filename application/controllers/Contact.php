<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index()
    {
        $this->load_datatable();
        $this->data['list'] = $this->contact_model->get($this->load_filter())->result();
        $this->setPageTitle('Contatos');

        $this->get_city();

        $this->renderer();
    }

    private function load_filter()
    {
        $filter = array();
        $this->data['filter'] = new stdClass();

        if ($this->input->get('city_id')) {
            $filter['city_id'] = $this->data['filter']->city_id = $this->input->get('city_id');
        }
        return $filter;
    }


    private function get_city()
    {

        $this->load->model('city_model');
        $city = $this->city_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        foreach ($city as $row) {
            $this->data['city'][$row->id] = $row;
        }
    }

    private function parse_post()
    {
        $data = (object)$_POST;
        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($id = NULL)
    {
        $session_form = $this->session->userdata($this->form_key);
        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }
        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {
            if ($id) {
                $data = $this->contact_model->get(array('id' => $id))->result();
                $this->data['data'] = current($data);
            }
        }
        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de contato - ' . $this->data['data']->name : 'Cadastro de contatos'));

        $this->content = 'tpl_default/contact/index';

        $this->index();
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {
            $this->session->set_userdata($this->form_key, $this->parse_post());
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Nome', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                $redirect = ($id > 0) ? '/editar/' . $id : '/';
                redirect(base_url($this->uri->segment(1) . $redirect));
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'city_id' => $this->input->post('city_id'),
                    'seguiment' => $this->input->post('seguiment'),
                    'address' => $this->input->post('address'),
                    'contact' => $this->input->post('contact'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                );
                if ($id > 0) {
                    $update = $this->contact_model->update(array(
                        'id' => $id
                    ), $data);

                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }
                } else {
                    $id = $this->contact_model->insert($data, TRUE);

                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }
        redirect(base_url($this->uri->segment(1)));
    }

    public function delete($id = NULL)
    {
        if ($id && $this->data['me']->user_type == 2) {
            if ($this->contact_model->delete(array('id' => $id))) {
                $this->setMsg($this->lang->line('successfully_data_delete'));
            } else {
                $this->setError($this->lang->line('error_processing_delete'));
            }
        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }
        redirect(base_url($this->uri->segment(1)));
    }
}