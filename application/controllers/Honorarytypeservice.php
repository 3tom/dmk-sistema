<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Honorarytypeservice extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('honorarytypeservice_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index()
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $this->load_datatable();

        $this->data['list'] = $this->honorarytypeservice_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->setPageTitle('Lista de Honorários por Tipo de Serviço');

        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($id = NULL)
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {

            if ($id > 0) {
                $data = $this->honorarytypeservice_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();

                $this->data['data'] = (count($data) > 0) ? current($data) : NULL;
            }
        }

        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de Honorários por Tipo de Serviço - ' . $this->data['data']->name : 'Cadastro de Honorários por Tipo de Serviço'));

        $this->renderer();
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Nome', 'trim|required');


            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());

                $redirect = ($id > 0) ? '/editar/' . $id : '/novo';

                redirect(base_url($this->uri->segment(1) . $redirect));

            } else {
                $data = array(
                    'name' => $this->input->post('name')
                );

                if ($id > 0) {
                    $update = $this->honorarytypeservice_model->update(array(
                        'account_id' => $this->data['me']->account_id,
                        'id' => $id
                    ), $data);

                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }

                } else {
                    $data['account_id'] = $this->data['me']->account_id;

                    $id = $this->honorarytypeservice_model->insert($data, TRUE);

                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }

    public function delete($id = NULL)
    {
        if ($id && $this->data['me']->user_type == 2) {

            $this->load->model(array('clientfee_model', 'clientfeenote_model', 'correspondentfee_model', 'correspondentfeenote_model'));

            $cannot_deleted = $this->clientfee_model->get(array(
                'honorarytypeservice_id' => $id,
                'value !=' => NULL
            ))->result();

            $cannot_deleted = array_merge($cannot_deleted, $this->clientfeenote_model->get(array(
                'honorarytypeservice_id' => $id,
                'note !=' => NULL
            ))->result());

            $cannot_deleted = array_merge($cannot_deleted, $this->correspondentfee_model->get(array(
                'honorarytypeservice_id' => $id,
                'value !=' => NULL
            ))->result());

            $cannot_deleted = array_merge($cannot_deleted, $this->correspondentfeenote_model->get(array(
                'honorarytypeservice_id' => $id,
                'note !=' => NULL
            ))->result());

            if (count($cannot_deleted) == 0) {

                if ($this->honorarytypeservice_model->delete(array(
                    'id' => $id
                ))
                ) {
                    $this->setMsg($this->lang->line('successfully_data_delete'));
                } else {
                    $this->setError($this->lang->line('error_processing_delete'));
                }
            } else {
                $this->setError($this->lang->line('cannot_deleted'));
            }

        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }


        redirect(base_url($this->uri->segment(1)));
    }
}