<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chargecategory extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('chargecategory_model');
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index()
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $this->load_datatable();
        $this->data['list'] = $this->chargecategory_model->get()->result();
        $this->setPageTitle('Categoria de despesas');
        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;
        return count((array)$data) !== 0 ? $data : null;
    }

    public function form($id = NULL)
    {
        if($this->data['me']->user_type == 1){
            redirect('./');
        }
        $session_form = $this->session->userdata($this->form_key);
        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }
        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;
        } else {
            if ($id) {
                $data = $this->chargecategory_model->get(array('id' => $id))->result();
                $this->data['data'] = current($data);
            }
        }
        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de categoria de despesas - ' . $this->data['data']->name : 'Cadastro de categoria de despesas'));
        $this->renderer();
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {
            $this->session->set_userdata($this->form_key, $this->parse_post());
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Nome', 'trim|required');

            if ($this->form_validation->run() === FALSE) {
                $this->setError(validation_errors());
                $redirect = ($id > 0) ? '/editar/' . $id : '/nova';
                redirect(base_url($this->uri->segment(1) . $redirect));
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                );
                if ($id > 0) {
                    $update = $this->chargecategory_model->update(array(
                        'id' => $id
                    ), $data);

                    if (!$update) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->session->unset_userdata($this->form_key);
                        $this->setMsg($this->lang->line('successfully_data'));
                    }
                } else {
                    $id = $this->chargecategory_model->insert($data, TRUE);

                    if (!$id) {
                        $this->setError($this->lang->line('error_processing_form'));
                    } else {
                        $this->setMsg($this->lang->line('successfully_data_insert'));
                    }
                }
            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }
        redirect(base_url($this->uri->segment(1)));
    }

    public function delete($id = NULL)
    {
        if ($id && $this->data['me']->user_type == 2) {
            if ($this->chargecategory_model->delete(array('id' => $id))) {
                $this->setMsg($this->lang->line('successfully_data_delete'));
            } else {
                $this->setError($this->lang->line('error_processing_delete'));
            }
        } else {
            $this->setError($this->lang->line('error_processing_delete'));
        }
        redirect(base_url($this->uri->segment(1)));
    }
}