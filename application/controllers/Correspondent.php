<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Correspondent extends MY_Controller
{
    private $form_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('correspondent_model'));
        $this->lang->load('site');

        $this->form_key = $this->router->class;
    }

    public function index($id = NULL)
    {
        $this->load_datatable();
        $this->setPageTitle("Correspondentes");
        $this->data['list'] = $this->correspondent_model->get()->result();
        $this->renderer();
    }

    private function parse_post()
    {
        $data = (object)$_POST;

        $data->correspondentemail = array();
        if (isset($_POST['correspondentemail'])) {
            foreach ($_POST['correspondentemail'] as $key => $row) {
                $data->correspondentemail[$key] = (object)$row;
            }
        }

        if (isset($_POST['correspondentbank'])) {
            $data->correspondentbank = array();
            foreach ($_POST['correspondentbank'] as $key => $row) {
                $data->correspondentbank[$key] = (object)$row;
            }
        }

        $data->correspondentlawareas = array();
        if (isset($_POST['lawareas_id'])) {
            foreach ($_POST['lawareas_id'] as $key => $row) {
                $data->correspondentlawareas[$key] = $row;
            }
        }

        $data->correspondentcoststypeservice = array();
        if (isset($_POST['coststypeservice_id'])) {
            foreach ($_POST['coststypeservice_id'] as $key => $row) {
                $data->correspondentcoststypeservice[$key] = $row;
            }
        }

        if (isset($_POST['correspondentfee'])) {
            $data->correspondentfee = array();
            foreach ($_POST['correspondentfee'] as $key => $row) {
                $data->correspondentfee[$key] = $row;
            }
        }

        return count((array)$data) !== 0 ? $data : null;
    }
    public function form($id = null)
    {
        $this->load_mask();
        $this->load_chosen();


        $this->loadJs(array(
            'name' => 'automatic_value'
        ));
        $this->loadCss(array(
            'name' => 'automatic_value'
        ));

        $session_form = $this->session->userdata($this->form_key);

        if ($session_form && $this->data['error'] === NULL) {
            $this->session->unset_userdata($this->form_key);
            $session_form = null;
        }

        $this->data['data'] = (object)array();

        $correspondentemail = array();

        $correspondentbank = array();

        $correspondentlawareas = array();

        $correspondentcoststypeservice = array();

        $correspondentfee = array();

        $cities_selected = array();

        if ($session_form) {
            $this->data['data'] = $session_form;
            $this->data['data']->id = $id;

            $correspondentemail = $session_form->correspondentemail;

            $correspondentbank = $session_form->correspondentbank;

            $correspondentlawareas = $session_form->correspondentlawareas;

            $correspondentcoststypeservice = $session_form->correspondentcoststypeservice;

            $correspondentfee = $session_form->correspondentfee;

            if ($correspondentfee && count($correspondentfee) > 0) {
                foreach ($correspondentfee as $row) {
                    foreach ($row['city'] as $city_id => $value) {
                        $cities_selected[$city_id] = $city_id;
                    }
                }
            }

        } else {

            if ($id > 0) {
                $data = $this->correspondent_model->get(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ))->result();

                $this->data['data'] = (count($data) > 0) ? current($data) : (object)array();

                $this->load->model('correspondentemail_model');

                $correspondentemail = $this->correspondentemail_model->get(array(
                    'correspondent_id' => $id
                ))->result();

                $this->load->model('correspondentbank_model');

                $correspondentbank = $this->correspondentbank_model->get(array(
                    'correspondent_id' => $id
                ))->result();

                $this->load->model('correspondentlawareas_model');

                $correspondentlawareas_get = $this->correspondentlawareas_model->get(array(
                    'correspondent_id' => $id
                ))->result();

                if (count($correspondentlawareas_get)) {
                    foreach ($correspondentlawareas_get as $row) {
                        $correspondentlawareas[] = $row->lawareas_id;
                    }
                }

                $this->load->model('correspondentcoststypeservice_model');

                $correspondentcoststypeservice_get = $this->correspondentcoststypeservice_model->get(array(
                    'correspondent_id' => $id
                ))->result();

                if (count($correspondentcoststypeservice_get)) {
                    foreach ($correspondentcoststypeservice_get as $row) {
                        $correspondentcoststypeservice[] = $row->coststypeservice_id;
                    }
                }

                $this->load->model('correspondentfee_model');

                $correspondentfee_get = $this->correspondentfee_model->get(array(
                    'correspondent_id' => $id
                ))->result();

                foreach ($correspondentfee_get as $row) {
                    $correspondentfee[$row->honorarytypeservice_id]['city'][$row->city_id]['value'] = $row->value;
                    $cities_selected[$row->city_id] = $row->city_id;
                }

                $this->load->model('correspondentfeenote_model');

                $correspondentfeenote_get = $this->correspondentfeenote_model->get(array(
                    'correspondent_id' => $id
                ))->result();

                foreach ($correspondentfeenote_get as $row) {
                    $correspondentfee[$row->honorarytypeservice_id]['note'] = $row->note;
                }

            }
        }

        $this->data['data']->correspondentemail = (object)$correspondentemail;

        $this->data['data']->correspondentbank = (object)$correspondentbank;

        $this->data['data']->correspondentlawareas = $correspondentlawareas;

        $this->data['data']->correspondentcoststypeservice = $correspondentcoststypeservice;

        $this->data['data']->correspondentfee = $correspondentfee;

        $this->data['data']->cities_selected = $cities_selected;

        $this->load->model('lawareas_model');
        $this->data['data']->lawareas = $this->lawareas_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->load->model('coststypeservice_model');
        $this->data['data']->coststypeservice = $this->coststypeservice_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();

        $this->load->model('honorarytypeservice_model');
        $this->data['data']->honorarytypeservice = $this->honorarytypeservice_model->get(array(
            'account_id' => $this->data['me']->account_id
        ))->result();




        $this->load->model('citygroup_model');
        $this->data['data']->citygroup = $this->citygroup_model->get(array('account_id' => $this->data['me']->account_id))->result();

        $this->load->model('city_model');
        $this->data['data']->cities = $this->city_model->get(array('account_id' => $this->data['me']->account_id))->result();

        $this->setPageTitle((isset($this->data['data']->name) ? 'Edição de correspondente - ' . $this->data['data']->name : 'Cadastro de correspondente'));

        $this->renderer();
    }

    public function save($id = NULL)
    {
        if ($this->input->post() && $this->data['me']->user_type == 2) {

            $this->session->set_userdata($this->form_key, $this->parse_post());

            $correspondentemail = $this->input->post('correspondentemail');
            $correspondentbank = $this->input->post('correspondentbank');

            $data = array(
                'name' => $this->input->post('name'),
                'document' => $this->input->post('document'),
                'phone1' => $this->input->post('phone1'),
                'phone2' => $this->input->post('phone2'),
                'phone3' => $this->input->post('phone3'),
                'phone4' => $this->input->post('phone4'),
                'street' => $this->input->post('street'),
                'number' => $this->input->post('number'),
                'complement' => $this->input->post('complement'),
                'cep' => $this->input->post('cep'),
                'city' => $this->input->post('city'),
                'district' => $this->input->post('district'),
                'state' => $this->input->post('state'),
                'note' => $this->input->post('note')
            );

            if ($id > 0) {
                $update = $this->correspondent_model->update(array(
                    'account_id' => $this->data['me']->account_id,
                    'id' => $id
                ), $data);

                if (!$update) {
                    $this->setError($this->lang->line('error_processing_form'));
                } else {
                    $this->session->unset_userdata($this->form_key);
                    $this->setMsg($this->lang->line('successfully_data'));
                }

            } else {
                $data['account_id'] = $this->data['me']->account_id;

                $id = $this->correspondent_model->insert($data, TRUE);

                if (!$id) {
                    $this->setError($this->lang->line('error_processing_form'));
                } else {
                    $this->session->unset_userdata($this->form_key);
                    $this->setMsg($this->lang->line('successfully_data_insert'));
                }
            }

            if ($id > 0 && $this->data['error'] === NULL) {

                $this->load->model('correspondentemail_model');

                $this->correspondentemail_model->delete(array(
                    'correspondent_id' => $id
                ));

                if (isset($correspondentemail) && count($correspondentemail) > 0) {
                    $correspondentemail_insert = array();
                    foreach ($correspondentemail as $row) {
                        $correspondentemail_insert[] = array(
                            'correspondent_id' => $id,
                            'email' => $row['email']
                        );
                    }

                    $this->correspondentemail_model->insert_batch($correspondentemail_insert);
                }

                $this->load->model('correspondentbank_model');

                $this->correspondentbank_model->delete(array(
                    'correspondent_id' => $id
                ));

                if (isset($correspondentbank) && count($correspondentbank) > 0) {
                    $correspondentbank_insert = array();
                    foreach ($correspondentbank as $row) {
                        $row['correspondent_id'] = $id;
                        $correspondentbank_insert[] = $row;
                    }
                    $this->correspondentbank_model->insert_batch($correspondentbank_insert);
                }


                $this->load->model('correspondentlawareas_model');

                $this->correspondentlawareas_model->delete(array(
                    'correspondent_id' => $id
                ));

                $lawareas = $this->input->post('lawareas_id');

                if ($lawareas && count($lawareas) > 0) {
                    $correspondentlawareas_inset = array();
                    foreach ($lawareas as $row) {
                        $correspondentlawareas_inset[] = array(
                            'correspondent_id' => $id,
                            'lawareas_id' => $row
                        );
                    }
                    $this->correspondentlawareas_model->insert_batch($correspondentlawareas_inset);
                }


                $this->load->model('correspondentcoststypeservice_model');

                $this->correspondentcoststypeservice_model->delete(array(
                    'correspondent_id' => $id
                ));

                $coststypeservice = $this->input->post('coststypeservice_id');

                if ($coststypeservice && count($coststypeservice) > 0) {
                    $correspondentcoststypeservice = array();
                    foreach ($coststypeservice as $row) {

                        $correspondentcoststypeservice[] = array(
                            'correspondent_id' => $id,
                            'coststypeservice_id' => $row
                        );
                    }

                    $this->correspondentcoststypeservice_model->insert_batch($correspondentcoststypeservice);
                }

                $correspondentfee_post = $this->input->post('correspondentfee');

                $correspondentfee = array();
                $correspondentfeenote = array();

                $this->load->model(array('correspondentfee_model', 'correspondentfeenote_model'));

                $this->correspondentfee_model->delete(array(
                    'correspondent_id' => $id
                ));

                $this->correspondentfeenote_model->delete(array(
                    'correspondent_id' => $id
                ));

                if ($correspondentfee_post && count($correspondentfee_post) > 0) {

                    foreach ($correspondentfee_post as $honorarytypeservice_id => $row) {
                        foreach ($row['city'] as $city_id => $value) {
                            $correspondentfee[] = array(
                                'correspondent_id' => $id,
                                'city_id' => $city_id,
                                'honorarytypeservice_id' => $honorarytypeservice_id,
                                'value' => $value['value'] ? $value['value'] : NULL
                            );
                        }

                        $correspondentfeenote[] = array(
                            'correspondent_id' => $id,
                            'honorarytypeservice_id' => $honorarytypeservice_id,
                            'note' => $row['note'] ? $row['note'] : NULL
                        );
                    }

                    if (count($correspondentfee) > 0) $this->correspondentfee_model->insert_batch($correspondentfee);
                    if (count($correspondentfeenote) > 0) $this->correspondentfeenote_model->insert_batch($correspondentfeenote);
                }

            }

        } else {
            $this->setError($this->lang->line('error_processing_form'));
        }

        redirect(base_url($this->uri->segment(1)));
    }
}