<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        if (isset($where['q'])) {

            $this->db->like('name', $where['q']);
            $this->db->or_like('code', $where['q']);
            unset($where['q']);
        }
        return parent::get($where, $limit);
    }
}