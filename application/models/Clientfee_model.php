<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clientfee_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        $this->db->select($this->getAlias() . '.*, city.name');
        $this->db->join('city', $this->getAlias() . '.city_id = city.id');
        $this->db->order_by('city.name', 'ASC');
        $this->db->order_by('city_id', 'ASC');
        $this->db->order_by('honorarytypeservice_id', 'ASC');
        return parent::get($where, $limit);
    }


}