<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Processfee_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function totalize(array $where = array(), $limit = null)
    {
        $this->db->select('SUM(' . $this->getAlias() . '.value) as total');
        $this->db->select($this->getAlias() . '.type');
        $this->db->select($this->getAlias() . '.process_id');
        $this->db->group_by($this->getAlias() . '.type');
        $this->db->group_by($this->getAlias() . '.process_id');
        return parent::get($where, $limit);
    }

}
