<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Chat_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        $this->db->order_by('datetime', 'DESC');
        return parent::get($where, $limit);
    }

    public function get_related(array $where = array(), $limit = null)
    {
        $this->db->join('user', $this->getAlias() . '.user_id = user.id');
        $this->db->select($this->getAlias() . '.*, user.name AS user_name');
        return $this->get($where, $limit);
    }
}
