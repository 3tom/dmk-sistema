<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class City_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        $this->db->join('country', $this->getAlias() . '.country_id = country.id');
        $this->db->select($this->getAlias() . '.*, country.abbreviation AS country_abbreviation');
        $this->db->order_by($this->getAlias() . '.name', 'ASC');

        return parent::get($where, $limit);
    }
}