<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Processfile_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        $this->db->order_by('created', 'DESC');
        return parent::get($where, $limit);
    }
}
