<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Honorarytypeservice_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get(array $where = array(), $limit = null)
    {
        if(isset($where['process_id'])) {
            $this->db->join('processfee', $this->getAlias() . '.id = processfee.honorarytypeservice_id AND processfee.value != "0.00" AND processfee.process_id = ' . $where['process_id']);
            unset($where['process_id']);
        }
        $this->db->select($this->getAlias() . '.*');
        return parent::get($where, $limit);
    }
}