$(function () {
    $("#client_id").chosen({
        placeholder_text: "Selecione o cliente",
        no_results_text: "Cliente não encontrado!"
    });
    $("#judicialpanel_id").chosen({
        placeholder_text: "Selecione a vara",
        no_results_text: "Vara não encontrada!"
    });
    $('#shippingprocess_id').chosen({
        placeholder_text: "Selecione o Advogado",
        no_results_text: "Advogado não encontrado!"
    });
    $('#city_id').chosen({
        placeholder_text: "Selecione a cidade",
        no_results_text: "Cidade não encontrado!"
    });
    $('#correspondent_id').chosen({
        placeholder_text: "Selecione o Correspondente",
        no_results_text: "Correspondente não encontrado!"
    });
    $('#client_lawarea_id').chosen({
        placeholder_text: "Selecione a area de direito",
        no_results_text: "Area de direito não encontrado!"
    });
    $('#correspondent_lawarea_id').chosen({
        placeholder_text: "Selecione a area de direito",
        no_results_text: "Area de direito não encontrado!"
    });
    $('#user_id').chosen({
        placeholder_text: "Selecione um responsável para o cadastro",
        no_results_text: "Responsável não encontrado!"
    });


    $('#number').change(function () {
        var data = 'number=' + this.value;

        var xhr = new XMLHttpRequest();
        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                var data = this.responseText;
                data = jQuery.parseJSON(data);

                if(data.process_get.length > 0) {
                    $('#duplicate-alert').modal();
                }
            }
        });

        xhr.open("POST", "./ws/process/get");
        xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader("cache-control", "no-cache");
        xhr.send(data);
    });
});
function load_correspondent_lawareas() {
    var correspondent_id = $('#correspondent_id').val();
    $('.now_loading').css("display", "block");
    $.ajax({
        url: 'ws/Correspondentlawareas/get',
        data: {id: correspondent_id},
        method: 'POST',
        success: function (data) {
            if (data.error) {
                alert_message(data.error, 'alert-danger');
            } else {
                var result = data.Correspondentlawareasresult;
                if(result.length > 0){
                    var result_html = '<option></option>';
                    for(i in result){
                        result_html += '<option value="'+result[i].lawareas_id+'">'+result[i].name+'</option>'
                    }
                }else{
                    result_html = '<option disabled>Nenhuma area de direto disponivel.</option>';
                }
                $('#correspondent_lawarea_id').html(result_html);
                $('#correspondent_lawarea_id').prop('disabled', false);
                $('#correspondent_lawarea_id').trigger("chosen:updated");
            }
            $('.now_loading').css("display", "none");
        }
    });
}

function add_shipping() {
    $('.now_loading').css("display", "block");
    var client_id = $('#client_id').val();
    $.ajax({
        url: 'ws/Shippingprocess/get',
        data: {id: client_id},
        method: 'POST',
        success: function (data) {
            if (data.error) {
                alert_message(data.error, 'alert-danger');
            }else{
                var result = data.Shippingprocessresult;
                if(result.length > 0){
                    var result_html = '<option></option>';
                    for(i in result){
                        result_html += '<option value="'+result[i].id+'">'+result[i].name+'</option>'
                    }
                }else{
                    result_html = '<option disabled>Nenhum advogado disponivel.</option>';
                }
                $('#shippingprocess_id').html(result_html);
                $('#shippingprocess_id').prop('disabled', false);
                $('#shippingprocess_id').trigger("chosen:updated");
            }

        }
    });
    $.ajax({
        url: 'ws/Clientlawareas/get',
        data: {id: client_id},
        method: 'POST',
        success: function (data) {
            if (data.error) {
                alert_message(data.error, 'alert-danger');
            }else{
                var result = data.Clientlawareasresult;
                if(result.length > 0){
                    result_html = '<option> </option>';
                    for(i in result){
                        result_html += '<option value="'+result[i].lawareas_id+'">'+result[i].name+'</option>'
                    }
                }else{
                    result_html = '<option disabled>Nenhuma area de direto disponivel.</option>';
                }
                $('#client_lawarea_id').html(result_html);
                $('#client_lawarea_id').prop('disabled', false);
                $('#client_lawarea_id').trigger("chosen:updated");
            }
            $('.now_loading').css("display", "none");
        }
    });
    $.ajax({
        url: 'ws/Clientcoststypeservice/get',
        data: {id: client_id},
        method: 'POST',
        success: function (data) {
            if (data.error) {
                alert_message(data.error, 'alert-danger');
            }else{
                var result = data.Clientcoststypeserviceresult;
                if(result.length > 0){
                    result_html = '';
                    for(i in result){
                        result_html += '<label>' +
                            '<input type="checkbox" name="coststypeservice_id['+result[i].coststypeservice_id+']" value="'+result[i].coststypeservice_id+'">'+
                            '<i class="fa icon-check"></i>' +
                            result[i].name+
                            '</label>' +
                            '' +
                            '<input type="text" class="form-control" name="costypeservice_value['+result[i].coststypeservice_id+']">'
                    }

                }else{
                    result_html = '<label disabled>Nenhum tipo de custo cadastrado.</option>';
                }
                $('#coststypeservice').html(result_html);
            }
            $('.now_loading').css("display", "none");
        }
    });
}
function fee_automatic(checkbox) {
    var $tr = $(checkbox).closest('tr');
    var $checkbox = $(checkbox);
    var $input = $tr.find('.money');

    if(!$checkbox.prop('checked')){
        $input.css("color", "#555555" );
        $input.prop('disabled', false);
    }else{
        $input.css("color", "transparent" );
        $input.prop('disabled', true);
    }
}



function load_clientfee() {
    var client_id = $('#client_id').val(),
        city_id = $('#city_id').val();

    if(client_id != '' && city_id != ''){
        $('.now_loading').css("display", "block");
        $.ajax({
            url: 'ws/clientfee/get',
            data: {id: client_id,
                    city: city_id},
            method: 'POST',
            success: function (data) {
                if (data.error) {
                    alert_message(data.error, 'alert-danger');
                } else {
                    var result = data.clientfeeresult;
                    if(result.length > 0){
                        for(i in result){
                            var input = $('#client_honorarytypeservice-'+result[i].honorarytypeservice_id).find('[data-id = "'+result[i].honorarytypeservice_id+'"]'),
                                automatic = $('#client_honorarytypeservice-'+result[i].honorarytypeservice_id).find('[name = "automatic_clientfee['+result[i].honorarytypeservice_id+']"]');
                            input.val(result[i].value);
                            if(result[i].value != null){
                                automatic.prop('checked', true);
                                input.prop('disabled', true);
                            }else{
                                input.prop('disabled', false);
                                automatic.prop('checked', false);
                            }
                        }
                    }else{
                        var input = $('#clientfee-table').find('input');
                        input.prop('checked', false);
                        input.prop('disabled', false);
                        input.val('');
                    }
                }
                $('.now_loading').css("display", "none");
            }
        });
    }
}

function load_correspondentfee() {
    var correspondent_id = $('#correspondent_id').val(),
        city_id = $('#city_id').val();

    if(correspondent_id != '' && city_id != ''){
        $('.now_loading').css("display", "block");
        $.ajax({
            url: 'ws/correspondentfee/get_fee',
            data: {id: correspondent_id,
                city: city_id},
            method: 'POST',
            success: function (data) {
                if (data.error) {
                    alert_message(data.error, 'alert-danger');
                } else {
                    var result = data.correspondentfeeresult;
                    if(result.length > 0){
                        for(i in result){
                            var input = $('#correspondent_honorarytypeservice-'+result[i].honorarytypeservice_id).find('[data-id = "'+result[i].honorarytypeservice_id+'"]'),
                                automatic = $('#correspondent_honorarytypeservice-'+result[i].honorarytypeservice_id).find('[name = "automatic_corespondentfee['+result[i].honorarytypeservice_id+']"]');
                            input.val(result[i].value);
                            if(result[i].value != null){
                                automatic.prop('checked', true);
                                input.prop('disabled', true);
                            }else{
                                automatic.prop('checked', false);
                                input.prop('disabled', false);
                            }
                        }
                    }else{
                        var input = $('#correspondentfee-table').find('input');
                        input.prop('checked', false);
                        input.prop('disabled', false);
                        input.val('');
                    }
                }
                $('.now_loading').css("display", "none");
            }
        });
    }
}

function add_correspondent() {
    $('.now_loading').css("display", "block");
    var city_id = $('#city_id').val();
    $.ajax({
        url: 'ws/correspondentfee/get',
        data: {id: city_id },
        method: 'POST',
        success: function (data) {
            if(data.error){
                alert_message(data.error, 'alert-danger');
            }else{
                var result = data.correspondentfeeresult;
                if(result.length > 0){
                    var result_html = '<option></option>';
                    for(i in result){
                        result_html += '<option value="'+result[i].id+'">'+result[i].name+'</option>'
                    }
                }else{
                    result_html = '<option disabled>Nenhum Correspondente disponivel.</option>';
                }
                $('#correspondent_id').html(result_html);
                $('#correspondent_id').prop('disabled', false);
                $('#correspondent_id').trigger("chosen:updated");
            }
            $('.now_loading').css("display", "none");
        }
    });
}