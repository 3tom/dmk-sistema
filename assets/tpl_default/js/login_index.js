$(function () {

    $("#forgot-password .forgot-password").click(function () {

        $(".forgot-password").addClass('hide');
        $(".forgot-password-hide").removeClass('hide');

        $("form").attr('action', $(this).attr('href').replace("#", ''));

        return false;
    });

    $("#forgot-password .forgot-password-hide").click(function () {

        $(".forgot-password").removeClass('hide');
        $(".forgot-password-hide").addClass('hide');

        $("form").attr('action', $(this).attr('href').replace("#", ''));

        return false;
    });

});

