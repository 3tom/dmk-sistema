$(function () {
    $("#all-city").chosen({
        placeholder_text: "Selecione a Cidade",
        no_results_text: "Cidade não encontrada!"
    });
});