$(function () {
    $(function () {
        $('#dataTable.charge').dataTable({
            "language": {
                "url": "./assets/datatable/Portuguese-Brasil.json"
            }
        });
        $('#dataTable.correspondent').dataTable({
            "pageLength": -1,
            "language": {
                "url": "./assets/datatable/Portuguese-Brasil.json"
            }
        });
    });
});