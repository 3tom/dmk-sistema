$(function () {

    $("#honorarytypeservice-city").chosen({
        placeholder_text: "Selecione a Cidade",
        no_results_text: "Cidade não encontrada!"
    });

    add_city();
});

function remove_item_table(city_id) {
    var $table = $("#fee-table"),
        $select = $("#honorarytypeservice-city"),
        $option = $select.find("option[value='" + city_id + "']");

    $table.find('thead tr .col-city-' + city_id + ', tbody tr .col-city-' + city_id).remove();

    if ($table.find('tbody tr td[data-class]').length == 0) {
        $table.find('thead tr .col-note, tbody tr .col-note').remove();
    }

    $option.prop('disabled', false);

    $select.val('').change().trigger("chosen:updated");
}

function add_item_table(city_id, city_name) {
    var $table = $("#fee-table");

    var html_th = "<th class=\"col-city-" + city_id + "\" data-class=\".col-city-" + city_id + "\">" +
            city_name + " <button type=\"button\" class=\"remove-item-table\" onclick=\"remove_item_table(" + city_id + ");\"><i class=\"fa fa-close\"></i></button>" +
            "</th>",
        html_th_note = "<th class=\"col-note\">Observações</th>";

    $table.find('thead tr').append(html_th);

    if ($table.find('thead tr .col-note').length == 1) {
        $table.find('thead tr .col-note').remove();
    }

    $table.find('thead tr').append(html_th_note);

    $table.find('tbody tr').each(function (i, el) {
        var $tr = $(el),
            id = $tr.attr('data-id'),
            html_td = "<td class=\"col-city-" + city_id + "\" data-class=\".col-city-" + city_id + "\">" +
                    "<div class=\"input-group\">"+
                    "<span class=\"input-group-addon pointer\">" +
                    "R$" +
                    "<div class=\"action-menu\">" +
                    "<ul>" +
                    "<li>" +
                    "<button type=\"button\" class=\"btn btn-default screen-reader-text\" onclick=\"automatic_value(this, 'all');\">Action</button>" +
                    "<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#automatic-value-confirm-modal\">" +
                    "Replicar este valor para toda a linha." +
                    "</a>" +
                    "</li>" +
                    "<li>" +
                    "<button type=\"button\" class=\"btn btn-default screen-reader-text\" onclick=\"automatic_value(this, 'empty');\">Action</button>" +
                    "<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#automatic-value-confirm-modal\">" +
                    "Replicar o valor para os campos em branco ." +
                    "</a>" +
                    "</li>" +
                    "<li>" +
                    "<button type=\"button\" class=\"btn btn-default screen-reader-text\" onclick=\"automatic_value(this, 'old');\">Action</button>" +
                    "<a href=\"javascript:;\" data-toggle=\"modal\" data-target=\"#automatic-value-confirm-modal\">" +
                    "Replicar o novo conteúdo para o mesmo os campos de mesmo valor." +
                    "</a>" +
                    "</li>" +
                    "</ul>" +
                    "</div>" +
                    "</span>"+
                    "<input type=\"text\" class=\"form-control money\" name=\"clientfee[" + id + "][city][" + city_id + "][value]\" data-id="+ id +" value = \"\" >" +
                    "</div>"+
                    "</td>",
            html_note = "<td class=\"col-note\"><textarea class=\"form-control\" name=\"clientfee[" + id + "][note]\"></textarea></td>",
            data_note = false;

        if ($tr.find('.col-note').length == 1) {
            $tr.data('note', $tr.find('.col-note').html());
            $tr.data('note-value', $tr.find('.col-note textarea').val());
            $tr.find('.col-note').remove();
            data_note = true;
        }

        $tr.append(html_td);
        $tr.append(html_note);

        if (data_note) {
            $tr.find('.col-note').html($tr.data('note'));
            $tr.find('.col-note textarea').val($tr.data('note-value'));
        }

    });

    $table.find("[data-class]").hover(function () {
        $table.find($(this).attr('data-class')).addClass('hover');
    }, function () {
        $table.find($(this).attr('data-class')).removeClass('hover');
    });

}

function group_city() {
    $('.now_loading').css("display", "block");
    var value = $("#groupcity").val(),
        $list = $("#honorarytypeservice-city"),
        form_id = $("#groupcity").data('id'),
        $table = $("#fee-table");
    $.ajax({
        url: 'ws/citygroup/get',
        data: {group_id: value,
            id: form_id},
        method: 'POST',
        success: function (data) {
            if (data.error) {
                alert_message(data.error, 'alert-danger');
            } else {
                var result = data.citygroupresult;
                if(result.length > 0){
                    var result_html = '<option value="all">Todas</option>';
                    for (i in result) {
                        var on_screen = $table.find('[data-class = ".col-city-'+result[i].city_id+'"]'),
                            selected = '',
                            name = result[i].name+'-'+result[i].country_abbreviation;
                        if(on_screen.length > 0){
                            selected = 'disabled';
                        }
                        if(result[i].selected){
                            selected = 'disabled';
                        }
                        result_html += '<option value="'+result[i].city_id+'" '+selected+'>'+name+'</option>'
                    }
                }else{
                    result_html = '<option disabled>Nenhum resultado encontrado.</option>';
                }
                var list_html = result_html;
                $list.html(list_html);
                $list.trigger("chosen:updated");
            }
            $('.now_loading').css("display", "none");
        }
    });
}

function add_city() {
    var $table = $("#fee-table");

    $table.find("[data-class]").hover(function () {
        $table.find($(this).attr('data-class')).addClass('hover');
    }, function () {
        $table.find($(this).attr('data-class')).removeClass('hover');
    });

    $("#add-city").click(function () {

        var $select = $("#honorarytypeservice-city"),
            city_id = $select.val(),
            done = false;

        if (city_id) {
            $('.now_loading').css("display", "block");
            if(city_id == 'all'){
                $("#honorarytypeservice-city option").each(function(){
                    if($(this).val() != 'all' && $(this).prop('disabled') == false){
                        var city_id = $(this).val(),
                            city_name = $(this).text();
                        $(this).prop('disabled', true);;
                        add_item_table(city_id, city_name);
                    }
                    $select.val('').change().trigger("chosen:updated");
                    done = true;
                });
            }else{
                var $option = $select.find("option[value='" + city_id + "']"),
                    city_name = $option.text();
                $option.prop('disabled', true);
                $select.val('').change().trigger("chosen:updated");
                add_item_table(city_id, city_name);
                done = true;
            }
        }
        if(done == true){
            $('.now_loading').css("display", "none");
            done = false;
        }
    });

}