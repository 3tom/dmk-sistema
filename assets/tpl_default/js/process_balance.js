$(function () {
    $('.dataTable').dataTable({
        "pageLength": -1,
        "language": {
            "url": "./assets/datatable/Portuguese-Brasil.json"
        }
    });
});