$(function () {
    open_menu();
    active_menu();
    toggle_panel();
    template_loop();

    $(".external-link-confirm").click(function () {
        var text = 'Tem certeza que quer sair desta página?\n Os dados não salvos do formulário serão perdidos.';
        if (confirm(text)) {
            return true;
        } else {
            return false;
        }
    });


    if(jQuery().mask) {
        $('.money').mask("#.##0,00", {reverse: true});
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00');
    }
});

$(document).ready(function() {
    $(window).load(function() {
        document.getElementById("now_loading").style.display = 'none';
    });
});

function alert_message(data, alert_class) {

    console.log(data);

    if (data) {
        var message = "",
            html = '',
            alert_class = (alert_class == undefined || alert_class == '') ? 'alert-warning' : alert_class;

        if ($.isArray(data) || $.isPlainObject(data)) {
            for (i in data) {
                message += '<p>' + data[i] + '</p>';
            }
        } else {
            message += '<p>' + data + '</p>';
        }

        html = '<div class="alert ' + alert_class + ' alert-dismissible" role="alert">'
            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
            + message
            + '</div>';

        $('.global-inf').prepend(html);

    }

}

function get_template(list_item, template_name) {

    if (undefined == template_name) {
        template_name = 'template';
    }

    var $list_item = $(list_item);
    var template = $list_item.data(template_name);
    if (undefined == template) {
        var $template = $list_item.find('.' + template_name);
        if ($template.length != 0) {
            template = $template[0].outerHTML;
            template = template.replace(template_name, '');
            template = template.replace('___src___', 'src');
            $template.remove();
            $list_item.data(template_name, template);
        } else {
            console.log('Template: "' + template_name + '" was not found in "' + list_item.html() + '');
            console.log('Callee: "' + arguments.callee.caller.toString());
        }
    }

    return template;
}

function fill_template(list_item, data, template_name) {

    var template = get_template(list_item, template_name);

    for (i in data) {
        template = template.split('{{' + i + '}}').join(data[i]);
    }
    return template;
}

function open_menu() {
    $("#toggle-menu").click(function () {
        var $body = $("body");
        $body.toggleClass('toggle-menu-open');
        if ($body.hasClass('toggle-menu-open')) {
            var h_nav = $("nav.navbar").height(),
                h_window = $(window).height();

            $(".toggle-menu").height((h_window - h_nav) - 20);
        }
    });
}

function active_menu() {
    var $a = $("a[href='" + window.location.href + "']");

    if ($a.length == 0) {
        var base = $("base").attr('href');
        $a = $("a[href='" + window.location.href.replace(base, './') + "']");
    }

    $a.parents('li').addClass('active');
}

function toggle_panel() {
    var $panel_local_storage = $(".panel[data-local-storage-id]");
    var $toggle_panel = $(".panel-title .toggle-panel");

    if ($panel_local_storage.length > 0) {
        $panel_local_storage.each(function (i, element) {
            var $panel = $(element),
                local_storage_id = $panel.data('local-storage-id'),
                local_storage = localStorage.getItem(local_storage_id);

            if (local_storage == null) {
                $panel.removeClass('panel-close');
            }
        });
    }

    $toggle_panel.click(function () {
        var $panel = $(this).closest('.panel'),
            local_storage_id = $panel.data('local-storage-id');

        $panel.toggleClass('panel-close');

        if ($panel.hasClass('panel-close')) {

            if (local_storage_id != undefined && local_storage_id != '' && local_storage_id) {
                localStorage.setItem(local_storage_id, true);
            }

        } else {
            localStorage.removeItem(local_storage_id);
        }

    });


}

function check_id($content, id_complete) {

    var id = 1;

    for (i = 1; $content.find(id_complete + id).length > 0; i++) {
        id = i;
    }

    return id;

}

function remove_template_item(button) {
    var $button = $(button);
    $button.closest('.item').remove();

    return false;
}


function template_loop() {
    var $template_loop = $(".template-loop");
    if ($template_loop.length > 0) {

        $template_loop.each(function (i, element) {
            var $this_loop = $(element);

            var $list_item = $this_loop.find('.list-item');

            get_template($list_item);

            $this_loop.find(".plus-template").click(function () {

                var id = check_id($list_item, '#item-');

                var html = fill_template($list_item, {
                    id: id
                });

                $list_item.append(html);

                return false;

            });
        });
    }
}