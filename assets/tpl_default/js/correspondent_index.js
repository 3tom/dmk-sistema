$(function () {
    $('table').dataTable({
        "language": {
            "url": "./assets/datatable/Portuguese-Brasil.json"
        }
    });
});

function change_status(id) {
    var btn = $("[data-id='"+id+"']");
    if(btn.hasClass('btn-success')){
        $(btn).addClass('btn-danger');
        $(btn).removeClass('btn-success');
        var status = 0;
    }else{
        $(btn).addClass('btn-success');
        $(btn).removeClass('btn-danger');
        var status = 1;
    }
    $.ajax({
        url: 'ws/correspondent/status',
        data: {status: status, id: id},
        method: 'POST',
        success: function (data) {
            if (data.error) {
                alert_message(data.error, 'alert-danger');
            }else{

            }
        }
    });
}