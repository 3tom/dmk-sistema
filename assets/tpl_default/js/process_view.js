$(function () {
    $('.dataTable').dataTable({
        "language": {
            "url": "./assets/datatable/Portuguese-Brasil.json"
        }
    });

    $('#processstatus_id').change(function () {
        if(this.value == 6) {
            $('#reason').closest('.hidden').removeClass('hidden');
            $('#reason').prop('required', true).focus;
        }
    })
});