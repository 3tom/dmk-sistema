function automatic_value(_item, option) {
    var $td = $(_item).closest('td');
    var $tr = $(_item).closest('tr');
    var $input = $td.find('input');
    var value = $input.prop('value');

    switch (option) {
        case 'all':
            automatic_value_all($tr, value);
            break;
        case 'empty':
            automatic_value_empty($tr, value);
            break;
        case 'old':
            automatic_value_old($tr, $input, value);
            break;
    }
}

function automatic_value_all($tr, value) {
    var $input = $tr.find('input.money');
    $input.prop('value', value);

}
function automatic_value_empty($tr, value) {
    $tr.find('input.money').each(function () {
        if (!this.value) {
            this.value = value;
        }
    })
}
function automatic_value_old($tr, $input, value) {
    var old = $input.data('old_value');
    var $input = $tr.find('[data-old_value="' + old + '"]');
    $input.prop('value', value);
}


$(function() {
    var modal_id = 'automatic-value-confirm-modal';
    var modal = '<div id="' + modal_id + '" class="modal fade" role="dialog">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '<h4 class="modal-title">Confirmar substituições</h4>' +
        '</div>' +
        '<div class="modal-body">' +
        '<p>Essa ação é irreversível. Tem certeza que deseja prosseguir?.</p>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>' +
        '<button type="button" class="btn btn-danger btn-ok" data-dismiss="modal">Continuar</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $('body').prepend(modal);

    $('#' + modal_id).on('show.bs.modal', function(e) {
        var btn = $(this).find('.btn-ok');

        btn.unbind( "click" );
        btn.data('relatedTarget', e.relatedTarget);
        btn.click(function () {
            var relatedTarget = $(this).data('relatedTarget');
            console.log($(relatedTarget))
            $(relatedTarget).parent().find('.screen-reader-text').first().click();
        })
    });
});